﻿function Get-ServiceByAccount
{
<#
.SYNOPSIS 
    Get services that has a specific useraccount

.Description
    Supports pipeline input. Outputs the wmi object 

.Parameter ComputerName
    Name of the computer

.Parameter UserName
    Name of the user

.EXAMPLE
    Get-ServiceByAccount -ComputerName "server2","server1" -UserName "administrator"

.EXAMPLE
    "server2","server1" | Get-ServiceByAccount -UserName "administrator"

.EXAMPLE
    "server2","server1" | Get-ServiceByAccount -UserName "administrator" | select Name, PScomputerName

.Notes
    NAME: Get-ServiceByAccount
    AUTHOR: Tore Groneng tore@firstpoint.no @toregroneng tore.groneng@gmail.com
    LASTEDIT: April 2014 
    KEYWORDS: General scripting SMART
    HELP:OK
#>
[cmdletbinding()]
Param(
    [parameter(ValueFromPipeline = $true)]
    [string[]] 
    $ComputerName = "localhost"
    ,
    [string] 
    $UserName
)

BEGIN
{
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"
}

PROCESS
{    
    foreach ($computer in $computername)
    {
        Write-Verbose -Message "Now checking on computer $computer"
        Get-WmiObject -Query "Select * from win32_Service" -ComputerName $computer | where {$_.StartName -like "*$username*"}
    }
    $
}

END
{
write-Verbose "END"
}
}