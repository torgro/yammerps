[cmdletbinding()]
Param(
		[String]$AppPoolName = ""
	)

# Powershell script to Restart WebAppPools in IIS
# Created by: Tore Gr�neng - www.firstpoint.no @toregroneng tore@firstpoint.no tore.groneng@gmail.com
# Created   : December 2013 (Bergen Norway)
# Changed   : 27.02.2014 - Bug in Dynamic-LoadModule - wrong variable name (tore)
# Changed   : 06.12.2013 - Beta release version 1.0 (Initial release)
#		
#
# Version   : 1.1

Function Dynamic-LoadModule
{
[cmdletbinding()]
Param(
		[String]$ModuleName
	)
	
	$f = $MyInvocation.MyCommand.Name
	[bool]$ModuleIsLoaded = $false
	write-verbose "$f - Start"
	write-verbose "$f - Checking module $ModuleName"
	
	if(get-module -name $ModuleName)
	{
		write-verbose "$f - $ModuleName is loaded"
		$ModuleIsLoaded = $true
	}
	else
	{
		import-module -name $ModuleName
		$ModuleIsLoaded = (get-module -name $ModuleName | measure-object).count
		if($ModuleIsLoaded -ne 1)
		{
			$msg = "Unable to load $ModuleName module"
			write-verbose "$f - $msg"
            throw "$msg"
		}
		else
		{
			$ModuleIsLoaded = $true
		}
	}
	write-verbose "$f - END"
	return $ModuleIsLoaded
}

Function Action-AppPool
{
[cmdletbinding()]
Param(
		[String]$AppPoolName = ""
		,
		[switch]$Stop
		,
		[switch]$Start
	)
	
	$f = $MyInvocation.MyCommand.Name
	write-verbose "$f - Start"
	
	[string]$Action = "Stop()"
	
	Write-verbose "$f -  Changing location to the IIS PS-drive"
	cd iis:
	
	if($pwd.path -ne "IIS:\")
	{
		Throw("Unable to change location to the IIS: PS-drive")
		break
	}
	
	$AllAppPools =  Get-ChildItem -Path .\AppPools
	
	if($Start)
	{
		$Action = "Start()"
	}
	
	Write-Verbose "$f -  Action is $action"
	
	if($AppPoolName -eq "")
	{
		write-verbose "$f -  Running action $Action on all AppPools"
		$AllAppPools | foreach {
							if($start)
							{
								if($_.state -ne "Started")
								{
									Write-verbose "$f -  Starting AppPool '$($_.name)'"
									invoke-Expression ('$_' + ".$action")
								}
								else
								{
									write-verbose "$f -  AppPool '$($_.name)' is Started"
								}
							}
									
							if($Stop)
							{
								if($_.state -ne "Stopped")
								{
									Write-verbose "$f -  Stopping AppPool '$($_.name)'"
									invoke-Expression ('$_' + ".$action")
								}
								else
								{
									write-verbose "$f -  AppPool '$($_.name)' is Stopped"
								}
							}
						}
	}
	else
	{
		write-verbose "$f -  Running action $Action on single AppPool"
		[int]$CountSingleAppPool = ($AllAppPools | where {$_.Name -eq $AppPoolName} | Measure-object).count
		if ($CountSingleAppPool -eq 1)
		{
			$SingleAppPool = $AllAppPools | where {$_.Name -eq $AppPoolName}
			if($start)
			{
				if($SingleAppPool.state -ne "Started")
				{
					Write-verbose "$f -  Starting AppPool '$($SingleAppPool.name)'"
					invoke-Expression ('$SingleAppPool' + ".$action")
				}
				else
				{
					write-verbose "$f -  AppPool '$($SingleAppPool.name)' is Started"
				}
			}
			
			if($stop)
			{
				if($SingleAppPool.state -ne "Stopped")
				{
					Write-verbose "$f -  Stopping AppPool '$($SingleAppPool.name)'"
					invoke-Expression ('$SingleAppPool' + ".$action")
				}
				else
				{
					write-verbose "$f -  AppPool '$($SingleAppPool.name)' is Stopped"
				}
			}
		}
		else
		{
			if($CountSingleAppPool -eq 0)
			{
				Throw("AppPool named '$AppPoolName' was not found")
				break
			}
		}
	}
	write-verbose "$f - END"
}


    # Main entry point
	
	$f = $MyInvocation.MyCommand.Name
	Write-Verbose "$f - Start"
	Write-Verbose "$f -  We need the ServerManager module, checking"
	
	$curDir = $pwd
	
	if ((Dynamic-LoadModule -ModuleName ServerManager) -eq $false)
	{
		Throw("Unable to load ServerManager module")
		break
	}
	
	Write-Verbose "$f -  We need the WebAdministration module, checking"
	if ((Dynamic-LoadModule -ModuleName WebAdministration) -eq $false)
	{
		Throw("Unable to load WebAdministration module")
		break
	}
	
	cd iis:
	
	Write-Verbose "$f -  Stopping AppPool(s)"

	if($AppPoolName -eq "")
	{
		Write-Verbose "$f -  AppPoolName is empty, assuming action on all AppPools"
		Action-AppPool -Stop
	}
	else
	{
		Write-Verbose "$f -  Stopping AppPool '$AppPoolName'"
		Action-AppPool -AppPoolName $AppPoolName -Stop
	}
	
	Write-Verbose "$f -  Resolving WAS-serivce dependencies (Stopping dependent Services)"
	
	$WASservice = Get-Service -Name "WAS" 
	
	foreach($DependentService in $WASservice.DependentServices)
	{
		Write-Verbose "$f -    Stopping Service '$($DependentService.Name)'"
		Get-Service -Name $DependentService.Name | Stop-Service
	}
	
	Write-Verbose "$f -  Restarting WAS-service with the FORCE flag"
	$WASservice | Restart-Service -force
	
	Write-Verbose "$f -  Starting dependent services"
	
	foreach($DependentService in $WASservice.DependentServices)
	{
		# We have to ignore the W3SVC-service
		if($DependentService.Name -ne "W3SVC")
		{
			$ServiceStartMode = Get-WMIobject -Query "Select StartMode from win32_service where name = '$($DependentService.Name)'"
			if($ServiceStartMode -ne $null)
			{
				if($ServiceStartMode.StartMode -ne "Disabled")
				{
					Write-Verbose "$f -   Starting dependent Service '$($DependentService.Name)'"
					Get-Service -Name $DependentService.Name | Start-Service
				}
				else
				{
					Write-Verbose "$f -   The service '$($DependentService.Name)' has StartMode set to DISABLED, we cannot start the service"
				}
			}
			else
			{
				Throw("Unable to query WMI for service properties, service name '$($DependentService.Name)'")
				break
			}
		}
	}
	
	Write-Verbose "$f -  Starting AppPool(s)"
	
	if($AppPoolName -eq "")
	{
		Write-Verbose "$f -  AppPoolName is empty, assuming action on all AppPools"
		Action-AppPool -Start
	}
	else
	{
		Write-Verbose "$f -  Starting AppPool '$AppPoolName'"
		Action-AppPool -AppPoolName $AppPoolName -Start
	}
	
	Write-Verbose "$f -  Starting W3SVC-Service"
	Get-Service -Name W3SVC | Start-Service
	Write-Verbose "$f - END"
	cd $curDir.path