﻿function Get-SubString
{
<#
.SYNOPSIS
    An implementation of .Net substring in powershell

.DESCRIPTION
    Slice a string using position or from/to keyword or next linebreak

.EXAMPLE
    PS C:\>"say my name you silly person" | Get-Substring -FromString you
    you silly person

    Will output from the first occurance of you and to the end of the string

.EXAMPLE
    PS C:\>Get-Substring -Inputobject "say my name you silly person" -FromString you
    you silly person

    Will output from the first occurance of you and to the end of the string

.EXAMPLE
    PS C:\>$string = "say my name you silly person"
    PS C:\>$string | Get-Substring -FromString you -ToString silly
    you silly

    Will output from the first occurance of you and including the ToString

.EXAMPLE
    PS C:\>$string = "say my name you silly person"
    PS C:\>Get-SubString -Inputobject $string -FromPosition 12
    you silly person

    Will output from char 12 and to the end of the inputobject

.EXAMPLE
    PS C:\>$string = "say my name you silly person"
    PS C:\>Get-SubString -Inputobject $string -FromPosition 0 -ToPosition 3
    say

    Will output from char 0 and 3

.LINK
    https://bitbucket.org/torgro/yammerps/src/master/Get-SubString.ps1?fileviewer=file-view-default

.Notes
    NAME: Get-SubString
    AUTHOR: Tore Groneng tore@firstpoint.no @toregroneng tore.groneng@gmail.com
    LASTEDIT: Jul 2016
    KEYWORDS: General scripting SMART
    HELP:OK   
#>
[cmdletbinding(DefaultParameterSetName='ByString')]
[OutputType([string])]
Param(
    [Parameter(ValueFromPipeLine)]
    $Inputobject
    ,
    [Parameter(Mandatory,ParameterSetName="ByString")]
    [Parameter(ParameterSetName="ByStringBlankLine")]
    [string]
    $FromString
    ,
    [Parameter(ParameterSetName="ByString")]
    [string]
    $ToString
    ,
    [Parameter(ParameterSetName="ByStringBlankLine")]
    [switch]
    $UntilBlankLine
    ,
    [Parameter(Mandatory,ParameterSetName="ByPos")]
    [int]
    $FromPosition
    ,
    [Parameter(ParameterSetName="ByPos")]
    [int]
    $ToPosition
)

Begin
{
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"
}

Process
{
    if($PSBoundParameters.ContainsKey("FromString"))
    {
        [int]$startIndex = $Inputobject.ToLower().IndexOf($FromString.ToLower())
    }
    
    if($PSBoundParameters.ContainsKey("ToString"))
    {
        [int]$endIndex = ($Inputobject.ToLower().SubString($startIndex)).IndexOf($ToString.ToLower()) + $startIndex + $ToString.Length
    }

    if($PSBoundParameters.ContainsKey("UntilBlankLine"))
    {
        [int]$endIndex = $Inputobject.IndexOf([System.Environment]::NewLine)
        if($endIndex -eq -1)
        {
            $endIndex = $null
        }
    }

    if($PSBoundParameters.ContainsKey("FromPosition"))
    {
        [int]$startIndex = $FromPosition
    }

    if($PSBoundParameters.ContainsKey("ToPosition"))
    {
        [int]$endIndex = $ToPosition
    }

    if($endIndex)
    {
        $Inputobject.SubString($startIndex,($endIndex - $startIndex))
    }
    else
    {
        $Inputobject.SubString($startIndex)
    }
}

End
{
    Write-Verbose -Message "$f - END"
}
}