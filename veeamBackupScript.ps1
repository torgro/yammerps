﻿<#
.Synopsis
   Backup selected VM(s) on a VMware host

.DESCRIPTION
   Need a longer description of the purpose of this script. It requires the Veeam powershell snapin

.EXAMPLE
   .\veeamBackupScript.ps1 -VMNames "vm1","vm2","server1" -HostName vmwarehost1.company.net -Directory c:\backupfolder\host1

.EXAMPLE
   $splatVariable = @{
        VMNames = "vm1","vm2","server1"
        HostName = "vmwarehost1.company.net"
        Directory = "c:\backupfolder\host1"
   }

   .\veeamBackupScript.ps1 @splatVariable
.NOTES
   Author: Vladimir Eremin
   Created Date: 3/24/2015
   http://forums.veeam.com/member31097.html
   Modified by: Tore Grøneng tore.groneng@gmail.com @toregroneng
   Modified date: June 2015
.COMPONENT
   This is a script for Veeam backup of a vmware host VMs
.ROLE
   Veeam
.FUNCTIONALITY
   The functionality that best describes this cmdlet
#>
[cmdletbinding()]
Param(
    # Names of VMs to backup separated by semicolon (Mandatory)
    [Parameter(Mandatory=$true)]
    [ValidateNotNull()]
    [ValidateNotNullOrEmpty()]
    [string[]]
    $VMNames
    ,
    # Name of vCenter or standalone host VMs to backup reside on (Mandatory)
    [Parameter(Mandatory=$true)]
    [ValidateNotNull()]
    [ValidateNotNullOrEmpty()]
    [string]
    $HostName = ""
    ,
    # Directory that VM backups should go to (Mandatory; for instance, C:\Backup)
    [Parameter(Mandatory=$true)]
    [ValidateNotNull()]
    [ValidateNotNullOrEmpty()]
    [string]
    $Directory = ""
    ,
    # Desired compression level (Optional; Possible values: 0 - None, 4 - Dedupe-friendly, 5 - Optimal, 6 - High, 9 - Extreme) 
    [ValidateSet(0,4,5,6,9)]
    [int]
    $CompressionLevel = 5
    ,   
    # Quiesce VM when taking snapshot (Optional; VMware Tools are required; Possible values: $True/$False)
    [switch]
    $EnableQuiescence
    ,
    # Protect resulting backup with encryption key (Optional; $True/$False)
    [switch]$EnableEncryption
    ,
    # Encryption Key (Optional; path to a secure string)
    [string]
    $EncryptionKey
    ,
    # Retention settings (Optional; By default, VeeamZIP files are not removed and kept in the specified location for an indefinite period of time. 
    # Possible values: Never , Tonight, TomorrowNight, In3days, In1Week, In2Weeks, In1Month)
    [ValidateSet("Never","Tonight","TomorrowNight","In3days","In1Week","In2Weeks","In1Month")]
    $Retention = "Never"
    ,
    # Email SMTP server
    [string]
    $SMTPServer
    ,
    # Email FROM
    [string]
    $EmailFrom
    ,
    # Email TO
    [string]
    $EmailTo
    ,
    # Email subject
    [string]
    $EmailSubject
)

    Write-Verbose -Message "Validating input parameters"

    if (-not (Test-Connection -ComputerName $HostName -Count 1 -Quiet))
    { 
        Write-Error -Message "Unable to connect to '$HostName'" -Category ConnectionError
        break
    }
    
    if (-not (Test-Path -path $Directory))
    { 
        Write-Verbose -Message "Creating directory '$Directory'"
        New-Item -ItemType directory -Path $Directory
    }

    if (-not (Test-Connection -ComputerName $SMTPServer -Count 1 -Quiet))
    { 
        Write-Error -Message "Unable to connect to notification server '$SMTPServer'" -Category ConnectionError
        break
    }

    $style = @"
<style>BODY{font-family: Arial; font-size: 10pt;}
TABLE{border: 1px solid black; border-collapse: collapse;}
TH{border: 1px solid black; background: #dddddd; padding: 5px; }
TD{border: 1px solid black; padding: 5px; }
</style>
"@

    Add-PSSnapin VeeamPSSnapin -ErrorVariable addSnapInError

    if ($addSnapInError)
    { 
        $addSnapInError
        Write-Error -Message "Unable to find VeeamPSSnapin or error loading snapin" -Category InvalidOperation -ErrorAction Stop        
    }

    $Server = Get-VBRServer -Name $HostName
    $MesssagyBody = @()

    foreach ($VMName in $VMNames)
    {
        $VM = Find-VBRViEntity -Name $VMName -Server $Server -ErrorAction SilentlyContinue

        if(-not $VM)
        {
            Write-Warning -Message "Unable to find VM '$VMName', skipping" 
            continue
        }

        $StartVBRZip = @{
            Entity = $VM
            Folder = $Directory
            Compression = $CompressionLevel
            AutoDelete = $Retention
        }
  
        if ($EnableEncryption)
        {
            $EncryptionKey = Add-VBREncryptionKey -Password (Get-Content -Path $EncryptionKey | ConvertTo-SecureString)
            $null = $StartVBRZip.Add("EncryptionKey",$EncryptionKey)
        }  
    
        if (-not $EnableQuiescence)
        {
            $null = $StartVBRZip.Add("DisableQuiesce",$true)
        }

        $ZIPSession = Start-VBRZip @StartVBRZip
  
        if ($SMTPServer) 
        {
            $TaskSessions = $ZIPSession.GetTaskSessions().logger.getlog().updatedrecords
            $FailedSessions =  $TaskSessions | where {$_.status -eq "EWarning" -or $_.Status -eq "EFailed"}
  
            if ($FailedSessions -ne $Null)
            {
                $MesssagyBody = $MesssagyBody + ($ZIPSession | Select-Object @{n="Name";e={($_.name).Substring(0, $_.name.LastIndexOf("("))}} ,@{n="Start Time";e={$_.CreationTime}},@{n="End Time";e={$_.EndTime}},Result,@{n="Details";e={$FailedSessions.Title}})
            }
            else
            {
                $MesssagyBody = $MesssagyBody + ($ZIPSession | Select-Object @{n="Name";e={($_.name).Substring(0, $_.name.LastIndexOf("("))}} ,@{n="Start Time";e={$_.CreationTime}},@{n="End Time";e={$_.EndTime}},Result,@{n="Details";e={($TaskSessions | sort creationtime -Descending | select -first 1).Title}})
            }
        } 
    }

    if ($SMTPServer)
    {
        $Message = New-Object System.Net.Mail.MailMessage $EmailFrom, $EmailTo
        $Message.Subject = $EmailSubject
        $Message.IsBodyHTML = $True
        $message.Body = $MesssagyBody | ConvertTo-Html -head $style | Out-String
        $SMTP = New-Object Net.Mail.SmtpClient($SMTPServer)
        $SMTP.Send($Message)
    }

    Write-Verbose -Message "Done"