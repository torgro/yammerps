function Get-UrlEncoding
{
<#
.Synopsis
   URL encodes a string

.DESCRIPTION
   Long description

.PARAMETER Value
    The conent that you want to encode

.PARAMETER SentToClipboard
    Sends the output to the clipboard as well

.EXAMPLE
   Get-UrlEncoding -Value "encode & parse this text"

   encode+%26+parse+this+text

.EXAMPLE
    Get-UrlEncoding -Value "encode & parse this text" -SentToClipboard

    encode+%26+parse+this+text

.EXAMPLE
    Get-UrlEncoding -Value "encode+%26+parse+this+text" -Decode

    encode & parse this text

.NOTES
    Created by Tore.Groneng@firstpoint.no @ToreGroneng 2014
#>
[CmdletBinding()]
[OutputType([string])]
Param
(        
    [Parameter(Mandatory=$true)]
    [Alias("URL")]
    [string] 
    $Value
    ,
    [Alias("Clip")]
    [switch] 
    $SentToClipboard
    ,
    [switch] 
    $Decode
)
    
    $loadedAssemblies = $null
    $loadedAssemblies = [System.AppDomain]::CurrentDomain.GetAssemblies() | Where-Object {$_.FullName -like "System.Web*"}

    if (-not($loadedAssemblies))
    {
        Write-Verbose "Loading assembly System.Web"
        [System.Reflection.Assembly]::LoadWithPartialName("System.Web")
    }
    else
    {
        Write-Verbose "System.Web is loaded"
    }

    if ($Decode)
    {
        Write-Verbose "In decode mode"
        $ProcessedValue = [System.Web.HttpUtility]::UrlDecode($Value)
    }
    else
    {
        Write-Verbose "In encode mode"
        $ProcessedValue = [System.Web.HttpUtility]::UrlEncode($Value)        
    }

    if ($SentToClipboard)
    {
        Write-Verbose "Sending content to clipboard"
        $ProcessedValue | clip.exe
    }        
    $ProcessedValue
}