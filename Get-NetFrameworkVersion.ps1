﻿function Get-NetFrameworkVersion
{
[cmdletbinding()]
Param(    
    [switch]$ShowOnly32Bit
    ,
    [switch]$ShowOnly64Bit
)
    function Get-OutObject
    {
    [cmdletbinding()]
    param(
        [string] $BitVersion
        ,
        [string] $frameworkVersion
    )
        [pscustomobject]@{BitVersion=$BitVersion;InstalledVersion=$frameworkVersion}
    }
 
    function Test-Key
    {
    [cmdletbinding()]
    Param(
        [string] $Path
        ,
        [string] $Key
    )
        $f = $MyInvocation.MyCommand.Name
        Write-Verbose -Message "$f - Start"
        if(!(Test-Path $path)) 
        { 
            Write-Verbose "Test-path $Path returned false"
            return $false
        }

        if ((Get-ItemProperty $path).$key -eq $null)
        { 
            Write-Verbose "Get-ItemProperty returned null for $Path"
            return $false
        }

        Write-Verbose "Test-Path is true and Get-ItemProperty did not return null"
        return $true
    }

    $installedFrameworks = @()
    if(Test-Key "HKLM:\Software\Microsoft\.NETFramework\Policy\v1.0" "3705") { $installedFrameworks += Get-OutObject -BitVersion x64 -frameworkVersion "1.0" }
    if(Test-Key "HKLM:\Software\Microsoft\NET Framework Setup\NDP\v1.1.4322" "Install") { $installedFrameworks += Get-OutObject -BitVersion x64 -frameworkVersion "1.1" }
    if(Test-Key "HKLM:\Software\Microsoft\NET Framework Setup\NDP\v2.0.50727" "Install") { $installedFrameworks += Get-OutObject -BitVersion x64 -frameworkVersion "2.0" }
    if(Test-Key "HKLM:\Software\Microsoft\NET Framework Setup\NDP\v3.0\Setup" "InstallSuccess") { $installedFrameworks += Get-OutObject -BitVersion x64 -frameworkVersion "3.0" }
    if(Test-Key "HKLM:\Software\Microsoft\NET Framework Setup\NDP\v3.5" "Install") { $installedFrameworks += Get-OutObject -BitVersion x64 -frameworkVersion "3.5" }
    if(Test-Key "HKLM:\Software\Microsoft\NET Framework Setup\NDP\v4.0\Client" "Install") { $installedFrameworks += Get-OutObject -BitVersion x64 -frameworkVersion "4.0c" }
    if(Test-Key "HKLM:\Software\Microsoft\NET Framework Setup\NDP\v4.0\Full" "Install") { $installedFrameworks += Get-OutObject -BitVersion x64 -frameworkVersion "4.0" }
    if(Test-Key "HKLM:\Software\Microsoft\NET Framework Setup\NDP\v4\Client" "Install") { $installedFrameworks += Get-OutObject -BitVersion x64 -frameworkVersion "4.5c" }
    if(Test-Key "HKLM:\Software\Microsoft\NET Framework Setup\NDP\v4\Full" "Install") { $installedFrameworks += Get-OutObject -BitVersion x64 -frameworkVersion "4.5" }   

    if(Test-Key "HKLM:\SOFTWARE\Wow6432Node\Microsoft\NET Framework Setup\NDP\v3.0\Setup" "InstallSuccess") { $installedFrameworks += Get-OutObject -BitVersion x32 -frameworkVersion "3.0" }
    if(Test-Key "HKLM:\SOFTWARE\Wow6432Node\Microsoft\NET Framework Setup\NDP\v3.5" "Install") { $installedFrameworks += Get-OutObject -BitVersion x32 -frameworkVersion "3.5" }
    if(Test-Key "HKLM:\SOFTWARE\Wow6432Node\Microsoft\NET Framework Setup\NDP\v4.0\Full" "Install") { $installedFrameworks += Get-OutObject -BitVersion x32 -frameworkVersion "4.5" }
    if(Test-Key "HKLM:\SOFTWARE\Wow6432Node\Microsoft\NET Framework Setup\NDP\v4.0\Client" "Install") { $installedFrameworks += Get-OutObject -BitVersion x32 -frameworkVersion "4.5c" }
    if(Test-Key "HKLM:\SOFTWARE\Wow6432Node\Microsoft\NET Framework Setup\NDP\v4\Client" "Install") { $installedFrameworks += Get-OutObject -BitVersion x32 -frameworkVersion "4.0c" }
    if(Test-Key "HKLM:\SOFTWARE\Wow6432Node\Microsoft\NET Framework Setup\NDP\v4\Full" "Install") { $installedFrameworks += Get-OutObject -BitVersion x32 -frameworkVersion "4.0" }
    
    if($ShowOnly32Bit)
    {
        Write-Verbose "showing only 32-bit versions"
        $installedFrameworks | where {$_.BitVersion -eq "x32"}
    }

    if($ShowOnly64Bit)
    {
        Write-Verbose "showing only 64-bit versions"
        $installedFrameworks | where {$_.BitVersion -eq "x64"}
    }
    if(-not $ShowOnly32Bit -and (-not $ShowOnly64Bit))
    {
        Write-Verbose "showing both"
        $installedFrameworks
    }
    
}

