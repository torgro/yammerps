function Get-ServiceStartTime
{
[cmdletbinding()]
Param(
    [string] $Computername
    ,
    [String] $ServiceName
)
    [string] $f = $MyInvocation.MyCommand.Name

    Write-Verbose "$f - Start"

    if(-not $Computername)
    {
        $Computername = "."
    }
    [string]$exe = (get-wmiobject -query "Select * from win32_service where name='$ServiceName'" -ComputerName $ComputerName).PathName

    if($exe)
    {
        write-verbose "Have Service executeable $exe"
		$exe = $exe.replace('"','')
        [string]$ExeName = [System.IO.Path]::GetFileName($exe)
		[string]$FullPath = [System.IO.Path]::GetDirectoryName($exe) + "\"

        if($ExeName.Contains(" "))
        {
            $ExeName = ($ExeName -split " ")[0]
        }
		if($ExeName -ne "")
		{
            write-verbose "Have fileName '$ExeName', looking for the process"
			$process = Get-WmiObject -computername $Computername -query "select * from Win32_Process where name='$ExeName'"
			if ($process -ne $null)
			{
				write-verbose "Have process, looking for the startTime"
							
				#[wmi]$wmi | out-null
				write-verbose "Created wmi-object"
				$startTime = ([wmi]"").ConvertToDateTime($process.CreationDate)
				write-verbose "Converted date to readable format $starttime"
				$output = "" | select ProcessName, StartTime, Path, Computer
				write-verbose "Created output object"
				$output.ProcessName = $ExeName 
				$output.StartTime = $startTime
				$output.path = $FullPath
				$output.computer = $ComputerName
				write-verbose "Outputing results"
				$output								
			}
			else
			{
				write-verbose "Could not find a process for executable '$ExeName'"
			}
        }
        else
        {
            Write-Verbose "Unable to find executable for service $ServiceName"
        }
    }
    else
    {
        Write-Verbose -Message "Unable to find service $ServiceName on computer $Computername"
    }
}