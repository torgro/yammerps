function Get-MSGraphAuthenticationToken 
{
<# 
.SYNOPSIS 
    This function is used to get an authentication token for the Graph API REST interface 
.DESCRIPTION 
    Built based on the following example script from Microsoft: https://github.com/microsoftgraph/powershell-intune-samples/blob/master/Authentication/Auth_From_File.ps1 
.EXAMPLE 
    $Credential = Get-Credential 
    $AuthToken = Get-MSGraphAuthenticationToken -Credential $Credential
#>
    [cmdletbinding()]
      
Param
(
    [PSCredential]
    $Credential
)
      
Write-Verbose -Message 'Importing prerequisite modules...'
      
try 
{
    $AadModule = Import-Module -Name AzureAD -ErrorAction Stop -Verbose:$false -PassThru  
}    
catch
{
    throw 'Prerequisites not installed, AzureAD PowerShell module not installed'
}

if (-not $AadModule)
{
    Write-Error -Message "Unable to find AzureAD module" -ErrorAction Stop
}

$errAction = $ErrorActionPreference
$ErrorActionPreference = "Stop"
$mailAdr = [System.Net.Mail.MailAddress]::new($Credential.UserName)

$Tenant = $mailAdr.Host

Write-Verbose -Message "Tenant is [$Tenant]"
Write-Verbose -Message "Module path is [$($AadModule.ModuleBase)]"
      
$adal = Join-Path -Path $AadModule.ModuleBase -Childpath "Microsoft.IdentityModel.Clients.ActiveDirectory.dll"
$adalforms = Join-Path $AadModule.ModuleBase "Microsoft.IdentityModel.Clients.ActiveDirectory.Platform.dll"

Write-Verbose -Message "Loading assembley [$adal]"
$null = [System.Reflection.Assembly]::LoadFrom($adal)

Write-Verbose -Message "Loading assembley [$adalforms]"
$null = [System.Reflection.Assembly]::LoadFrom($adalforms)

$ErrorActionPreference = $errAction
       
$clientId = "d1ddf0e4-d672-4dae-b554-9d5bdfd93547"
$resourceAppIdURI = "https://graph.microsoft.com"
$authority = "https://login.microsoftonline.com/$Tenant"

Write-Verbose -Message "Authority is [$authority]"
      
try 
{       
    $tokenCache = [Microsoft.IdentityModel.Clients.ActiveDirectory.TokenCache]::new()
    $authContext = [Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext]::new($authority, $tokenCache)

    Write-Verbose -Message "Creating UserPassword credential"

    $userPasswordCredential = [Microsoft.IdentityModel.Clients.ActiveDirectory.UserPasswordCredential]::new($Credential.UserName, $Credential.GetNetworkCredential().Password)

    Write-Verbose -Message "Acuire token"

    $AsyncToken = [Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContextIntegratedAuthExtensions]::AcquireTokenAsync($authContext, $resourceAppIdURI, $clientId, $userPasswordCredential).Result
    
    if ($AsyncToken)
    {  
        Write-Verbose -Message "Creating header"       
        
        $authHeader = @{
            'Content-Type'  = 'application/json'
            'Authorization' = $AsyncToken.CreateAuthorizationHeader() #"Bearer $($tokenItem.AccessToken)"
        }        
        $authHeader                
    }
    else
    {
        Write-Verbose -Message "AcquireTokenAsync did not return a result"
        Write-Error -Message "AcquireTokenAsync did not return a result" -ErrorAction Stop
    }          
}      
catch 
{      
    throw $_.Exception.Message          
}      
} 

function Get-IntunePolicy
{
<# 
.SYNOPSIS 
    Lists Intune device policies
.DESCRIPTION 
    You need Global Admin rights to use this cmdlet.
.EXAMPLE      
    $AuthToken = Get-MSGraphAuthenticationToken -Credential $Credential
    $policies = Get-IntunePolicy -AuthHeader $AuthToken -APIVersion beta
#>
[cmdletbinding()]
Param(
    [hashtable]
    $AuthHeader
    ,
    [string]
    [ValidateSet("v1.0","beta")]
    $APIVersion = "v1.0"
)

    $baseUrl = "https://graph.microsoft.com/$APIVersion"
    $apiCall = "/deviceManagement/deviceCompliancePolicies"
    $fullUrl = "$baseUrl$apiCall"
    Write-Verbose -Message "Querying [$fullUrl]"
    $respons = Invoke-RestMethod -Method Get -Uri $fullUrl -Headers $AuthHeader -ErrorAction Stop

    if ($respons)
    {
        $respons.value
    }
    else
    {
        Write-Error -Message "Invoke-RestMethod did not return a respons"
    }
}

function Get-IntuneUpdate
{
[cmdletbinding()]
    Param(
        [hashtable]
        $AuthHeader
        ,
        [string]
        [ValidateSet("v1.0","beta")]
        $APIVersion = "v1.0"
)

    $baseUrl = "https://graph.microsoft.com/$APIVersion"
    
    $apiCall = "/deviceManagement/deviceConfigurations?`$filter=isof('microsoft.graph.windowsUpdateForBusinessConfiguration')&`$expand=groupAssignments"
    $fullUrl = "$baseUrl$apiCall"
    Write-Verbose -Message "Querying [$fullUrl]"
    $respons = Invoke-RestMethod -Method Get -Uri $fullUrl -Headers $AuthHeader -ErrorAction Stop

    if ($respons)
    {
        $respons.value
    }
    else
    {
        Write-Error -Message "Invoke-RestMethod did not return a respons"
    }
}

function Get-IntuneMe
{
[cmdletbinding()]
Param(
    [hashtable]
    $AuthHeader
    ,
    [string]
    [ValidateSet("v1.0","beta")]
    $APIVersion = "v1.0"
)
    $baseUrl = "https://graph.microsoft.com/$APIVersion"
    $apiCall = "/Me"
    $fullUrl = "$baseUrl$apiCall"
    Write-Verbose -Message "Querying [$fullUrl]"
    
    $respons = Invoke-RestMethod -Method Get -Uri $fullUrl -Headers $AuthHeader -ErrorAction Stop -ContentType "application/json"

    if ($respons)
    {
        $respons
    }
    else
    {
        Write-Error -Message "Invoke-RestMethod did not return a respons"
    }

}

function Get-IntuneDeviceCompliance
{
<# 
.SYNOPSIS 
    Lists Intune device policy compliance for devices. Use Get-IntunePolicy to show available policies
.DESCRIPTION 
    You need Global Admin rights to use this cmdlet.
.EXAMPLE      
    $AuthToken = Get-MSGraphAuthenticationToken -Credential $Credential
    $policies = Get-IntuneDeviceCompliance -AuthHeader $AuthToken -PolicyID 9b66ba18-164c-4f43-9527-9313e1bb3994
#>
[cmdletbinding()]
Param(
    [hashtable]
    $AuthHeader
    ,
    [string]
    [ValidateSet("v1.0","beta")]
    $APIVersion = "v1.0"
    ,
    [string]
    $PolicyID
)

    $baseUrl = "https://graph.microsoft.com/$APIVersion"
    $apiCall = "/deviceManagement/deviceCompliancePolicies/$PolicyID/deviceStatuses" #"/users"
    $fullUrl = "$baseUrl$apiCall"
    Write-Verbose -Message "Querying [$fullUrl]"

    $respons = Invoke-RestMethod -Method Get -Uri $fullUrl -Headers $AuthHeader -ErrorAction Stop

    if ($respons)
    {
        $respons.value
    }
    else
    {
        Write-Error -Message "Invoke-RestMethod did not return a respons"
    }
}

Update-TypeData -TypeName Intune.Device -DefaultDisplayPropertySet ID, IsEncrypted, Model, UserDisplayName -ErrorAction SilentlyContinue -Force

function Get-IntuneDevice
{
<# 
.SYNOPSIS 
    Lists Intune devices
.DESCRIPTION 
    You need Global Admin rights to use this cmdlet. You can filter on operatingsystem
.EXAMPLE      
    $AuthToken = Get-MSGraphAuthenticationToken -Credential $Credential
    $devices = Get-IntuneDevice -AuthHeader $AuthToken
.EXAMPLE      
    $WindowsDevices = Get-IntuneDevice -AuthHeader $AuthToken -OperatingSystem Windows
#>
[cmdletbinding()]
    Param(
        [hashtable]
        $AuthHeader
        ,
        [string]
        [ValidateSet("v1.0","beta")]
        $APIVersion = "v1.0"
        ,
        [ValidateSet("Android","Windows","iOS")]
        [string]
        $OperatingSystem
)

    $baseUrl = "https://graph.microsoft.com/$APIVersion"
    
    $apiCall = "/deviceManagement/managedDevices" #?`$filter=startswith(operatingSystem,'$OperatingSystem')"
    
    $fullUrl = "$baseUrl$apiCall"
    Write-Verbose -Message "Querying [$fullUrl]"
    $respons = Invoke-RestMethod -Method Get -Uri $fullUrl -Headers $AuthHeader -ErrorAction Stop

    if ($respons)
    {
        $list = [System.Collections.Generic.List[PScustomobject]]::new()
        $list.PSTypeNames.Insert(0,"Intune.Device")
        foreach ($d in $respons.value)
        {
            $d.PSTypeNames.Insert(0, "Intune.Device")
            $list.Add($d)
        }
        if (-not [string]::IsNullOrEmpty($OperatingSystem))
        {
            Write-Verbose "doing filtering by [$OperatingSystem]"
            foreach ($device in $list)
            {                
                if ($device.operatingSystem -like "$OperatingSystem*")
                {                    
                    $device
                }
            }
        }
        else
        {
            $list
        }
    }
    else
    {
        Write-Error -Message "Invoke-RestMethod did not return a respons"
    }
}

