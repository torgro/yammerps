﻿function Get-SysmonEvent
{
<#
.Synopsis
   Get sysmon events from local computer or remote computers
.DESCRIPTION
   This cmdlet lists sysmon events. You can filter on eventtypes based on the schema 
   from the sysmon tool. You can also filter on specific properties and values. The
   filter must be provided as an hashtable where the key is the property and the value
   contains the the search value. Only wildcard search (*) is supported. This cmdlet
   expects that the Sysmon eventlog is present on the local system or on a remote system
   you want to query.
.EXAMPLE
   Get-SysmonEvent

   Will list all sysmon events
.EXAMPLE
   Get-sysmonEvent -EventType CREATE_PROCESS

   Will list only events that is for process create
.EXAMPLE
   Get-sysmonEvent -EventType CREATE_PROCESS -MinutesAgo 10

   Will list only events that is for process create created the last 10 minutes
.EXAMPLE
   Get-sysmonEvent -MinutesAgo 10 -Filter @{executable='taskhost*'}

   Will list only events that is for process create created the last 10 minutes and where
   the executable is like 'taskhost*'
.EXAMPLE
   Get-sysmonEvent -EventType CREATE_PROCESS -ComputerName Remotehost.domain.com

   Will list only events that is for process create created the last 10 minutes and where
   the executable is like 'taskhost*' from the computer remotehost.domain.com
.OUTPUTS
   PSCustomobject
.NOTES
   Sysmon
   Tore.groneng@gmail.com
   Twittter @toregroneng
   Date December 2018
.ROLE
   Poke around
#>
[cmdletbinding(
    DefaultParameterSetName='byMinutes'
)]
Param(
    [ValidateSet(
        'ERROR',
        'CREATE_PROCESS',
        'FILE_TIME',
        'NETWORK_CONNECT',
        'SERVICE_STATE_CHANGE',
        'PROCESS_TERMINATE',
        'DRIVER_LOAD',
        'IMAGE_LOAD',
        'CREATE_REMOTE_THREAD',
        'RAWACCESS_READ',
        'ACCESS_PROCESS',
        'FILE_CREATE',
        'REG_KEY',
        'REG_SETVALUE',
        'REG_NAME',
        'FILE_CREATE_STREAM_HASH',
        'SERVICE_CONFIGURATION_CHANGE',
        'CREATE_NAMEDPIPE',
        'CONNECT_NAMEDPIPE',
        'WMI_FILTER',
        'WMI_CONSUMER',
        'WMI_BINDING'
    )]
    [string]
    [Parameter(ParameterSetName='byMinutes')]
    [Parameter(ParameterSetName='byHours')]
    [Parameter(ParameterSetName='byDays')]
    $EventType
    ,
    [string]
    $ComputerName
    ,
    [Parameter(ParameterSetName='byMinutes')]
    [Parameter(ParameterSetName='Filtered')]
    [int]
    $MinutesAgo
    ,
    [Parameter(ParameterSetName='byHours')]
    [Parameter(ParameterSetName='Filtered')]
    [int]
    $HoursAgo
    ,
    [Parameter(ParameterSetName='byDays')]
    [Parameter(ParameterSetName='Filtered')]
    [int]
    $DaysAgo
    ,
    [Parameter(ParameterSetName='Filtered')]
    [hashtable]
    $Filter
)
    
    $LogHash = @{
        FilterHashtable = @{
            LogName = Get-SysmonEventLogName        
        }
    }

    if (-not ([string]::IsNullOrEmpty($EventType)))
    {
        $schema = Get-SysmonSchema -Name "SYSMON_$EventType"

        if ($null -ne $schema)
        {
            $LogHash.FilterHashtable.Add("id",$schema.value)
        }    
    }

    if (-not ([string]::IsNullOrEmpty($ComputerName)))
    {
        $LogHash.ComputerName = $ComputerName    
    }

    if ($PSBoundParameters.ContainsKey("MinutesAgo"))
    {
        $startDate = [datetime]::Now.AddMinutes(-$MinutesAgo)
    }

    if ($PSBoundParameters.ContainsKey("HoursAgo"))
    {
        $startDate = [datetime]::Now.AddHours(-$HoursAgo)
    }

    if ($PSBoundParameters.ContainsKey("DaysAgo"))
    {
        $startDate = [datetime]::Now.AddDays(-$DaysAgo)
    }

    if ($null -ne $startDate)
    {
        $LogHash.FilterHashtable.StartTime = $startDate
        $LogHash.FilterHashtable.EndTime = [datetime]::Now
    }

    $events = Get-WinEvent @LogHash -Verbose:$false

    foreach ($event in $events)
    {        
        $schema = Get-SysmonSchema -id $event.Id
        $SchemaCount = ($schema | Measure-Object).Count        

        if ($null -eq $schema)
        {
            Write-Error -Message "Error unable to find sysmon schema"
            continue
        }

        $EventObj = [ordered]@{
            FullName = $schema.template
            RuleName = $schema.rulename
            EventId = $event.Id
            ComputerName = $event.MachineName
        }

        $Counter = 1

        foreach ($prop in $schema.data)
        {                
            if ($prop.name -eq "RuleName")
            {
                continue
            }
        
            $EventObj.Add($prop.name,"")

            if ($event.Properties[$Counter].Value -is [guid])
            {
                $EventObj[$prop.name] = $event.Properties[$Counter].Value.Guid
            }
            else 
            {
                $eventobj[$prop.name] = $event.Properties[$counter].Value
            }

            $counter++
        }

        if ($EventObj.Contains("Image"))
        {
            $EventObj.Add("Executable",($EventObj.Image | Split-Path -Leaf))
        }

        if ($EventObj.Contains("ParentImage"))
        {
            $EventObj.Add("ExecutableParent",($EventObj.ParentImage | Split-Path -Leaf))
        }

        if ($PSBoundParameters.ContainsKey("Filter"))
        {
            foreach ($key in $Filter.Keys)
            {
                if ($EventObj.Contains("$key"))
                {
                    if ($EventObj.$key -like $filter.$key)
                    {
                        [pscustomobject]$EventObj
                    }
                }
            }
        }
        else
        {
            [pscustomobject]$EventObj
        }
        
    }
}

function Get-SysmonEventLogName
{

    return 'Microsoft-Windows-Sysmon/Operational'

}

function Get-SysmonSchema
{
[cmdletbinding(
    DefaultParameterSetName='by name'
)]
Param(
    [Parameter(
        ValueFromPipeline,
        Position=0,
        ParameterSetName='by RuleName'
    )]
    [string]    
    $RuleName
    ,
    [Parameter(
        ParameterSetName='by full name'
    )]
    [string]
    $FullName
    ,
    [Parameter(
        ParameterSetName='by name'
    )]
    [string]
    $Name
    ,
    [Parameter(
        ParameterSetName='by EventID'
    )]
    [Alias('EventID')]
    [int]
    $Id
)
$schemaTxt = @'
<manifest schemaversion="4.1" binaryversion="8.00">
  <configuration>
    <options>
      <!-- Command-line only options -->
      <option switch="i" name="Install" argument="optional" noconfig="true" exclusive="true" />
      <option switch="c" name="Configuration" argument="optional" noconfig="true" exclusive="true" />
      <option switch="u" name="UnInstall" argument="none" noconfig="true" exclusive="true" />
      <option switch="m" name="Manifest" argument="none" noconfig="true" exclusive="true" />
      <option switch="t" name="DebugMode" argument="none" noconfig="true" />
      <option switch="s" name="PrintSchema" argument="optional" noconfig="true" exclusive="true" />
      <option switch="nologo" name="NoLogo" argument="none" noconfig="true" />
      <option switch="accepteula" name="AcceptEula" argument="none" noconfig="true" />
      <option switch="-" name="ConfigDefault" argument="none" noconfig="true" />
      <!-- Configuration file -->
      <option switch="h" name="HashAlgorithms" argument="required" />
      <option switch="n" name="NetworkConnect" argument="optional" rule="true" />
      <option switch="l" name="ImageLoad" argument="optional" rule="true" />
      <option switch="d" name="DriverName" argument="required" />
      <option switch="k" name="ProcessAccess" argument="required" rule="true" forceconfig="true" />
      <option switch="r" name="CheckRevocation" argument="none" />
      <option switch="g" name="PipeMonitoring" argument="required" rule="true" forceconfig="true" />
    </options>
    <filters default="is">is,is not,contains,excludes,begin with,end with,less than,more than,image</filters>
  </configuration>
  <events>
    <event name="SYSMON_ERROR" value="255" level="Error" template="Error report" version="3">
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ID" inType="win:UnicodeString" outType="xs:string" />
      <data name="Description" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_CREATE_PROCESS" value="1" level="Informational" template="Process Create" rulename="ProcessCreate" ruledefault="include" version="5">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ProcessGuid" inType="win:GUID" />
      <data name="ProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="Image" inType="win:UnicodeString" outType="xs:string" />
      <data name="FileVersion" inType="win:UnicodeString" outType="xs:string" />
      <data name="Description" inType="win:UnicodeString" outType="xs:string" />
      <data name="Product" inType="win:UnicodeString" outType="xs:string" />
      <data name="Company" inType="win:UnicodeString" outType="xs:string" />
      <data name="CommandLine" inType="win:UnicodeString" outType="xs:string" />
      <data name="CurrentDirectory" inType="win:UnicodeString" outType="xs:string" />
      <data name="User" inType="win:UnicodeString" outType="xs:string" />
      <data name="LogonGuid" inType="win:GUID" />
      <data name="LogonId" inType="win:HexInt64" />
      <data name="TerminalSessionId" inType="win:UInt32" />
      <data name="IntegrityLevel" inType="win:UnicodeString" outType="xs:string" />
      <data name="Hashes" inType="win:UnicodeString" outType="xs:string" />
      <data name="ParentProcessGuid" inType="win:GUID" />
      <data name="ParentProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="ParentImage" inType="win:UnicodeString" outType="xs:string" />
      <data name="ParentCommandLine" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_FILE_TIME" value="2" level="Informational" template="File creation time changed" rulename="FileCreateTime" ruledefault="include" version="4">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ProcessGuid" inType="win:GUID" />
      <data name="ProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="Image" inType="win:UnicodeString" outType="xs:string" />
      <data name="TargetFilename" inType="win:UnicodeString" outType="xs:string" />
      <data name="CreationUtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="PreviousCreationUtcTime" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_NETWORK_CONNECT" value="3" level="Informational" template="Network connection detected" rulename="NetworkConnect" version="5">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ProcessGuid" inType="win:GUID" />
      <data name="ProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="Image" inType="win:UnicodeString" outType="xs:string" />
      <data name="User" inType="win:UnicodeString" outType="xs:string" />
      <data name="Protocol" inType="win:UnicodeString" outType="xs:string" />
      <data name="Initiated" inType="win:Boolean" />
      <data name="SourceIsIpv6" inType="win:Boolean" />
      <data name="SourceIp" inType="win:UnicodeString" outType="xs:string" />
      <data name="SourceHostname" inType="win:UnicodeString" outType="xs:string" />
      <data name="SourcePort" inType="win:UInt16" />
      <data name="SourcePortName" inType="win:UnicodeString" outType="xs:string" />
      <data name="DestinationIsIpv6" inType="win:Boolean" />
      <data name="DestinationIp" inType="win:UnicodeString" outType="xs:string" />
      <data name="DestinationHostname" inType="win:UnicodeString" outType="xs:string" />
      <data name="DestinationPort" inType="win:UInt16" />
      <data name="DestinationPortName" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_SERVICE_STATE_CHANGE" value="4" level="Informational" template="Sysmon service state changed" version="3">
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="State" inType="win:UnicodeString" outType="xs:string" />
      <data name="Version" inType="win:UnicodeString" outType="xs:string" />
      <data name="SchemaVersion" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_PROCESS_TERMINATE" value="5" level="Informational" template="Process terminated" rulename="ProcessTerminate" ruledefault="include" version="3">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ProcessGuid" inType="win:GUID" />
      <data name="ProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="Image" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_DRIVER_LOAD" value="6" level="Informational" template="Driver loaded" rulename="DriverLoad" ruledefault="include" version="3">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ImageLoaded" inType="win:UnicodeString" outType="xs:string" />
      <data name="Hashes" inType="win:UnicodeString" outType="xs:string" />
      <data name="Signed" inType="win:UnicodeString" outType="xs:string" />
      <data name="Signature" inType="win:UnicodeString" outType="xs:string" />
      <data name="SignatureStatus" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_IMAGE_LOAD" value="7" level="Informational" template="Image loaded" rulename="ImageLoad" version="3">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ProcessGuid" inType="win:GUID" />
      <data name="ProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="Image" inType="win:UnicodeString" outType="xs:string" />
      <data name="ImageLoaded" inType="win:UnicodeString" outType="xs:string" />
      <data name="FileVersion" inType="win:UnicodeString" outType="xs:string" />
      <data name="Description" inType="win:UnicodeString" outType="xs:string" />
      <data name="Product" inType="win:UnicodeString" outType="xs:string" />
      <data name="Company" inType="win:UnicodeString" outType="xs:string" />
      <data name="Hashes" inType="win:UnicodeString" outType="xs:string" />
      <data name="Signed" inType="win:UnicodeString" outType="xs:string" />
      <data name="Signature" inType="win:UnicodeString" outType="xs:string" />
      <data name="SignatureStatus" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_CREATE_REMOTE_THREAD" value="8" level="Informational" template="CreateRemoteThread detected" rulename="CreateRemoteThread" version="2">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="SourceProcessGuid" inType="win:GUID" />
      <data name="SourceProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="SourceImage" inType="win:UnicodeString" outType="xs:string" />
      <data name="TargetProcessGuid" inType="win:GUID" />
      <data name="TargetProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="TargetImage" inType="win:UnicodeString" outType="xs:string" />
      <data name="NewThreadId" inType="win:UInt32" />
      <data name="StartAddress" inType="win:UnicodeString" outType="xs:string" />
      <data name="StartModule" inType="win:UnicodeString" outType="xs:string" />
      <data name="StartFunction" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_RAWACCESS_READ" value="9" level="Informational" template="RawAccessRead detected" rulename="RawAccessRead" version="2">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ProcessGuid" inType="win:GUID" />
      <data name="ProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="Image" inType="win:UnicodeString" outType="xs:string" />
      <data name="Device" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_ACCESS_PROCESS" value="10" level="Informational" template="Process accessed" rulename="ProcessAccess" version="3">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="SourceProcessGUID" inType="win:GUID" />
      <data name="SourceProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="SourceThreadId" inType="win:UInt32" />
      <data name="SourceImage" inType="win:UnicodeString" outType="xs:string" />
      <data name="TargetProcessGUID" inType="win:GUID" />
      <data name="TargetProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="TargetImage" inType="win:UnicodeString" outType="xs:string" />
      <data name="GrantedAccess" inType="win:HexInt32" />
      <data name="CallTrace" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_FILE_CREATE" value="11" level="Informational" template="File created" rulename="FileCreate" ruledefault="include" version="2">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ProcessGuid" inType="win:GUID" />
      <data name="ProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="Image" inType="win:UnicodeString" outType="xs:string" />
      <data name="TargetFilename" inType="win:UnicodeString" outType="xs:string" />
      <data name="CreationUtcTime" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_REG_KEY" value="12" level="Informational" template="Registry object added or deleted" rulename="RegistryEvent" ruledefault="include" version="2">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="EventType" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ProcessGuid" inType="win:GUID" />
      <data name="ProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="Image" inType="win:UnicodeString" outType="xs:string" />
      <data name="TargetObject" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_REG_SETVALUE" value="13" level="Informational" template="Registry value set" rulename="RegistryEvent" ruledefault="include" version="2">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="EventType" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ProcessGuid" inType="win:GUID" />
      <data name="ProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="Image" inType="win:UnicodeString" outType="xs:string" />
      <data name="TargetObject" inType="win:UnicodeString" outType="xs:string" />
      <data name="Details" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_REG_NAME" value="14" level="Informational" template="Registry object renamed" rulename="RegistryEvent" ruledefault="include" version="2">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="EventType" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ProcessGuid" inType="win:GUID" />
      <data name="ProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="Image" inType="win:UnicodeString" outType="xs:string" />
      <data name="TargetObject" inType="win:UnicodeString" outType="xs:string" />
      <data name="NewName" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_FILE_CREATE_STREAM_HASH" value="15" level="Informational" template="File stream created" rulename="FileCreateStreamHash" ruledefault="include" version="2">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ProcessGuid" inType="win:GUID" />
      <data name="ProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="Image" inType="win:UnicodeString" outType="xs:string" />
      <data name="TargetFilename" inType="win:UnicodeString" outType="xs:string" />
      <data name="CreationUtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="Hash" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_SERVICE_CONFIGURATION_CHANGE" value="16" level="Informational" template="Sysmon config state changed" version="3">
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="Configuration" inType="win:UnicodeString" outType="xs:string" />
      <data name="ConfigurationFileHash" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_CREATE_NAMEDPIPE" value="17" level="Informational" template="Pipe Created" rulename="PipeEvent" ruledefault="exclude" version="1">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ProcessGuid" inType="win:GUID" />
      <data name="ProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="PipeName" inType="win:UnicodeString" outType="xs:string" />
      <data name="Image" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_CONNECT_NAMEDPIPE" value="18" level="Informational" template="Pipe Connected" rulename="PipeEvent" ruledefault="exclude" version="1">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="ProcessGuid" inType="win:GUID" />
      <data name="ProcessId" inType="win:UInt32" outType="win:PID" />
      <data name="PipeName" inType="win:UnicodeString" outType="xs:string" />
      <data name="Image" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_WMI_FILTER" value="19" level="Informational" template="WmiEventFilter activity detected" rulename="WmiEvent" ruledefault="exclude" version="3">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="EventType" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="Operation" inType="win:UnicodeString" outType="xs:string" />
      <data name="User" inType="win:UnicodeString" outType="xs:string" />
      <data name="EventNamespace" inType="win:UnicodeString" outType="xs:string" />
      <data name="Name" inType="win:UnicodeString" outType="xs:string" />
      <data name="Query" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_WMI_CONSUMER" value="20" level="Informational" template="WmiEventConsumer activity detected" rulename="WmiEvent" ruledefault="exclude" version="3">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="EventType" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="Operation" inType="win:UnicodeString" outType="xs:string" />
      <data name="User" inType="win:UnicodeString" outType="xs:string" />
      <data name="Name" inType="win:UnicodeString" outType="xs:string" />
      <data name="Type" inType="win:UnicodeString" outType="xs:string" />
      <data name="Destination" inType="win:UnicodeString" outType="xs:string" />
    </event>
    <event name="SYSMON_WMI_BINDING" value="21" level="Informational" template="WmiEventConsumerToFilter activity detected" rulename="WmiEvent" ruledefault="exclude" version="3">
      <data name="RuleName" inType="win:UnicodeString" outType="xs:string" />
      <data name="EventType" inType="win:UnicodeString" outType="xs:string" />
      <data name="UtcTime" inType="win:UnicodeString" outType="xs:string" />
      <data name="Operation" inType="win:UnicodeString" outType="xs:string" />
      <data name="User" inType="win:UnicodeString" outType="xs:string" />
      <data name="Consumer" inType="win:UnicodeString" outType="xs:string" />
      <data name="Filter" inType="win:UnicodeString" outType="xs:string" />
    </event>
  </events>
</manifest>
'@

    $schema = [xml]"$schemaTxt"

    if ($PSBoundParameters.Count -eq 0)
    {
        return $schema.manifest.events | Select-Object -ExpandProperty event
    }

    if ($PSBoundParameters.ContainsKey("Fullname"))
    {
        return $schema.manifest.events.event | Where-Object template -like $FullName
    }

    if ($PSBoundParameters.ContainsKey("RuleName"))
    {
        return $schema.manifest.events.event | Where-Object rulename -like $RuleName
    }

    if ($PSBoundParameters.ContainsKey("Name"))
    {
        return $schema.manifest.events.event | Where-Object name -like $Name
    }

    if ($PSBoundParameters.ContainsKey("Id"))
    {
        return $schema.manifest.events.event | Where-Object value -eq $Id
    }
}

function Get-SysmonProcessCreate
{
[cmdletbinding(
    DefaultParameterSetName='byMinutes'
)]
Param(
    [Parameter(ParameterSetName='byMinutes')]
    [Parameter(ParameterSetName='Filtered')]
    [int]
    $MinutesAgo
    ,
    [Parameter(ParameterSetName='byHours')]
    [Parameter(ParameterSetName='Filtered')]
    [int]
    $HoursAgo
    ,
    [Parameter(ParameterSetName='byDays')]
    [Parameter(ParameterSetName='Filtered')]
    [int]
    $DaysAgo
    ,
    [Parameter(ParameterSetName='Filtered')]
    [hashtable]
    $Filter
)

$invokeGetEvents = @{
    EventType = "CREATE_PROCESS"
}

$invokeGetEvents += $PSBoundParameters

Get-SysmonEvent @invokeGetEvents
}

function ConvertFrom-SysmonBinaryConfiguration {
<#
.SYNOPSIS
Parses a binary Sysmon configuration.
.DESCRIPTION
ConvertFrom-SysmonBinaryConfiguration parses a binary Sysmon configuration. The configuration is typically stored in the registry at the following path: HKLM\SYSTEM\CurrentControlSet\Services\SysmonDrv\Parameters\Rules
ConvertFrom-SysmonBinaryConfiguration currently only supports the following schema versions: 3.30, 3.40
Author: Matthew Graeber (@mattifestation)
License: BSD 3-Clause
.PARAMETER RuleBytes
Specifies the raw bytes of a Sysmon configuration from the registry.
.EXAMPLE
[Byte[]] $RuleBytes = Get-ItemPropertyValue -Path HKLM:\SYSTEM\CurrentControlSet\Services\SysmonDrv\Parameters -Name Rules
ConvertFrom-SysmonBinaryConfiguration -RuleBytes $RuleBytes
.OUTPUTS
Sysmon.EventCollection
Output a fully-parsed rule object including the hash of the rules blob.
.NOTES
ConvertFrom-SysmonBinaryConfiguration is designed to serve as a helper function for Get-SysmonConfiguration.
#>

    [OutputType('Sysmon.EventCollection')]
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $True)]
        [Byte[]]
        [ValidateNotNullOrEmpty()]
        $RuleBytes
    )

    #region Define byte to string mappings. This may change across verions.
    $SupportedSchemaVersions = @(
        [Version] '3.30.0.0',
        [Version] '3.40.0.0',
        [Version] '4.00.0.0',
        [Version] '4.10.0.0'
    )

    $EventConditionMapping = @{
        0 = 'Is'
        1 = 'IsNot'
        2 = 'Contains'
        3 = 'Excludes'
        4 = 'BeginWith'
        5 = 'EndWith'
        6 = 'LessThan'
        7 = 'MoreThan'
        8 = 'Image'
    }

    # The following value to string mappings were all pulled from
    # IDA and will require manual validation with with each new
    # Sysmon and schema version. Here's hoping they don't change often!
    $ProcessCreateMapping = @{
        0 = 'UtcTime'
        1 = 'ProcessGuid'
        2 = 'ProcessId'
        3 = 'Image'
        4 = 'CommandLine'
        5 = 'CurrentDirectory'
        6 = 'User'
        7 = 'LogonGuid'
        8 = 'LogonId'
        9 = 'TerminalSessionId'
        10 = 'IntegrityLevel'
        11 = 'Hashes'
        12 = 'ParentProcessGuid'
        13 = 'ParentProcessId'
        14 = 'ParentImage'
        15 = 'ParentCommandLine'
    }

    $ProcessCreateMapping_4_00 = @{
        0 = 'UtcTime'
        1 = 'ProcessGuid'
        2 = 'ProcessId'
        3 = 'Image'
        4 = 'FileVersion'
        5 = 'Description'
        6 = 'Product'
        7 = 'Company'
        8 = 'CommandLine'
        9 = 'CurrentDirectory'
        10 = 'User'
        11 = 'LogonGuid'
        12 = 'LogonId'
        13 = 'TerminalSessionId'
        14 = 'IntegrityLevel'
        15 = 'Hashes'
        16 = 'ParentProcessGuid'
        17 = 'ParentProcessId'
        18 = 'ParentImage'
        19 = 'ParentCommandLine'
    }

    $FileCreateTimeMapping = @{
        0 = 'UtcTime'
        1 = 'ProcessGuid'
        2 = 'ProcessId'
        3 = 'Image'
        4 = 'TargetFilename'
        5 = 'CreationUtcTime'
        6 = 'PreviousCreationUtcTime'
    }

    $NetworkConnectMapping = @{
        0 = 'UtcTime'
        1 = 'ProcessGuid'
        2 = 'ProcessId'
        3 = 'Image'
        4 = 'User'
        5 = 'Protocol'
        6 = 'Initiated'
        7 = 'SourceIsIpv6'
        8 = 'SourceIp'
        9 = 'SourceHostname'
        10 = 'SourcePort'
        11 = 'SourcePortName'
        12 = 'DestinationIsIpv6'
        13 = 'DestinationIp'
        14 = 'DestinationHostname'
        15 = 'DestinationPort'
        16 = 'DestinationPortName'
    }

    $SysmonServiceStateChangeMapping = @{
        0 = 'UtcTime'
        1 = 'State'
        2 = 'Version'
        3 = 'SchemaVersion'
    }

    $ProcessTerminateMapping = @{
        0 = 'UtcTime'
        1 = 'ProcessGuid'
        2 = 'ProcessId'
        3 = 'Image'
    }

    $DriverLoadMapping = @{
        0 = 'UtcTime'
        1 = 'ImageLoaded'
        2 = 'Hashes'
        3 = 'Signed'
        4 = 'Signature'
        5 = 'SignatureStatus'
    }

    $ImageLoadMapping = @{
        0 = 'UtcTime'
        1 = 'ProcessGuid'
        2 = 'ProcessId'
        3 = 'Image'
        4 = 'ImageLoaded'
        5 = 'Hashes'
        6 = 'Signed'
        7 = 'Signature'
        8 = 'SignatureStatus'
    }

    $ImageLoadMapping_4_00 = @{
        0 = 'UtcTime'
        1 = 'ProcessGuid'
        2 = 'ProcessId'
        3 = 'Image'
        4 = 'ImageLoaded'
        5 = 'FileVersion'
        6 = 'Description'
        7 = 'Product'
        8 = 'Company'
        9 = 'Hashes'
        10 = 'Signed'
        11 = 'Signature'
        12 = 'SignatureStatus'
    }

    $CreateRemoteThreadMapping = @{
        0 = 'UtcTime'
        1 = 'SourceProcessGuid'
        2 = 'SourceProcessId'
        3 = 'SourceImage'
        4 = 'TargetProcessGuid'
        5 = 'TargetProcessId'
        6 = 'TargetImage'
        7 = 'NewThreadId'
        8 = 'StartAddress'
        9 = 'StartModule'
        10 = 'StartFunction'
    }

    $RawAccessReadMapping = @{
        0 = 'UtcTime'
        1 = 'ProcessGuid'
        2 = 'ProcessId'
        3 = 'Image'
        4 = 'Device'
    }

    $ProcessAccessMapping = @{
        0 = 'UtcTime'
        1 = 'SourceProcessGUID'
        2 = 'SourceProcessId'
        3 = 'SourceThreadId'
        4 = 'SourceImage'
        5 = 'TargetProcessGUID'
        6 = 'TargetProcessId'
        7 = 'TargetImage'
        8 = 'GrantedAccess'
        9 = 'CallTrace'
    }

    $FileCreateMapping = @{
        0 = 'UtcTime'
        1 = 'ProcessGuid'
        2 = 'ProcessId'
        3 = 'Image'
        4 = 'TargetFilename'
        5 = 'CreationUtcTime'
    }

    $RegistryEventCreateKeyMapping = @{
        0 = 'EventType'
        1 = 'UtcTime'
        2 = 'ProcessGuid'
        3 = 'ProcessId'
        4 = 'Image'
        5 = 'TargetObject'
    }

    $RegistryEventSetValueMapping = @{
        0 = 'EventType'
        1 = 'UtcTime'
        2 = 'ProcessGuid'
        3 = 'ProcessId'
        4 = 'Image'
        5 = 'TargetObject'
        6 = 'Details'
    }

    $RegistryEventDeleteKeyMapping = @{
        0 = 'EventType'
        1 = 'UtcTime'
        2 = 'ProcessGuid'
        3 = 'ProcessId'
        4 = 'Image'
        5 = 'TargetObject'
        6 = 'NewName'
    }

    $FileCreateStreamHashMapping = @{
        0 = 'UtcTime'
        1 = 'ProcessGuid'
        2 = 'ProcessId'
        3 = 'Image'
        4 = 'TargetFilename'
        5 = 'CreationUtcTime'
        6 = 'Hash'
    }

    $SysmonConfigurationChangeMapping = @{
        0 = 'UtcTime'
        1 = 'Configuration'
        2 = 'ConfigurationFileHash'
    }

    $PipeEventCreatedMapping = @{
        0 = 'UtcTime'
        1 = 'ProcessGuid'
        2 = 'ProcessId'
        3 = 'PipeName'
        4 = 'Image'
    }

    $PipeEventConnectedMapping = @{
        0 = 'UtcTime'
        1 = 'ProcessGuid'
        2 = 'ProcessId'
        3 = 'PipeName'
        4 = 'Image'
    }

    $WmiEventFilterMapping = @{
        0 = 'EventType'
        1 = 'UtcTime'
        2 = 'Operation'
        3 = 'User'
        4 = 'EventNamespace'
        5 = 'Name'
        6 = 'Query'
    }

    $WmiEventConsumerMapping = @{
        0 = 'EventType'
        1 = 'UtcTime'
        2 = 'Operation'
        3 = 'User'
        4 = 'Name'
        5 = 'Type'
        6 = 'Destination'
    }

    $WmiEventConsumerToFilterMapping = @{
        0 = 'EventType'
        1 = 'UtcTime'
        2 = 'Operation'
        3 = 'User'
        4 = 'Consumer'
        5 = 'Filter'
    }

    $EventTypeMapping = @{
        1  = @('ProcessCreate', $ProcessCreateMapping)
        2  = @('FileCreateTime', $FileCreateTimeMapping)
        3  = @('NetworkConnect', $NetworkConnectMapping)
        # SysmonServiceStateChange is not actually present in the schema. It is here for the sake of completeness.
        4  = @('SysmonServiceStateChange', $SysmonServiceStateChangeMapping)
        5  = @('ProcessTerminate', $ProcessTerminateMapping)
        6  = @('DriverLoad', $DriverLoadMapping)
        7  = @('ImageLoad', $ImageLoadMapping)
        8  = @('CreateRemoteThread', $CreateRemoteThreadMapping)
        9  = @('RawAccessRead', $RawAccessReadMapping)
        10 = @('ProcessAccess', $ProcessAccessMapping)
        11 = @('FileCreate', $FileCreateMapping)
        12 = @('RegistryEventCreateKey', $RegistryEventCreateKeyMapping)
        13 = @('RegistryEventSetValue', $RegistryEventSetValueMapping)
        14 = @('RegistryEventDeleteKey', $RegistryEventDeleteKeyMapping)
        15 = @('FileCreateStreamHash', $FileCreateStreamHashMapping)
        # SysmonConfigurationChange is not actually present in the schema. It is here for the sake of completeness.
        16 = @('SysmonConfigurationChange', $SysmonConfigurationChangeMapping)
        17 = @('PipeEventCreated', $PipeEventCreatedMapping)
        18 = @('PipeEventConnected', $PipeEventConnectedMapping)
        19 = @('WmiEventFilter', $WmiEventFilterMapping)
        20 = @('WmiEventConsumer', $WmiEventConsumerMapping)
        21 = @('WmiEventConsumerToFilter', $WmiEventConsumerToFilterMapping)
    }
    #endregion

    $RuleMemoryStream = New-Object -TypeName System.IO.MemoryStream -ArgumentList @(,$RuleBytes)

    $RuleReader = New-Object -TypeName System.IO.BinaryReader -ArgumentList $RuleMemoryStream

    # I'm noting here for the record that parsing could be slightly more robust to account for malformed
    # rule blobs. I'm writing this in my spare time so I likely won't put too much work into increased
    # parsing robustness.

    if ($RuleBytes.Count -lt 16) {
        $RuleReader.Dispose()
        $RuleMemoryStream.Dispose()
        throw 'Insufficient length to contain a Sysmon rule header.'
    }

    # This value should be either 0 or 1. 1 should be expected for a current Sysmon config.
    # A value of 1 indicates that offset 8 will contain the file offset to the first rule grouping.
    # A value of 0 should indicate that offset 8 will be the start of the first rule grouping.
    # Currently, I am just going to check that the value is 1 and throw an exception if it's not.
    $HeaderValue0 = $RuleReader.ReadUInt16()

    if ($HeaderValue0 -ne 1) {
        $RuleReader.Dispose()
        $RuleMemoryStream.Dispose()
        throw "Incorrect header value at offset 0x00. Expected: 1. Actual: $HeaderValue0"
    }

    # This value is expected to be 1. Any other value will indicate the presence of a "registry rule version"
    # that is incompatible with the current Sysmon schema version. A value other than 1 likely indicates the
    # presence of an old version of Sysmon. Any value besides 1 will not be supported in this script.
    $HeaderValue1 = $RuleReader.ReadUInt16()

    if ($HeaderValue1 -ne 1) {
        $RuleReader.Dispose()
        $RuleMemoryStream.Dispose()
        throw "Incorrect header value at offset 0x02. Expected: 1. Actual: $HeaderValue1"
    }

    $RuleGroupCount = $RuleReader.ReadUInt32()
    $RuleGroupBeginOffset = $RuleReader.ReadUInt32()

    $SchemaVersionMinor = $RuleReader.ReadUInt16()
    $SchemaVersionMajor = $RuleReader.ReadUInt16()

    $SchemaVersion = New-Object -TypeName System.Version -ArgumentList $SchemaVersionMajor, $SchemaVersionMinor, 0, 0

    Write-Verbose "Obtained the following schema version: $($SchemaVersion.ToString(2))"

    if (-not ($SupportedSchemaVersions -contains $SchemaVersion)) {
        $RuleReader.Dispose()
        $RuleMemoryStream.Dispose()
        throw "Unsupported schema version: $($SchemaVersion.ToString(2)). Schema version must be at least $($MinimumSupportedSchemaVersion.ToString(2))"
    }

    #region Perform offset updates depending upon the schema version here
    # This logic should be the first candidate for refactoring should the schema change drastically in the future.
    switch ($SchemaVersion.ToString(2)) {
        '4.0' {
            Write-Verbose 'Using schema version 4.00 updated offsets.'
            # ProcessCreate and ImageLoad values changed
            $EventTypeMapping[1][1] = $ProcessCreateMapping_4_00
            $EventTypeMapping[7][1] = $ImageLoadMapping_4_00
        }
    }
    #endregion

    $null = $RuleReader.BaseStream.Seek($RuleGroupBeginOffset, 'Begin')

    $EventCollection = for ($i = 0; $i -lt $RuleGroupCount; $i++) {
        $EventTypeValue = $RuleReader.ReadInt32()
        $EventType = $EventTypeMapping[$EventTypeValue][0]
        $EventTypeRuleTypes = $EventTypeMapping[$EventTypeValue][1]
        $OnMatchValue = $RuleReader.ReadInt32()

        $OnMatch = $null

        switch ($OnMatchValue) {
            0 { $OnMatch = 'Exclude' }
            1 { $OnMatch = 'Include' }
            default { $OnMatch = '?' }
        }

        $NextEventTypeOffset = $RuleReader.ReadInt32()
        $RuleCount = $RuleReader.ReadInt32()
        [PSObject[]] $Rules = New-Object -TypeName PSObject[]($RuleCount)

        # Parse individual rules here
        for ($j = 0; $j -lt $RuleCount; $j++) {
            $RuleType = $EventTypeRuleTypes[$RuleReader.ReadInt32()]
            $Filter = $EventConditionMapping[$RuleReader.ReadInt32()]
            $NextRuleOffset = $RuleReader.ReadInt32()
            $RuleTextLength = $RuleReader.ReadInt32()
            $RuleTextBytes = $RuleReader.ReadBytes($RuleTextLength)
            $RuleText = [Text.Encoding]::Unicode.GetString($RuleTextBytes).TrimEnd("`0")

            $Rules[$j] = [PSCustomObject] @{
                PSTypeName = 'Sysmon.Rule'
                RuleType = $RuleType
                Filter = $Filter
                RuleText = $RuleText
            }

            $null = $RuleReader.BaseStream.Seek($NextRuleOffset, 'Begin')
        }

        [PSCustomObject] @{
            PSTypeName = 'Sysmon.EventGroup'
            EventType = $EventType
            OnMatch = $OnMatch
            Rules = $Rules
        }

        $null = $RuleReader.BaseStream.Seek($NextEventTypeOffset, 'Begin')
    }

    $RuleReader.Dispose()
    $RuleMemoryStream.Dispose()

    # Calculate the hash of the binary rule blob
    $SHA256Hasher = New-Object -TypeName System.Security.Cryptography.SHA256CryptoServiceProvider
    $ConfigBlobSHA256Hash = ($SHA256Hasher.ComputeHash($RuleBytes) | ForEach-Object { $_.ToString('X2') }) -join ''

    $sysmon = [PSCustomObject] @{
        PSTypeName = 'Sysmon.EventCollection'
        SchemaVersion = $SchemaVersion
        ConfigBlobSHA256Hash = $ConfigBlobSHA256Hash
        Events = $EventCollection
    }
}

function Get-EventLogBookmark
{
[cmdletbinding()]
Param(
    [Parameter(ParameterSetName="byRecord")]
    [int]
    $RecordId
    ,
    [Parameter(ParameterSetName="byId")]
    [int]
    $EventId
    ,
    [string]
    $EventLogName
)
#<Select Path="Microsoft-Windows-Sysmon/Operational">*[System[(EventID=$RecordId)]]</Select>

$xmlFilter = @"
<QueryList>
  <Query Id="0" Path="Microsoft-Windows-Sysmon/Operational">
    
  </Query>
</QueryList>
"@

$xml = [xml]"$xmlFilter"

$xml.QueryList.Query.Path = $EventLogName

$selectElement = Get-EventLogSelectElement @PSBoundParameters

$newNOde = $xml.ImportNode($selectElement, $true)

$null = $xml.QueryList.Query.AppendChild($newNOde)

$event = Get-WinEvent -FilterXml $xml

if ($null -ne $event)
{
    $event.Bookmark
}

}

function Get-EventLogSelectElement
{
[cmdletbinding()]
Param(
    [string]
    $EventLogName
    ,
    [string]
    $EventId
    ,
    [string]
    $RecordId
)
    #$PSBoundParameters
    $xml = [xml]""

    if ($PSBoundParameters.ContainsKey("EventId"))
    {
        $queryAttribute = "EventID"
        $queryValue = $EventId
    }

    if ($PSBoundParameters.ContainsKey("RecordId"))
    {
        $queryAttribute = "EventRecordID"
        $queryValue = $RecordId
    }

    $element = $xml.CreateElement("Select")
    
    $element.SetAttribute("Path",$EventLogName)
    $element.InnerText = "[System[($queryAttribute=$queryValue)]]".Insert(0,'*')
    $element

}


$bookMarkXML = @"
<BookmarkList Direction='backward'>
  <Bookmark Channel='Microsoft-Windows-Sysmon/Operational' RecordId='69815' IsCurrent='true'/>
</BookmarkList>
"@

$c = [System.Diagnostics.Eventing.Reader.EventBookmark].UnderlyingSystemType.GetConstructors([System.Reflection.BindingFlags]"public,NonPublic,Instance")
$ctor = $c[0]
$book2 = [System.Diagnostics.Eventing.Reader.EventBookmark]$ctor.Invoke("$bookMarkXML")
#$bookReflection = $book2.GetType().GetProperties([System.Reflection.BindingFlags]"public,NonPublic,Instance")
#$bookReflection[0].GetValue($book2)

$emptyQueryXML = @'
<QueryList>
  <Query Id="0" Path="Microsoft-Windows-Sysmon/Operational">
    <Select Path="Microsoft-Windows-Sysmon/Operational">*</Select>
  </Query>
</QueryList>
'@

$evenLogQuery = [System.Diagnostics.Eventing.Reader.EventLogQuery]::new((Get-SysmonEventLogName), [System.Diagnostics.Eventing.Reader.PathType]::LogName,$emptyQueryXML)
$eventReader = [System.Diagnostics.Eventing.Reader.EventLogReader]::new($evenLogQuery, $book2)

$all = foreach($int in (0..1000))
{
    $log = $eventReader.ReadEvent()
    if ($null -eq $log)
    {
        break
    }
    #"$int - $($log.RecordId)"
    $xmlEvent = [xml]$log.ToXml()
    $props = $xmlEvent.Event.EventData.Data.Name
    $counter = 0
    $msg = foreach($p in $props)
    {
        "$p" + ":" + $log.Properties[$counter].Value.ToString().TrimStart(" ")
        $counter++
    }
    $log | Add-Member -MemberType NoteProperty -Name Message -Value $msg -PassThru #$log.FormatDescription() -PassThru
}