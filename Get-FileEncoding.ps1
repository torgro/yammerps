﻿function Get-FileEncoding
{
<#
    .SYNOPSIS
      Gets file encoding.
      
    .DESCRIPTION
      The Get-FileEncoding function determines encoding by looking at Byte Order Mark (BOM).
      Based on port of C# code from http://www.west-wind.com/Weblog/posts/197245.aspx and http://poshcode.org/2059 by Chad Miller

    .PARAMETER Path
      A string value indicating the path to the file you want to get the encoding of

    .EXAMPLE
      Get-ChildItem  *.ps1 | Get-FileEncoding | where {$_.Encoding -ne 'ASCII'}
      This command gets ps1 files in current directory where encoding is not ASCII

    .EXAMPLE
      Get-ChildItem  *.ps1 | Get-FileEncoding | where {$_.Encoding -ne 'ASCII'} | foreach {(get-content $_.Path) | set-content $_.Path -Encoding ASCII}

      Same as previous example but fixes encoding using set-content

    .NOTES
      @toregroneng, tore.groneng@gmail.com
#>
[CmdletBinding()] 
Param (
    [Parameter(Mandatory = $True, ValueFromPipelineByPropertyName = $True)]
    [Alias("PSpath","FullName")]
    [string[]]
    $Path
    ,
    [ValidateSet("UTF8","Unicode","UTF32","ASCII")]
    [string]
    $Encoding
)
    BEGIN
    {
        $f = $MyInvocation.InvocationName
        Write-Verbose -Message "$f - START"
    }

    PROCESS
    {
        foreach($FilePath in $path)
        {
            if(-not(Test-Path -Path $FilePath))
            {
                Write-Error -Message "Path $FilePath not found" -ErrorAction Stop                
            }

            $out = "" | Select-Object Encoding, Path

            [byte[]]$byte = Get-Content -Encoding byte -ReadCount 4 -TotalCount 4 -Path $FilePath

            if ($byte[0] -eq 0xef -and $byte[1] -eq 0xbb -and $byte[2] -eq 0xbf )
            {
                $out.Encoding = "UTF8"
            }
            elseif ($byte[0] -eq 0xfe -and $byte[1] -eq 0xff)
            {
                $out.Encoding = "Unicode"
            }
            elseif ($byte[0] -eq 0 -and $byte[1] -eq 0 -and $byte[2] -eq 0xfe -and $byte[3] -eq 0xff)
            {
                $out.Encoding = "UTF32"
            }
            elseif ($byte[0] -eq 0x2b -and $byte[1] -eq 0x2f -and $byte[2] -eq 0x76)
            {
                $out.Encoding = "UTF7"
            }
            else
            { 
                $out.Encoding = "ASCII"
            }

            $out.Path = Resolve-Path -Path $FilePath

            if ($Encoding)
            {
                if ($out.Encoding -eq $Encoding)
                {
                    Write-Output $out
                }
            }
            else
            {
                Write-Output $out
            }            
        }
    }

    END
    {
        Write-Verbose -Message "$f - END"
    }
    
}