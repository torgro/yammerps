﻿function Get-ADSIADGroup
{
[cmdletbinding()]
Param(
    [string]
    $Identity = "LanternenSkypeBrukere"
)

    $GroupSearcher = [adsisearcher]"(samaccountname=$Identity)"
    $groupObj = $GroupSearcher.FindOne()

    if ($null -eq $groupObj)
    {
        Write-Error -Message "Unable to find group [$Identity]"
    }

    $out = [pscustomobject]@{
        distinguishedname = $groupObj.Properties.distinguishedname.Item(0)
        dn = $groupObj.Properties.distinguishedname.Item(0)
        member = $groupObj.Properties.member
        name = $groupObj.Properties.name.Item(0)
        cn = $groupObj.Properties.cn.Item(0)
        samaccountname = $groupObj.Properties.samaccountname.Item(0)
    }

    $out

}

function Get-ADSIADuser
{
[cmdletbinding()]
Param(
    [string]
    $Identity = $env:USERNAME
)

    $searcher = [adsisearcher]"(samaccountname=$Identity)"
    $userObj = $searcher.FindOne()

    if ($null -eq $userObj -or $null -eq $searcher)
    {
        Write-Error -Message "Unable to find user [$Identity]"
    }

    $out = [pscustomobject]@{
        distinguishedname = $userObj.Properties.distinguishedname.Item(0)
        dn = $userObj.Properties.distinguishedname.Item(0)
        name = $userObj.Properties.name.Item(0)
        cn = $userObj.Properties.cn.Item(0)
        samaccountname = $userObj.Properties.samaccountname.Item(0)
        proxyaddresses = $userObj.Properties.proxyaddresses
    }

     $out
}