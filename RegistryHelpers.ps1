﻿# ====================================================================================================0
# Create by : Tore Grøneng (Tore@firstpoint.no @ToreGroneng Tore.groneng@gmail.com
# Created on: Feb 2014
# Version   : 1.0
#
# ChangeLog : 1.0 Initial release
#
#
#
# ====================================================================================================0

#Function Global:Write-Verbose
Function Global:MyWrite-Verbose
{
[cmdletbinging()]
Param(
        [string] $Message = ""
)
    #Implement your loging logic here
}


Function Get-RegistryHive
{
[cmdletbinding()]
Param(
        [ValidateSet("LocalMachine","Users","CurrentUser", "CurrentConfig", "ClassesRoot")]
        [String] $Hive = "LocalMachine"
    )

    $f = $MyInvocation.MyCommand.name
    $HiveObject = $null
    $ErrorActionPreference = "stop"

    try
    {
        Write-verbose "Creating .net object and connecting to hive '$hive'"
        $HiveObject = [Microsoft.Win32.RegistryKey]::OpenBaseKey($Hive, "default")
    }
    catch
    {
         Write-Verbose "$($_.Exception.Message)"
         $_.Exception.message
    }
    finally
    {
         $ErrorActionPreference = "Continue"
    }

    $HiveObject
}


function Get-RegistryKey
{
[cmdletbinding()]
Param(
        [Parameter(
            Mandatory=$true,
            ValueFromPipeline=$true,
            ValueFromPipelineByPropertyName=$true)
        ]
        [String[]] $RegPath = ""
        ,
        [ValidateSet("LocalMachine","Users","CurrentUser", "CurrentConfig", "ClassesRoot")]
        [String] $Hive = "LocalMachine"
       
      )

BEGIN
{
    $f = $MyInvocation.MyCommand.name
    [string] $LoggMsg = ""
    Write-Verbose "Begin $f"
}

PROCESS
{
    foreach ($path in $RegPath)
    {
        $HiveObject = Get-RegistryHive -Hive $hive
        $key = $HiveObject.OpenSubkey($path)
        if ($key -ne $null)
        {
            $key
        }
        else
        {
            $LoggMsg = "Error - Could not find path '$($HiveObject.Name)\$RegPath'"
            Write-Verbose $LoggMsg
            throw($LoggMsg)
        }
    }
}

END
{
    Write-Verbose "END $f"
}
}


Function Get-RegistryValue
{
[cmdletbinding()]
Param(
    [Parameter(
        Mandatory=$true,
        ValueFromPipeline=$true,
        ValueFromPipelineByPropertyName=$true)
    ]
    [String] $RegPath = ""
    ,
    [ValidateSet("LocalMachine","Users","CurrentUser", "CurrentConfig", "ClassesRoot")]
    [String] $Hive = "LocalMachine"
    ,
    [Parameter(Mandatory=$true)]
    [String[]] $RegValueName = ""
    ,
    [string] $computername = ""
)
BEGIN
{
    $f = $MyInvocation.MyCommand.name
    [string] $LoggMsg = ""
    Write-Verbose "Begin $f"
}

PROCESS
{
    $ErrorActionPreference = "stop"

    Try
    {
        if ($computername -eq "")
        {
            Write-Verbose "Assuming local registry query"
            $key = Get-RegistryKey -Hive $hive -RegPath $RegPath
            foreach($keyValue in $RegValueName)
            {
                $output = "" | select Name, Value
                $output.value = $key.GetValue($keyValue)
                $output.name = $keyValue
                $output
            }
        }
        else
        {
            Write-Verbose "Assuming remote registry query"
            Write-Verbose "Getting helper-functions and creating command"
            
            [string] $functions = Get-Fullcommand -Name "Get-RegistryHive"
            $Functions += Get-Fullcommand -Name "Get-RegistryKey"
            $Functions += Get-Fullcommand -Name "Get-RegistryValue"
           
            [string] $Command = "Get-RegistryValue -Hive $hive -RegPath '$RegPath' -RegValueName $RegValueName"

            if ($VerbosePreference -ne "SilentlyContinue")
            {
                Write-Verbose "VerbosePreferance is turned on, adding verbose option"
                $command += " -verbose"
            }
            else
            {
                Write-Verbose "VerbosePreferance is turned OFF"
            }

            Write-Verbose "Command is: $command"
            
            $sb = [scriptblock]::Create("$functions $command")
            
            Write-Verbose "Invoking command on computer '$computername'"
            Invoke-Command -ComputerName $computername -ScriptBlock $sb
        }
    }
    catch
    {
         Write-Verbose "$($_.Exception.Message)"
         $_.Exception.message
    }
    finally
    {
        $ErrorActionPreference = "Continue"
    }
 
}

END
{
}

}


function Get-Fullcommand
{
[cmdletbinding()]
Param(
        [Parameter(Mandatory=$true)]
        [string] $Name
)
    $ErrorActionPreference = "stop"
    [String] $FullCommand = ""

    try
    {
        [string]$FunctionBody = (Get-Command -Name $Name).ScriptBlock.ToString()

        if ($FunctionBody -ne $null -and $FunctionBody -ne "")
        {
            $FullCommand = "Function $name { $FunctionBody }"
        }
        else
        {
            [string] $msg = "Could not find command '$name'"
            Write-Verbose $msg
            throw($msg)
        }
    }
    Catch
    {
         Write-Verbose "$($_.Exception.Message)"
         $_.Exception.message
    }
    Finally
    {
        $ErrorActionPreference = "Continue"
    }
    $FullCommand
}


#Not used
Function Get-RegistryPath
{
[cmdletbinding()]
Param(
    [Parameter(
        Mandatory=$true,
        ValueFromPipeline=$true,
        ValueFromPipelineByPropertyName=$true)
    ]
    [object[]] $HiveObject
    ,
    [Parameter(Mandatory=$true)]
    [String] $RegPath = ""
)

BEGIN
{
    $f = $MyInvocation.MyCommand.name
    [string] $LoggMsg = ""
    Write-Verbose "Begin $f"
}

PROCESS
{
    foreach ($hive in $HiveObject)
    {
        $key = $hive.OpenSubkey($RegPath)
        
        if ($key -ne $null)
        {
            Write-Verbose "Key is: $($key.Name)"
            Write-Verbose "Getting ValueNames"
            $ValueArray = $key.GetValueNames()
            if (($ValueArray | Measure-Object).Count -gt 0)
            {
                Write-Verbose "Have values in path, enumerating values"
                foreach($KeyName in $ValueArray)
                {
                    $outPut = "" | Select Name, Value
                    $output.Name = $KeyName
                    $output.Value = $key.GetValue($KeyName)
                    $output
                }
                Write-Verbose "Done enumerating"
            }
            else
            {
                $LoggMsg = "'$($HiveObject.Name)\$RegPath' does not contain any values"
                write-Verbose $LoggMsg
                throw($LoggMsg)
            }
        }
        else
        {
            $LoggMsg = "Error - Could not find path '$($HiveObject.Name)\$RegPath'"
            Write-Verbose $LoggMsg
            throw($LoggMsg)
        }
    }
}

END
{
    Write-Verbose "END $f"
}

}