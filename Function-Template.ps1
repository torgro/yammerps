function New-FunctionTemplate
{
<#
.SYNOPSIS 
    This is a template for a new function
.Description
    Short description
.Parameter Name
    Give me a name
.Parameter Number
    Give me a number
.Parameter Boolean
    Give me a Boolean
.EXAMPLE
    New-FunctionTemplate -Name "My name"
    Explain what the functiong does
.EXAMPLE
    New-FunctionTemplate -Name "My name" -number 2323 -Boolean $true
    Explain what the functiong does
.Notes
    Who made me, why and contact information
#>
[cmdletbinding()]
Param(
        [Parameter(Mandatory=$true)]
        [string]$Name
        ,
        [int]$Number
        ,
        [bool]$Boolean
)
    $f = $MyInvocation.MyCommand.Name
    Write-Verbose"$f - START"
 
    $ErrorActionPreference= "STOP"
    try
    {
        # type code here
    }
    catch
    {
        $_.Exception.Message
    }
    finally
    {
        $ErrorActionPreference= "Continue"
    }
    # Return data here
}  
