﻿$code = @'
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Numerics;

public static class PrimeCalc
{
    public static BigInteger zero = BigInteger.Zero;
    static BigInteger one = BigInteger.One;
    static BigInteger two = new BigInteger(2);
    static BigInteger four = new BigInteger(4);

    private static bool isMersennePrime(int p)
    {
        if (p % 2 == 0) return (p == 2);
        else
        {
            for (int i = 3; i <= (int)Math.Sqrt(p); i += 2)
                if (p % i == 0) return false; //not prime
            BigInteger m_p = BigInteger.Pow(two, p) - one;
            BigInteger s = four;
            for (int i = 3; i <= p; i++)
                s = (s * s - two) % m_p;
            return s == zero;
            
        }
    }

    public static int[] GetMersennePrimeNumbers(int upTo)
    {
        List<int> response = new List<int>();
        Parallel.For(2, upTo, i =>
        {
            if (isMersennePrime(i)) response.Add(i);
        });
        //for (int i = 0; i < 1024; i++)
        //{
        //    if (isMersennePrime(i)) response.Add(i);
        //}
        response.Sort();
        return response.ToArray();
    }

}
'@

#Add-Type -AssemblyName System.Numerics

Add-Type -TypeDefinition $code -Language CSharp -ReferencedAssemblies System.Numerics

