﻿<#
.SYNOPSIS 
    Binary clock
.Description
    Shows the time in binary format. Make no mistake, it is uttery useless!
.Notes
    @ToreGroneng tore.groneng@gmail.com #Just4Fun
#>

while($true)
{
    $hour = [convert]::ToString((get-date).Hour,16)
    $min = [convert]::ToString((get-date).Minute,16)
    $sec = [convert]::ToString((get-date).Second,16)
    Clear-Host
    "$hour`:$min`:$sec"
    Start-Sleep -Seconds 1
}