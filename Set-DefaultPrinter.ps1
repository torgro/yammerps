﻿function Set-DefaultPrinter
{
[cmdletbinding()]
Param(
    [Parameter(
        ValueFromPipelineByPropertyName
    )]
    [string]
    $Name
    ,
    [string]
    $ComputerName
    ,
    [Microsoft.Management.Infrastructure.CimSession]
    $CimSession
)

    $GetPrinter = @{}

    if (-not [string]::IsNullOrEmpty($ComputerName))
    {
        $GetPrinter.ComputerName = $ComputerName
    }

    if ($null -ne $CimSession)
    {
        $GetPrinter.CimSession = $CimSession
    }

    $printer = Get-CimInstance -ClassName "Win32_Printer" @GetPrinter | Where-Object Name -like $Name
    
    if (($printer | Measure-Object).Count -gt 1)
    {
        Write-Error -Message "Multiple printers found" -ErrorAction Stop
    }

    if ($null -ne $printer)
    {
        Write-Verbose -Message "Setting default printer to [$($printer.Name)]"

        $null = Invoke-CimMethod -InputObject $printer -MethodName SetDefaultPrinter        
    }
}