﻿    function Validate-Credential
    {
    <#
    .Synopsis
       Validate domain, machine or application credential objects 
    .DESCRIPTION
       Validates a credential object against local machine, a domain or an ApplicationDirectoryType. It outputs a boolean
       value where a successfull authentication returns True
    .EXAMPLE
       Validate against local machine
       Validate-Credential -Credential (Get-Credential) -MachineType
       True
    .EXAMPLE
       Validate against domain
       Validate-Credential -Credential (Get-Credential) -DomainType
       True
    .EXAMPLE
       Validate against local machine
       $cred = Get-Credential
       Validate-Credential -Credential $cred -DomainType
       True
    .INPUTS
       [pscredential]
    .OUTPUTS
       [bool]
    .NOTES
       Use at your own Risk!
    .COMPONENT
       General scripting
    .ROLE
       Credentials validation
    .FUNCTIONALITY
       Validating credential objects against local machine, domain or applications
    #>
    [cmdletbinding()]
    Param(
        [pscredential]$Credential
        ,
        [switch]$MachineType
        ,
        [switch]$DomainType
        ,
        [switch]$ApplicationDirectoryType
    )
        Try 
        { 
            Add-Type -AssemblyName System.DirectoryServices.AccountManagement

            if ($MachineType)
            { 
                $ContextType = [System.DirectoryServices.AccountManagement.ContextType]::Machine
            }

            if ($DomainType)
            { 
                $ContextType = [System.DirectoryServices.AccountManagement.ContextType]::Domain
            }

            if ($ApplicationDirectoryType)
            { 
                $ContextType = [System.DirectoryServices.AccountManagement.ContextType]::ApplicationDirectory
            }

            $ContextOption = [System.DirectoryServices.AccountManagement.ContextOptions]::Negotiate

            $DomainName = $env:UserDnsDomain

            if(-not $DomainName)
            { 
                $DomainName = $env:USERDOMAIN
            }

            $PrincipalContext = New-Object System.DirectoryServices.AccountManagement.PrincipalContext($ContextType, $DomainName)

            if ($PrincipalContext)
            { 
                return $PrincipalContext.ValidateCredentials($Credential.UserName, $Credential.GetNetworkCredential().Password, $ContextOption)
            }
        }
        Catch
        { 
            throw $_
        }     
    }