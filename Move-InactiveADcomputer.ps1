﻿#Requires -RunAsAdministrator
#Requires -Modules ActiveDirectory
function Move-InactiveADcomputer
{
<#
.Synopsis
   Move Active Directory computer objects to a specified OU that has not been active
   last n days

.DESCRIPTION
   To run this function, you have to have the ActiveDirectory module installed. 
   TargetOUName can be a AD container or an OU. MoveToOUName must be an OU.
   Searchbase for inactive computers is always the TargetOU/container

.EXAMPLE
   Move-InactiveADcomputer -TargetOUName Computers -MoveToOUName InactiveLast60Days -InactivityDaysLimit 60

   This will move all computers from the OU/container Computers that has not been active for the last 60 days to the InactiveLast60Days OU

.EXAMPLE
    $splat = @{
        TargetOUName = "Computers"
        MoveToOUName = "InactiveLast60Days"
        InactivityDaysLimit = 60
        Description = "Powershell moved this computer"
    }

    Move-InactiveADcomputer @splat

   This will move all computers from the OU/container Computers that has not been active for the last 60 days to the InactiveLast60Days OU
   and update the Object with the description "Powershell moved this computer"

.EXAMPLE
    $splat = @{
        TargetOUName = "Computers"
        MoveToOUName = "InactiveLast60Days"
        InactivityDaysLimit = 60
        Description = "Powershell moved this computer - on $(get-date)"
    }

    Move-InactiveADcomputer @splat

   This will move all computers from the OU/container Computers that has not been active for the last 60 days to the 
   InactiveLast60Days OU and update the Object with the description "owershell moved this computer - on 06/12/2015 15:46:03"

.EXAMPLE
    $splat = @{
        TargetOUName = "Computers"
        MoveToOUName = "InactiveLast60Days"
        InactivityDaysLimit = 60
        Description = "Powershell moved this computer - on $(get-date)"
        Confirm = $false
    }

    Move-InactiveADcomputer @splat

   This will move all computers from the OU/container Computers that has not been active for the last 60 days to the InactiveLast60Days OU
   and update the Object with the description "owershell moved this computer - on 06/12/2015 15:46:03" and will
   not ask for your permission (Confirm=$false). Not using splatting the command looks like this:

   Move-InactiveADcomputer -TargetOUName Computers -MoveToOUName InactiveLast60Days -InactivityDaysLimit 60 -Description "Powershell moved this computer - on $(get-date)" -Confirm:$false

.OUTPUTS
   none

.NOTES
    AUTHOR: Tore Groneng tore@firstpoint.no @toregroneng tore.groneng@gmail.com
    LASTEDIT: June 2015
    KEYWORDS: ActiveDirectory, computer, LastLogonTimeStamp
    HELP:OK

.FUNCTIONALITY
   Move an object based upon an activity timestamp in Active Directory
#>
[cmdletbinding(SupportsShouldProcess,ConfirmImpact = 'High')]
Param( 
    [Parameter(Mandatory=$true)]
    [string]
    $TargetOUName
    ,
    [Parameter(Mandatory=$true)]
    [String]
    $MoveToOUName
    ,
    [Int]
    $InactivityDaysLimit
    ,
    [String]
    $Description
)
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"

    Try 
    { 
        $MoveToTargetOuObject = Get-ADOrganizationalUnit -Filter { Name -eq $MoveToOUName } -ErrorAction SilentlyContinue

        if (($MoveToTargetOuObject | Measure-Object).Count -gt 1)
        { 
            Write-Error -Message "$f -  MoveToOUName is not unique, multiple results returned" -Category InvalidArgument -ErrorAction Stop            
        }

        if (-not $MoveToTargetOuObject)
        { 
            Write-Error -Message "$f -  Unable to find target OU for the move, '$MoveToOUName' does not exists" -Category InvalidArgument -ErrorAction Stop             
        }

        $TargetOUobject =  Get-ADObject -LDAPFilter "(&(name=$TargetOUName)(objectClass=organizationalUnit))" -ErrorAction SilentlyContinue

        if (-not $TargetOUobject)
        {
            Write-Verbose "$f -  No organizationalUnit with name '$TargetOUName' was found, looking for a container"

            $TargetOUobject =  Get-ADObject -LDAPFilter "(&(name=$TargetOUName)(objectClass=container))" -ErrorAction SilentlyContinue
        }

        if (($TargetOUobject | Measure-Object).Count -gt 1)
        {
            Write-Error -Message "$f -  TargetOUName is not unique, multiple results returned" -Category InvalidArgument -ErrorAction Stop
        }

        if (-not $TargetOUobject)
        {
            Write-Error -Message "$f -  Unable to find target OU/container to search, '$TargetOUName' does not exists" -Category InvalidArgument -ErrorAction Stop            
        }

        $DateInactiveLimit = (Get-Date).AddDays(- $InactivityDaysLimit)

        $InactiveComputers = Get-ADcomputer -Filter { LastLogonTimeStamp -lt $DateInactiveLimit } -Properties "LastLogonTimeStamp","cn" -SearchBase $TargetOUobject.DistinguishedName

        foreach ($computer in $InactiveComputers)
        { 
            if ($PSCmdlet.ShouldProcess("Computer object: $($computer.CN) Destination:$($MoveToTargetOuObject.DistinguishedName)", "$f"))
            { 
                Write-Verbose -Message "$f -  Moving computer '$($computer.distinguishedName)' to '$($MoveToTargetOuObject.DistinguishedName)'"
                Move-Adobject -identity $computer.distinguishedName -TargetPath $MoveToTargetOuObject.DistinguishedName
                if ($Description)
                { 
                    Write-Verbose -Message "$f -  Updating computer description with '$Description'"
                    $computerObject = Get-ADComputer -Identity $computer.CN
                    Set-ADcomputer -identity $computerObject.distinguishedName -Description "$Description"
                }
            }            
        }

        if (-not $InactiveComputers)
        {
            Write-Verbose -Message "$f -  Could not find any computers that have been inactive for the last $InactivityDaysLimit"
        }
    }
    Catch
    { 
        Write-Error -Message "Exception in $f -  $($_.Exception.Message)"
    }
    Finally
    { 
        Write-Verbose -Message "$f - END"
    }
}