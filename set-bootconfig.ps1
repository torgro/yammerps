﻿$str = @"
Firmware Boot Manager
---------------------
identifier {fwbootmgr}
displayorder {bootmgr}
{a72bcf85-d76e-11e3-aca9-00155d4ab601}
{a72bcf89-d76e-11e3-aca9-00155d4ab601}
{a72bcf88-d76e-11e3-aca9-00155d4ab601}
{a72bcf87-d76e-11e3-aca9-00155d4ab601}
{a72bcf86-d76e-11e3-aca9-00155d4ab601}
{a72bcf83-d76e-11e3-aca9-00155d4ab601}
{a72bcf84-d76e-11e3-aca9-00155d4ab601}
timeout 2

Windows Boot Manager
--------------------
identifier {bootmgr}
device partition=\Device\HarddiskVolume1
path \EFI\Microsoft\Boot\bootmgfw.efi
description Windows Boot Manager
locale en-US
inherit {globalsettings}
bootshutdowndisabled Yes
default {current}
resumeobject {a72bcf81-d76e-11e3-aca9-00155d4ab601}
displayorder {current}
toolsdisplayorder {memdiag}
timeout 30

Firmware Application (101fffff)
-------------------------------
identifier {a72bcf83-d76e-11e3-aca9-00155d4ab601}
description EFI Network

Firmware Application (101fffff)
-------------------------------
identifier {a72bcf84-d76e-11e3-aca9-00155d4ab601}
description EFI SCSI Device

Firmware Application (101fffff)
-------------------------------
identifier {a72bcf85-d76e-11e3-aca9-00155d4ab601}
description EFI SCSI Device

Firmware Application (101fffff)
-------------------------------
identifier {a72bcf86-d76e-11e3-aca9-00155d4ab601}
path \EFI\Microsoft\Boot\bootmgfw.efi
description Windows Boot Manager

Firmware Application (101fffff)
-------------------------------
identifier {a72bcf87-d76e-11e3-aca9-00155d4ab601}
path \EFI\Microsoft\Boot\bootmgfw.efi
description Windows Boot Manager

Firmware Application (101fffff)
-------------------------------
identifier {a72bcf88-d76e-11e3-aca9-00155d4ab601}
path \EFI\Microsoft\Boot\bootmgfw.efi
description Windows Boot Manager

Firmware Application (101fffff)
-------------------------------
identifier {a72bcf89-d76e-11e3-aca9-00155d4ab601}
path \EFI\Microsoft\Boot\bootmgfw.efi
description Windows Boot Manager
"@

function Get-GUIDentries
{
[cmdletbinding()]
Param(
        [Parameter(Mandatory=$true)]
        [string] $ConfigFileContent
)
    $Array = $ConfigFileContent -split [System.Environment]::NewLine
    $GUIDlines= $Array | where {$_ -like "*{*}*"} | where {$_ -like "*-*"}

    foreach($line in $GUIDlines)
    {
        $indexOf = $line.IndexOf("{")
        $line.Substring($indexOf)
    }
}

function Set-BootConfig
{
[cmdletbinding()]
Param(
        [Parameter(Mandatory=$true)]
        [string] $ConfigFileContent
)

    Write-Verbose "Create Array of configfile"

    $Array = $ConfigFileContent -split [System.Environment]::NewLine

    $indexOfBootManager = $Array.IndexOf("Windows Boot Manager")

    Write-Verbose "IndexOf 'Windows Boot Manager' is $indexOfBootManager"

    $ArrayLength = $array.count - 1
    $newArray = $array[$indexOfBootManager..$ArrayLength]

    Write-Verbose "Building new string for select-string"
    $SelectStringContent = $newArray | foreach {$_}

    [string]$EFInetworkText = $newArray | Select-String -Pattern "EFI Network"

    Write-Verbose "Looking for pattern 'description EFI Network' and selecting 3 pre-lines from match and 10 lines after"
    $FirmwareApplicationSelect = $SelectStringContent | Select-String -Pattern $EFInetworkText -Context 3,10

    Write-Verbose "Building new string for select string pattern match"
    #[string]$FirmwareApplicationContent = ($FirmwareApplicationSelect.Context.DisplayPreContext | foreach {[string]$line = $_;$line = $line.TrimStart(' ') + [environment]::NewLine;write-verbose "'$line'";$line}) 
    $FirmwareApplicationContent = ($FirmwareApplicationSelect.Context.DisplayPreContext | foreach {$_.TrimStart(" ")}) 
    $FirmwareApplicationContent += $FirmwareApplicationSelect.line.TrimStart(" ") 
    $FirmwareApplicationContent += ($FirmwareApplicationSelect.Context.DisplayPostContext | foreach {$_.TrimStart(" ")})

    $FirmWareApplicationIndex = $newArray.IndexOf("Firmware Application (101fffff)") - 1

    Write-Verbose "IndexOf 'Firmware Application (101fffff)' is $FirmWareApplicationIndex"

    Write-Verbose "Building string for 'Windows Boot Manager'" 
    $BootManager = $newArray[0..$FirmWareApplicationIndex] | foreach{$_.TrimStart(" ")}

    $FirmwareApplicationContent = $BootManager + $FirmwareApplicationContent

    Write-Verbose "Converting array to string"
    $FirmwareApplicationContent -join [environment]::NewLine
}

Function Get-GuidsToDelete
{
[cmdletbinding()]
Param(
        [Parameter(Mandatory=$true)]
        [string[]] $AllGuids
        ,
        [string[]] $KeepGuids
)
    Write-Verbose "Returning all GUIDs that you can delete"
    $AllGuids | where {$KeepGuids -notcontains $_} | Select-Object -Unique
}

<#
Eksempel:

Henter config fra fil:
$CurrentConfig = Get-Content -Path c:\temp\config.txt -Raw

Lag ny config:
$NewConfig = Set-BootConfig -ConfigFileContent $CurrentConfig

Finn alle guids i "gammel" config
$allGuids = Get-GUIDentries -ConfigFileContent $CurrentConfig

Finn alle guids i "ny" config
$KeepGuids = Get-GUIDentries -ConfigFileContent $NewConfig

Finn guids som skal slettes
$deleteThese = Get-GuidsToDelete -AllGuids $allGuids -KeepGuids $KeepGuids

#>