﻿function New-Hash
{
[cmdletbinding()]
Param(
    $Key
    ,
    $Value
    ,
    [Parameter(ValueFromPipeLine)]
    [hashtable]$inputobject
)
Begin
{

}

Process
{
    if ($PSBoundParameters.ContainsKey("Inputobject"))
    {
        $Inputobject[$Key]= $Value
    }
    else
    {
        $Inputobject = @{$Key=$Value}
    }
    return $Inputobject
}

End 
{

}

}

