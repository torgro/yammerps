﻿function Show-Message
{
[cmdletbinding()]
Param(
    [Parameter(ValueFromPipeLine)]
    $Message
    ,
    [Parameter(ParameterSetName="Win10")]
    $DisplayDuration = 10
    ,
    [Parameter(ParameterSetName="Win10")]
    [switch]
    $NotificationArea
    ,
    [Parameter(ParameterSetName="Win10")]
    [string]
    $Tag = "Powershell"
    ,
    [Parameter(ParameterSetName="Win10")]
    [string]
    $Group = "Powershell"
)

Begin
{
    if ($NotificationArea)
    {        
        try
        {
            $null = [Windows.UI.Notifications.ToastNotificationManager, Windows.UI.Notifications, ContentType = WindowsRuntime]
            $template = [Windows.UI.Notifications.ToastNotificationManager]::GetTemplateContent([Windows.UI.Notifications.ToastTemplateType]::ToastText01)    
        }
        catch
        {
            $ex = $_.Exception
            Write-Warning -Message "Notification area not supported, $($ex.Message)"
            $null = $PSBoundParameters.Remove("NotificationArea")
            $NotificationArea = $false
        }
    }
}

Process
{
    if (-not $NotificationArea)
    {
        $null = [System.Windows.Forms.Messagebox]::Show($Message)
    }

    if ($NotificationArea)
    {
        $toastXml = [xml]$template.GetXml()

        $null = $toastXml.GetElementsByTagName("text").AppendChild($toastXml.CreateTextNode($Message))

        #Convert back to WinRT type

        $xml = New-Object -TypeName Windows.Data.Xml.Dom.XmlDocument
        $xml.LoadXml($toastXml.OuterXml)
        $toast = [Windows.UI.Notifications.ToastNotification]::new($xml)
        $toast.Tag = $Tag
        $toast.Group = $Group

        $toast.ExpirationTime = [DateTimeOffset]::Now.AddSeconds($DisplayDuration)
        $notifier = [Windows.UI.Notifications.ToastNotificationManager]::CreateToastNotifier("Powershell")
        
        $notifier.Show($toast)
    }
}
}