﻿Function Get-DateRange
{
[cmdletbinding()]
Param(
    [Datetime]$Start
    ,
    [Datetime]$End
    ,
    [int]$Days
)
    if(-not $days)
    {
        $days = (New-TimeSpan -Start $Start -End $End).TotalDays
    }
    $culture = [cultureinfo]::CurrentCulture.Calendar
    foreach($Day in (0..$Days))
    {        
        $date = (Get-Date).AddDays($Day)
        New-Object -TypeName psobject -Property @{
            Date = $date
            Day = $Date.DayOfWeek
            intDay = $date.DayOfWeek.value__
            intMonth = $date.Month
            Month = [cultureinfo]::CurrentCulture.DateTimeFormat.GetMonthName($date.month)
            Quarter = [system.math]::Ceiling($date.Month / 3)
            Week = $culture.GetWeekOfYear($date,[System.Globalization.CalendarWeekRule]::FirstDay,[System.DayOfWeek]::Monday)
            YearDay = $date.DayOfYear
        }        
        $AddDays += $Day
    }
}