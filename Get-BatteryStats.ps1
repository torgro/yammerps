﻿Function Get-BatteryStats
{
[CmdletBinding()]
Param()
    $batteryStatus = gwmi batterystatus -name root\wmi
    $batterystats  = gwmi win32_portablebattery
    [int]$index = 0
    foreach ($battery in $batteryStatus)
    {
        $missing = [Math]::Round(((1-($battery.RemainingCapacity/$batterystats[$index].DesignCapacity))*100), 2)
        $batteryReport = [pscustomobject]@{
            Charging = $battery.Charging
            PluggedIn = $battery.PowerOnline
            ListedCapacity = $batterystats[$index].DesignCapacity;
            ActualCapacity = $battery.RemainingCapacity;
            PercentRemaining = [Math]::Round((($battery.RemainingCapacity/$batterystats[$index].DesignCapacity)*100),2);
            MissingCapacity = if ([math]::Sign($missing) -eq '-1'){"ReRunOnBattery"}else{$missing}
        }
        $batteryReport  
        $index ++
    }
}