﻿Add-Type '
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace SC.IO
{
    [StructLayout(LayoutKind.Sequential)]
    public struct FILETIME
    {
        public uint dwLowDateTime;
        public uint dwHighDateTime;
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct WIN32_FIND_DATA
    {
        public FileAttributes dwFileAttributes;
        public FILETIME ftCreationTime;
        public FILETIME ftLastAccessTime;
        public FILETIME ftLastWriteTime;
        public int nFileSizeHigh;
        public int nFileSizeLow;
        public int dwReserved0;
        public int dwReserved1;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
        public string cFileName;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 14)]
        public string cAlternate;
    }

    public class UnsafeNativeMethods
    {
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        public static extern IntPtr FindFirstFile(string lpFileName, out WIN32_FIND_DATA lpFindFileData);

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        public static extern IntPtr FindFirstFileExW(
            string              lpFileName,
            int                 fInfoLevelId,
            out WIN32_FIND_DATA lpFindFileData,
            int                 fSearchOp,
            IntPtr              lpSearchFilter,
            int                 dwAdditionalFlags
        );

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        public static extern bool FindNextFile(IntPtr hFindFile, out WIN32_FIND_DATA lpFindFileData);
        
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool FindClose(IntPtr hFindFile);
    }

    public class FileSearcher
    {
        public System.Collections.Generic.List<string> Items = new System.Collections.Generic.List<string>();
        public long[] CountItems(string path, bool recurse, long[] itemCount)
        {
            if (itemCount == null)
            {
                itemCount = new long[]{ 0, 0, 0 };
            }

            string searchPath = String.Format(@"\\?\{0}\*", path);

            WIN32_FIND_DATA findData = new WIN32_FIND_DATA();
            IntPtr findHandle = UnsafeNativeMethods.FindFirstFileExW(searchPath, 1, out findData, 0, IntPtr.Zero, 0);
            do
            {
                if (findData.dwFileAttributes.HasFlag(FileAttributes.Directory))
                {
                    if (recurse && findData.cFileName != "." && findData.cFileName != "..")
                    {
                        itemCount[2]++;
                        Items.Add(System.IO.Path.Combine(path,findData.cFileName));
                        itemCount = CountItems(
                            Path.Combine(path, findData.cFileName),
                            recurse,
                            itemCount
                        );
                    }
                }
                else
                {
                    Items.Add(System.IO.Path.Combine(path,findData.cFileName));
                    itemCount[0] += ((long)findData.nFileSizeHigh * UInt32.MaxValue) + (long)findData.nFileSizeLow;
                    itemCount[1]++;
                }
            } while (UnsafeNativeMethods.FindNextFile(findHandle, out findData));
            UnsafeNativeMethods.FindClose(findHandle);

            return itemCount;
        }
    }
}
'

function Measure-ChildItem 
{
[CmdletBinding()]
param (
    [String]$Path
    ,
    [Switch]$ValueOnly
    ,
    [switch]
    $PassThru
)

    $Path = $pscmdlet.GetUnresolvedProviderPathFromPSPath($Path)
    $dirInfo = [System.IO.DirectoryInfo]::new($Path)

    if ($dirInfo.Target.Count -ne 0)
    {
        $Path = $dirInfo.Target[0]
    }

    $fileSearcher = [SC.IO.FileSearcher]::new()
    $itemCount = $fileSearcher.CountItems($Path, $true, $null)
    
    if ($ValueOnly.IsPresent)
    {
        return $itemCount[0]
    }

    if ($PassThru.IsPresent)
    {
        return $fileSearcher.Items
    }

    if ($ValueOnly.IsPresent -eq $false -and $PassThru.IsPresent -eq $false)
    {
        [PSCustomObject]@{
            Path           = $Path
            Size           = $itemCount[0]
            FileCount      = $itemCount[1]
            DirectoryCount = $itemCount[2]
            Files          = $fileSearcher.Items
        }
    }
}

function Get-ChildItemHashtable
{
[cmdletbinding()]
Param(
    [Parameter(ValueFromPipeline)]
    [Alias("Files")]
    [string[]]
    $Path
)

Begin
{
    $outHashTable = @{}
}

Process
{   
    foreach ($p in $Path)
    {
        $info = [System.IO.FileInfo]::new($p)
        if ($info.Attributes -eq [System.IO.FileAttributes]::Directory)
        {
            if (-not $outHashTable.Contains($info.FullName))
            {
                $outHashTable.Add($info.FullName, [System.Collections.Generic.List[string]]::new())
            }
        }
        else
        {
            if (-not $outHashTable.Contains($info.DirectoryName))
            {
                $outHashTable.Add($info.DirectoryName, [System.Collections.Generic.List[string]]::new())
                ($outHashTable[$info.DirectoryName]).Add($info.Name)
            }
            else
            {
                ($outHashTable[$info.DirectoryName]).Add($info.Name)
            }
        }
    }    
}

End
{
    $outHashTable
}
    
}

function Show-ChildItems
{
[cmdletbinding()]
Param(
    [hashtable]
    $Table
)

$out = foreach ($key in $Table.Keys)
{
    $c = ($Table[$key]).Count
    [pscustomobject]@{
        Path = $key
        Count = $c
    }
}

$out
}

function Show-ChildItemExtension
{
[cmdletbinding()]
Param(
    [string[]]
    $PathArray
)

$out = @{}

foreach ($p in $PathArray)
{
    $info = [System.IO.FileInfo]::new($p)
    
    if ($info.Attributes -ne [System.IO.FileAttributes]::Directory)
    {
        $ext = $info.Extension.Replace(".","")
        if (-not [string]::IsNullOrEmpty($ext))
        {                       
            if (-not $out.ContainsKey($ext))
            {
                $out.Add($ext, [System.Collections.Generic.List[string]]::new())
                ($out.$ext).add($info.FullName)
            }
            else
            {
                ($out.$ext).Add($info.FullName)
            }        
        }
    }
}

$output = [System.Collections.Generic.List[PSCustomObject]]::new()
$extensions = [System.Management.Automation.PSScriptProperty]::new("Extensions",{$e[0].Filter})
$output.psobject.Members.Add($extensions)

foreach ($key in $out.Keys)
{
    $o = [PScustomObject]@{
        Name = $key
        Files = $out.$key
    }
    $output.Add($o)
}


$output
}




function MyFunction
{
[CmdletBinding()]
param([parameter(ValueFromRemainingArguments=$true)] 
    $k
)
begin{}
process{
    #Write-Verbose $k[0] -Verbose

    foreach($b in $k[0])
    {
        $out[$b]
    }
    
 }
}


$script = {
   @{
        foo = "bar"
        bar = if ($env:COMPUTERNAME){"computer"}
   }
}
$allowedCommands = [System.Collections.Generic.List[string]]::new()
$allowedCommands.Add("Write-output")
#$allowedCommands.Add("Write-verbose")
$allowedVars = [System.Collections.Generic.List[string]]::new()


$script.CheckRestrictedLanguage($allowedCommands,$allowedVars,$false)
&$script
 
 $fun = Get-Command MyFunction


 data {
    "kkkkK",
    "kkkk"
 }



$ParseErrors = @()
$InputString = 'string,@{key="value1","value2"},"another string"'
$FakeCommand = "Totally-NotACmdlet -FakeParameter $($script.ToString())"
$AST = [System.Management.Automation.Language.Parser]::ParseInput($script,[ref]$null,[ref]$ParseErrors)
if(-not $ParseErrors){  
    # Use Ast.Find() to locate the CommandAst parsed from our fake command
    $CmdAst = $AST.Find({param($ChildAst) $ChildAst -is [System.Management.Automation.Language.HashtableAst]},$false)
    # Grab the user-supplied arguments (index 0 is the command name, 1 is our fake parameter)
    $ArgumentAst = $CmdAst.CommandElements
    if($ArgumentAst -is [System.Management.Automation.Language.ArrayLiteralAst]) {
        # Argument was a list
        # Inspect $ArgumentAst.Elements
    }
    else {
        # Argument was a scalar.
        # Test if it's a [StringConstantExpressionAst] or [HashtableAst], otherwise throw an error  
    }
}