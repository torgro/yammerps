﻿function Get-NetListeningPort
{            
[cmdletbinding(DefaultParameterSetName="dummy")]            
param(
    [Parameter(ParameterSetName="TCP")]
    [switch]
    $TCP
    ,
    [Parameter(ParameterSetName="UDP")]
    [switch]
    $UDP  
    ,
    [int]
    $Port
)            

Begin
{
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"
    
    $displayTypeName = "net.Listner"
    $DefaultDisplayProperties = @("LocalAddress","Port","Type","IPVersion")

    $updateTypeData = @{
        TypeName = $displayTypeName
        DefaultDisplayPropertySet = $DefaultDisplayProperties
        ErrorAction = "SilentlyContinue"
    }
    if (-not (Get-TypeData -TypeName $displayTypeName -ErrorAction SilentlyContinue -Verbose:$false))
    {
        Update-TypeData @updateTypeData -Verbose:$false 
    }    
}

Process
{
    try 
    {            
        $GlobalProperties = [System.Net.NetworkInformation.IPGlobalProperties]::GetIPGlobalProperties()            
        $TCPConnections = $GlobalProperties.GetActiveTcpListeners()
        $UDPConnections = $GlobalProperties.GetActiveUdpListeners()

        $allConnections = New-Object -TypeName System.Collections.Generic.List[PScustomObject]
        $portType = "*"

        if ($PSBoundParameters.ContainsKey("TCP"))
        {
            Write-Verbose -Message "$f -  Getting TCP listeners"
            $portType = "TCP"
        }

        if ($PSBoundParameters.ContainsKey("UDP"))
        {
            Write-Verbose -Message "$f -  Getting UDP listeners"
            $portType = "UDP"
        }

        if ($PSBoundParameters.ContainsKey("TCP") -and $PSBoundParameters.ContainsKey("UDP"))
        {
            Write-Verbose -Message "$f -  Getting TCP and UDP listeners"
            $portType = "*"
        }

        foreach ($Connection in $TCPConnections) 
        {            
            $IPType = "4"            

            if ($Connection.address.AddressFamily -ne "InterNetwork")
            {
                $IPType = "6" 
            }

            $output = [pscustomobject]@{
                LocalAddress = $Connection.Address
                Port = $Connection.Port
                Type = "TCP"
                AddressFamily = $Connection.Address.AddressFamily
                IPVersion = $IPType
            }

            $output.PSObject.TypeNames.Insert(0,"$displayTypeName")
            $allConnections.Add($output)
        }

        foreach ($Connection in $UDPConnections) 
        {      
            $IPType = "4"
        
            if ($Connection.address.AddressFamily -ne "InterNetwork")
            {
                $IPType = "6"
            }  
                             
            $output = [pscustomobject]@{
                LocalAddress = $Connection.Address
                Port = $Connection.Port
                Type = "UDP"
                AddressFamily = $Connection.Address.AddressFamily
                IPVersion = $IPType
            }

            $output.PSObject.TypeNames.Insert(0,"$displayTypeName")
            $allConnections.Add($output)
        }

        foreach ($Connection in $allConnections)
        {
            if ($Port)
            {
                if ($Connection.port -eq $port -and $Connection.Type -like $portType)
                {
                    $Connection
                }
            }
            else
            {
                if ($Connection.Type -like $portType)
                {
                    $Connection
                }
            }
        }
    }
    catch 
    {            
        $ex = $_.Exception
        $ex            
    }         
}
}
