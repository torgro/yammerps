﻿function Get-PfxCertificate
{
[cmdletbinding()]
Param(
    [Parameter(ParameterSetName='ByPath')]
    [string[]]
    $FilePath
    ,
    [Parameter(ParameterSetName='ByLitteralPath')]
    [string[]]
    $LiteralPath
    ,
    [System.Security.SecureString]
    $Password
    ,
    [Parameter(ParameterSetName='ByThumbPrint')]
    [string]
    $ThumbPrint
)

$GetPfxCert = @{}

if ($null -ne $FilePath)
{
    $GetPfxCert.FilePath = $FilePath
    $paths = $FilePath
}

if ($null -ne $LiteralPath)
{
    $paths = $LiteralPath
    $GetPfxCert.LiteralPath = $LiteralPath
}

if ($null -ne $Password)
{
    $bstr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($Password)
    $clear = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstr)
    [System.Runtime.InteropServices.Marshal]::ZeroFreeBSTR($bstr)
}

if ($null -ne $ThumbPrint)
{
    Write-Verbose -Message "Opening cert store"
    $store = [System.Security.Cryptography.X509Certificates.X509Store]::new([System.Security.Cryptography.X509Certificates.StoreLocation]::LocalMachine)
    $store.Open([System.Security.Cryptography.X509Certificates.OpenFlags]::ReadOnly)
    $cert = $store.Certificates | where thumbprint -eq $t
    $store.Close()

    if ($null -eq $cert)
    {
        Write-Error -Message "Unable to find certificate with thumbprint [$ThumbPrint]" -ErrorAction Stop
    }
    
    if ($null -ne $cert.PrivateKey)
    {
        $containerInfo = $cert.PrivateKey.CspKeyContainerInfo
        $cert | Add-Member -MemberType NoteProperty -Name ProviderName -Value $containerInfo.ProviderName
    }
    $cert
}

try
{
if ($null -eq $Password -and $null -eq $ThumbPrint)
{
    Microsoft.PowerShell.Security\Get-PfxCertificate @GetPfxCert
}
else 
{
    foreach ($file in $paths)
    {
        $exists = Test-Path -Path $file
        if (-not $exists)
        {
            $exists = Test-Path -LiteralPath $file
        }

        if ($exists)
        {
            $cert = [System.Security.Cryptography.X509Certificates.X509Certificate2]::new($file, $clear)
            if ($null -ne $cert.PrivateKey)
            {
                $containerInfo = $cert.PrivateKey.CspKeyContainerInfo
                $cert | Add-Member -MemberType NoteProperty -Name ProviderName -Value $containerInfo.ProviderName
            }
            $cert
        }
    }
}

}
catch
{
    $ex = $_.Exception
    throw $ex
}
Finally
{
    $clear = [string]::Empty
}


}