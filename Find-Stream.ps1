﻿function Find-Stream
{
[cmdletbinding()]
Param(
    [Parameter(ValueFromPipeline)]
    [string[]]$FullName
    ,
    [switch]$Recurse
)

Process
{	
    if ([string]::IsNullOrEmpty($FullName))
    {
        $FullName = Get-ChildItem @PSBoundParameters | Select-Object -ExpandProperty FullName
    }

	foreach ($file in $FullName)
    {
		Write-Verbose -Message "File is [$($file)]"
        $streams = Get-Item -Path $file -Stream * | Where-Object {$_.Stream -ne ':$DATA' -and $_.Stream -ne 'Zone.Identifier'}
		foreach ($stream in $streams)
        {			
			[pscustomobject]@{                
                StreamName = $Stream.Stream
                File = $file
		    }
        }
	}
}
}