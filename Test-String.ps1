﻿function Test-String
{

param(

    [parameter(Valuefrompipeline)]
    [string]$InputObject  

)
    Begin
    {}

    Process
    {

        $bytes = [System.Text.Encoding]::ASCII.GetBytes("$InputObject")
        $asciiValue = [System.Text.Encoding]::ASCII.GetString($bytes)
        [pscustomobject]@{
            InputString = $InputObject
            ASCIIvalue = $asciiValue
            IsEqual = $asciiValue -eq $InputObject
        }
    }
}
