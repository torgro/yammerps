﻿Function Test-TCPPort
{
<#
.Synopsis
   Test a TCP port

.DESCRIPTION
   Specify an IP address or an host to test if the port is open

.PARAMETER Endpoint
    An ipaddress or an host name (FQDN)

.PARAMETER Port
    An integer value specifying the port to connect to

.PARAMETER Timeout
    Timeout to wait in ms, default is 1000

.EXAMPLE
   Test-TCPPort -endpoint 192.168.10.10 -port 443

   Will return true if the endpoint has port 443 open using the default timeout of 1000 ms

.EXAMPLE
   Test-TCPPort -endpoint somehost.domain.com -port 443 -timout 2000

   Will return true if the endpoint has port 443 open using the timeout of 2000 ms

.OUTPUTS
   Returns an boolean value

.NOTES
   Tore Grøneng @toregroneng tore@firstpoint.no
#>
[cmdletbinding()]	
Param ( 
    [ValidateNotNullOrEmpty()]
	[string] 
    $EndPoint
    ,
	[Int] 
    $Port
    ,
    [int] 
    $Timeout = 1000
)
	
	$IP = [System.Net.Dns]::GetHostAddresses($EndPoint)
	$Address = [System.Net.IPAddress]::Parse($IP)
	$Socket = New-Object System.Net.Sockets.TCPClient
	$Connect = $Socket.BeginConnect($Address,$Port,$null,$null)

    Write-Verbose -Message "Setting up a waithandle"
    $wait = $Connect.AsyncWaitHandle.WaitOne($timeout,$false)

	if ($wait)
	{
		Write-Verbose -Message "connected=$($Socket.Connected)"			
        [void]($Socket.EndConnect($Connect))
        [void]($socket.Close())
        $true
	}
	else
	{
        Write-Verbose -Message "Wait is false connected=$($Socket.Connected), connection timeout!"		
        $false
	}
    Write-Verbose -Message "Done"
}
