﻿Add-Type -Path "C:\Program Files\Microsoft SDKs\Azure\.NET SDK\v2.5\ToolsRef\Microsoft.WindowsAzure.Storage.dll"

$con = "DefaultEndpointsProtocol=https;AccountName=powershellps;AccountKey=;"

$StorageAccount = [Microsoft.WindowsAzure.Storage.CloudStorageAccount]::Parse($con)
$blobCli = $StorageAccount.CreateCloudBlobClient()

function Get-BlobContainerRef
{ 
Param(
    [CloudBlobClient]$cli
    ,
    [String]$ContainerName
)
    
    $container = $cli.GetContainerReference("$ContainerName")
    $container.CreateIfNotExists()
    return $container
}

$ctx = New-AzureStorageContext -StorageAccountName powershellps -StorageAccountKey ""