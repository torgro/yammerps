function Measure-String
{
[cmdletbinding()]
Param(
    [Parameter(ValueFromPipeline)]
    [string[]]$InputObject
    ,
    [string]$SearchString
    ,
    [switch]$SkipComments
)

Begin
{
    $count = 0
}

Process
{   
    if ($SearchString.Length -gt 1)
    {
        foreach ($str in $InputObject)
        {
            if ($SkipComments.IsPresent)
            {
                $trimmed = $str.TrimStart()

                if ($trimmed.StartsWith("#"))
                {
                    continue
                }
            }
            
            $stringlength = $str.length
            $searchTextLength = $SearchString.Length
            $replaced = $str.ToLower().Replace($SearchString.ToLower(),"")
            $foundCount = ($stringlength - $replaced.length) / $searchTextLength
            $count += $foundCount
        }
    }
    else
    {
        foreach ($str in $InputObject)
        {
            if ($SkipComments.IsPresent)
            {
                $trimmed = $str.TrimStart()

                if ($trimmed.StartsWith("#"))
                {
                    continue
                }
            }

            foreach ($char in $str.toCharArray())
            {
                if ($char -eq $SearchString)
                {
                    $count++
                }
            }
        }
    }    
}
END
{
    [Pscustomobject]@{
        SearchString = $SearchString
        Count = $count
    }    
}
}