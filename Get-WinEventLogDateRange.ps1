﻿function Get-WinEventLogDateRange
{
<#
.Synopsis
   Find events in a specific timeframe
.DESCRIPTION
   Correlate events from the eventlog. List all events in a specific timespan.
.EXAMPLE
   $StartTime = (Get-Date).Date.AddHours(10).AddMinutes(15)
   Get-WinEventLogDateRange -StartFrom $StartTime -Endtime $StartTime.AddSeconds(30)

   This will find all events in all eventlogs within the specified timepan (10:15:00 - 10:15:30)
.EXAMPLE
   Get-WinEventLogDateRange -StartFrom $StartTime -SpanSeconds 20

   This will find all events in all eventlogs within the specified timepan (10:15:00 - 10:15:20)
#>
[cmdletbinding()]
Param(
    [Parameter(Mandatory)]
    [datetime]$StartFrom
    ,
    [Parameter(ParameterSetName="EndTime")]
    [Datetime]$EndTime
    ,
    [string]$LogName
    ,
    [Parameter(ParameterSetName="TimeSpan")]
    [int]$SpanSeconds
    ,
    [string]$ComputerName
)

BEGIN{
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"

    $GetWinEvent = @{
        Verbose = $false
        ErrorAction = "SilentlyContinue"
    }

    $FilterHash = @{        
        StartTime = $StartFrom        
    }

    if($PSBoundParameters.ContainsKey("EndTime"))
    {
        $null = $FilterHash.Add("EndTime",$EndTime)
    }

    if($PSBoundParameters.ContainsKey("SpanSeconds"))
    {
        $null = $FilterHash.Add("EndTime",$StartFrom.AddSeconds($SpanSeconds))
    }

    if($PSBoundParameters.ContainsKey("LogName"))
    {
        Write-Verbose -Message "$f -  Scoping to log [$LogName]"
        $null = $FilterHash.Add("LogName",$LogName)
    }

    if($PSBoundParameters.ContainsKey("ComputerName"))
    {
        $null = $GetWinEvent.Add("ComputerName",$ComputerName)
    }
    
    Write-Verbose -Message ("Filterhash is" + ($FilterHash | Out-String))

    $events = New-Object -TypeName System.Collections.ArrayList
}

PROCESS{
    Write-Verbose -Message "$f -  Searching for events.."    

    $availableLogs = Get-WinEvent -ListLog * @GetWinEvent | where RecordCount -gt 0
    $null = $GetWinEvent.Add("FilterHashtable",$FilterHash)

    if($LogName)
    {
        $availableLogs = $availableLogs | where LogName -eq "$LogName"
    }

    foreach($log in $availableLogs)
    {
        $FilterHash.LogName = $log.LogName
        Write-Verbose -Message "$f -  Processing log [$($log.LogName)]"       
        $all = Get-WinEvent @GetWinEvent

        foreach($event in $all)
        {        
            $out = "" | Select-Object -Property TimeCreated, LogName, ProviderName, Level, Id, Message, ComputerName, EventObj
            $out.TimeCreated = $event.TimeCreated
            $out.LogName = $event.LogName
            $out.ProviderName = $event.ProviderName
            $out.Level = $event.Level
            $out.Id = $event.Id
            $out.Message = $event.Message
            $out.EventObj = $event
            $out.ComputerName = $ComputerName
            $null = $events.Add($out)
        }
    }   
}

END{    
    $events
    Write-Verbose -Message "$f -  Found [$($events.Count)] events"
    Write-Verbose -Message "$F - END"
}
}