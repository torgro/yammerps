function Reset-SCOMagentCache
{
[cmdletbinding()]
Param(
    [string] $AgentName = "."
)
    $HealthService = $null
    $AgentPath = $null
    
    Write-Verbose "Resetting cache for $AgentName"
    $HealthService = Get-WmiObject -Query "Select * from win32_service where name = 'healthservice'" -ComputerName $AgentName -ErrorAction SilentlyContinue

    if($HealthService)
    {
        $AgentPath = (Split-Path -Path $HealthService.pathname -Parent).Replace('"',"")
        Write-Verbose "Agentpath=$AgentPath"

        $AgentPath = "$AgentPath\Health Service State*"
        Write-Verbose "Agentpath=$AgentPath"        
    }
    else
    {
        throw "Unable to find Healthservice service on computer $AgentName"
    }

    Write-Verbose "Testing path to healthservice object"
    $testpath = $null

    if($AgentName -eq ".")
    {
        $testpath = Test-Path -Path $AgentPath
    }
    else
    {
        Write-Verbose "Creating scriptblock and invoking command"
        $sb = [scriptblock]::Create("test-path -path '$agentPath'")
        $testpath = Invoke-Command -ScriptBlock $sb -ComputerName $AgentName # -ArgumentList $AgentPath
    }

    Write-Verbose "Testpath results is $testpath"

    If(-not($testpath))
    {
        throw "Unable to find path $AgentPath"
    }

    Write-Verbose "At this point we have a vaild path and computername"

    Write-Verbose "Getting windows service healthservice"
    $HealthWindowsService = $null
    $HealthWindowsService = Get-Service -Name HealthService -ErrorAction SilentlyContinue -ComputerName $AgentName

    if($HealthWindowsService)
    {
        Write-Verbose "Have the healthservice running on $AgentName"
    }
    else
    {
        throw "Unable to find a service with name HealthService"
    }
    
    Write-Verbose "Stopping service HealthService"
    [void](Stop-Service -InputObject $HealthWindowsService)

    Write-Verbose "Removing state/cache in $AgentPath"
    $sb = [scriptblock]::Create("Remove-Item -path '$agentPath' -Recurse -Force")

    if($AgentName -eq ".")
    {
        Write-Verbose "Removing state/cache - current computer"
        [void](Remove-Item -Path "$AgentPath" -Recurse -Force)
    }
    else
    {
        Write-Verbose "Removing state/cache - invoking command on $AgentName"
        Invoke-Command -ScriptBlock $sb -ComputerName $AgentName
    }

    Write-Verbose "Starting service"
    [void](Start-Service -InputObject $HealthWindowsService)
}