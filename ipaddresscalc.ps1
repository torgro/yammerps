﻿#http://stackoverflow.com/questions/1038002/how-to-convert-cidr-to-network-and-ip-address-range-in-c

function Get-BroadCastAddress
{
[cmdletbinding()]
Param(
    [string]$ip
    ,
    [string]$mask
)
    $ipadr = [System.Net.IPAddress]::Parse($ip)
    $sub = [System.Net.IPAddress]::Parse($mask)

    $broad = New-Object byte[] 4
    $subBytes = $sub.GetAddressBytes()
    $i = 0
    foreach($oct in $ipadr.GetAddressBytes())
    {
        [byte]$bored = $oct -bor ($subBytes[$i] -bxor 255)
        $broad.Item($i) = $bored
        $i++
    }
    #$broad
    New-Object System.Net.IPAddress(,$broad)
    
}

function Get-IPaddresses
{
[cmdletbinding()]
Param(
    [string]$ip
    ,
    [int]$CIDR
)
    $CIDRbyteArray = Get-NetCIDRbytes -CIDR $CIDR
    [ipaddress]$SubNetMask = New-Object System.Net.IPAddress(,$CIDRbyteArray)

    $IPaddress = [ipaddress]"$ip"

    $hostBitCount = 32 - $CIDR
    $netMask = [UInt32]0xFFFFFFFFL -shl $hostBitCount
    $hostMask = -bnot $netMask

    $networkAddress = (Convert-IPaddressToInt -IPAddress $ipaddress) -band $netMask
    $broadcastAddress = $networkAddress -bor $hostMask

    
    foreach($address in 1..($broadcastAddress - $networkAddress -1))
    {
        $address = $address + $networkAddress
        Convert-IntToIPaddress -UInt32 $address
    }
}

function Convert-IntToIPaddress
{
[CmdletBinding()]
param (
    [Parameter(Mandatory)]
    [UInt32]
    $UInt32
)
    $bytes = [BitConverter]::GetBytes($UInt32)
 
    if ([BitConverter]::IsLittleEndian)
    {
        [Array]::Reverse($bytes)
    }
 
    return New-Object ipaddress(,$bytes)
}

function Convert-IPaddressToInt
{
[CmdletBinding()]
param (
    [Parameter(Mandatory)]
    [ipaddress]
    $IPAddress
)
 
    $bytes = $IPAddress.GetAddressBytes()
 
    if ([BitConverter]::IsLittleEndian)
    {
        [Array]::Reverse($bytes)
    }
 
    return [BitConverter]::ToUInt32($bytes, 0)
}

function Get-NetCIDRbytes
{
[cmdletbinding()]
[OutputType([byte[]])]
Param( 
    [int]$CIDR
)
    [string]$one = "1" * $CIDR

    [int]$zeros = 32 - $one.Length
    [string]$zero = "0" * $zeros
    [string]$StringCIDR = "$one$zero"
    $OCT1 = $StringCIDR.Substring(0,8)
    $OCT2 = $StringCIDR.Substring(8,8)
    $OCT3 = $StringCIDR.Substring(16,8)
    $OCT4 = $StringCIDR.Substring(24,8)
    $byteArray = New-Object byte[] 4
    $byteArray.item(0) = [byte][convert]::ToInt32("$OCT1",2)
    $byteArray.item(1) = [byte][convert]::ToInt32("$OCT2",2)
    $byteArray.item(2) = [byte][convert]::ToInt32("$OCT3",2)
    $byteArray.item(3) = [byte][convert]::ToInt32("$OCT4",2)
    $byteArray
    
}

function Get-SubNetMask
{
[cmdletbinding()]
Param(
    [int]$CIDR
)

    if($CIDR)
    {
        if(-not($CIDR -ge 0 -and $CIDR -le 32))
        {
            throw "CIDR must be a value in the range 0-32"
        }

        $bytes = Get-NetCIDRbytes -CIDR $CIDR
        return New-Object System.Net.IPAddress(,$bytes)
    }

    foreach($int in 0..32)
    {
        $bytes = Get-NetCIDRbytes -CIDR $int
        New-Object System.Net.IPAddress(,$bytes)
    }
}