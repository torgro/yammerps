Function Set-EnableCGI
{
<# 
.Synopsis  
    This function will Enable ISAPI and CGI for IIS7 and .net 4 (x64) 
.Description 
    Needs update to handle x86 CPU and different frameworks
.Example 
    Set-EnableCGI
    
.Parameter ""
	
.Role 
    General 
.Component 
    SMART 
.Notes 
    NAME: Set-EnableCGI 
    AUTHOR: Tore Groneng tore@firstpoint.no @toregroneng tore.groneng@gmail.com
    LASTEDIT: March 2013 
    KEYWORDS: General scripting SMART
    HELP:OK
.Link 
    Http://www.firstpoint.no 
#Requires -Version 2.0 
#> 
	try
	{
		if (Install-IISPSWebadm -eq $true)
		{
			$frameworkPath = "$env:windir\Microsoft.NET\Framework64\v4.0.30319\aspnet_isapi.dll"
			$isapiConfiguration = get-webconfiguration "/system.webServer/security/isapiCgiRestriction/add[@path='$frameworkPath']/@allowed"
			if (!$isapiConfiguration.value)
			{  
			   set-webconfiguration "/system.webServer/security/isapiCgiRestriction/add[@path='$frameworkPath']/@allowed" -value "True" -PSPath:IIS:\  
			   Write-Host "Enabled ISAPI - $frameworkPath " -ForegroundColor Green  
			}  
		}
	}
	catch
	{
		$ex = $_.Exception.message
		write-error "Exception in Set-EnableCGI - $ex"
	}
}