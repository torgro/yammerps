﻿Function Get-ServiceStartMode
{
[cmdletbinding()]
Param(
    [Parameter(ValueFromPipelineByPropertyName=$true)]
    [Alias("psComputerName","MachineName")]
    [string[]] 
    $ComputerName
    ,
    [Parameter(ValueFromPipelineByPropertyName=$true)]
    [Alias("Name")]
    [string[]] 
    $ServiceName
)
    Begin
    {
        $f = $MyInvocation.InvocationName
        Write-Verbose -Message "$f - START"        
    }
    #FIXME
    Process
    {        
        foreach ($service in $ServiceName)
        {
            Write-Verbose "Now processing service $service"
            $getService = @{}

            foreach ($computer in $ComputerName)
            {
                Write-Verbose -Message "Now processing computer $computer"

                $outObject = "" | Select -Property ServiceObject, StartMode, ComputerName

                $ServiceObject = $null
                $GetWMI = @{
                    ErrorAction = "SilentlyContinue"
                }
                
                $ServiceObject = Get-WmiObject -Query "select * from win32_service where name = '$service'" -ComputerName $computer -ErrorAction SilentlyContinue
                if ($ServiceObject)
                {
                    $psServiceObject = Get-Service -Name $service -ComputerName $computer
                    $psServiceObject | Add-Member -MemberType NoteProperty -Name StartupMode -Value $ServiceObject.startmode
                    $outObject.ServiceObject = $psServiceObject
                    $outObject.StartMode = $ServiceObject.startmode
                    $outObject.ComputerName = $computer
                    $outObject
                }
                else
                {
                    Write-Error "Unable to find service $service and/or connect to computer $ComputerName"
                }
            }
        }
    }

    End
    {
        Write-Verbose -Message "END"
    }
}


