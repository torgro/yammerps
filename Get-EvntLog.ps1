<#PSScriptInfo

.VERSION 1.0

.GUID e3221ab1-8c04-42b7-8e4d-a0c4c323d414

.AUTHOR Tore Grøneng

.DESCRIPTION 
 Displays the eventlog/Event Viewer entries. Filter on eventID and logname

.COMPANYNAME 

.COPYRIGHT 

.TAGS 

.LICENSEURI 

.PROJECTURI 

.ICONURI 

.EXTERNALMODULEDEPENDENCIES 

.REQUIREDSCRIPTS 

.EXTERNALSCRIPTDEPENDENCIES 

.RELEASENOTES


#> 

function Get-EvntLog
{
<#
.SYNOPSIS
    Displays the eventlog/Event Viewer entries. Filter on eventID and logname
.DESCRIPTION
    Get eventlog entries, from any provider. Filter on EventID. Returns by default
    the 5 last entries. Use the count parameter to display more/less.
.PARAMETER ComputerName
    Single computer name or an array of computers. Defaults to current computer if left blank
.PARAMETER Count
    Number of entries to return, default is 5
.PARAMETER EventID
    The EventID, default is 6005 (Eventlog service started)
.PARAMETER LogName
    The name of the log you want to query (Application, System etc). Defaults to System
.EXAMPLE
    Get-EvntLog -ComputerName "Server1" -EventID 6005

    Returns the last 5 events (Eventlog Service started event) from the System Eventlog
.EXAMPLE
    Get-EvntLog -ComputerName Server1 -count 10

    Returns the last 5 events from the System Eventlog
.EXAMPLE
    "Server1","Server2" | Get-EvntLog -EventID 6005

    Returns the last 5 events with ID 6005 from the System Eventlog for server1 and server2
   
.NOTES 
     SMART
     AUTHOR: Tore Groneng tore@firstpoint.no @toregroneng tore.groneng@gmail.com
#>
[CmdletBinding()]
param(
       [Parameter(ValueFromPipeline=$True)]
       [Alias("Name")]
       [string[]]$ComputerName
       ,
       [int]$Count = 5
       ,
       [int]$EventID = 6005
       ,
       [ValidateSet("Application","HardwareEvents","Internet Explorer","Operations Manager","Security","System")]
       [string]$EventLogName = "System"
       )
    BEGIN
    {
    $f = $MyInvocation.MyCommand.name
    write-verbose "Running function '$f'"
    Write-Verbose "Computerobjects $($ComputerName.Count)"
    write-verbose "In begin section"
    $eventID = 6005
    write-verbose "Building filter"
    $hash = @{
                #providername = "eventlog";
                #providername = "Microsoft-Windows-Security-Auditing"
                logname = "$EventLogName"
                keywords = 36028797018963968
            }
    $sw = [Diagnostics.Stopwatch]::StartNew()

    Function Create-CustomObject
    {
    [cmdletbinding()]
    Param(
        [string[]] $Properties
        ,
        [String[]] $DefaultDisplayProps
        )

        write-verbose "Building a hashlist of properties"
        $hash = @{}

        foreach($prop in $Properties)
        {
            $hash.Add($prop,$null)
        }

        Write-Verbose "Creating a new object of type PSobject and adding properties"
        $obj = New-Object -TypeName PSobject -Property $hash

        Write-Verbose "Creating a DefaultDisplayPropertySet"
        $set = New-Object ([System.Management.Automation.PSPropertySet])('DefaultDisplayPropertySet',$DefaultDisplayProps)

        Write-Verbose "Creating a PSMemberInfo array"
        $members = [System.Management.Automation.PSMemberInfo[]]@($set)

        Write-Verbose "Adding MemberSet to the PSobject"
        $obj | Add-Member -MemberType MemberSet -Name PSStandardMembers -Value $members

        $obj
    }

    Write-Verbose "Saving current culture due to bug in Get-WinEvent"
    $CurrentCultureInfo = [System.Threading.Thread]::CurrentThread.CurrentCulture

    if($ComputerName.Count -eq 0 -and ($psitem | Measure-Object).Count -eq 0)
    {
        $ComputerName += $env:COMPUTERNAME
    }
    } #End BEGIN

    PROCESS 
    {
        Write-Verbose "Setting culture to en-US due to BUG"
        [System.Threading.Thread]::CurrentThread.CurrentCulture = New-Object "System.Globalization.CultureInfo" "en-US"
        foreach($computer in $ComputerName)
        {
            $customEvents = @()
            Write-Verbose "Now processing for computer $computer"
            $events = get-winevent -FilterHashtable $hash -computername $computer -verbose:$false | where {$_.id -eq $EventID} | select -first $count 
            
            Write-Verbose "Building custom output object, get properties"
            [string[]]$propsMember = $events[0] | Get-Member -MemberType Property | foreach {$_.name}
            $props = $propsMember + "Message"

            Write-Verbose "Add default display properties"
            [string[]]$displayprops = "TimeCreated","id","Message","MachineName"

            foreach($event in $events)
            {
                $obj = Create-CustomObject -Properties $props -DefaultDisplayProps $displayprops -Verbose:$false
                foreach($prop in $props)
                {
                    $obj.($prop) = $event.($prop)
                }
                $customEvents += $obj
            }
            $customEvents
    }
        
    } #End PROCESS

    END
    {
        $sw.stop()
        write-verbose "Execution took $($sw.Elapsed) ms"
        write-verbose "End $f"
        Write-Verbose "Restoring previous culture to $($currentCultureInfo.DisplayName)"
        [System.Threading.Thread]::CurrentThread.CurrentCulture = $currentCultureInfo
    }
} 