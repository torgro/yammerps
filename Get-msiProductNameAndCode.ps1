﻿Function Get-MsiProductCodeAndName
{
<#
    .SYNOPSIS 
      Get the product name and GUID of an MSI-package
    .Description
      It also returns the path of the MSI. Supports Pipeline input and arrayinput 
    .Parameter PathToMSI
     Path and filename of MSI
    .EXAMPLE
     Get-MsiProductCodeAndName -PathToMSI "c:\temp\file.msi
    .EXAMPLE
     Get-MsiProductCodeAndName -PathToMSI "c:\temp\file.msi","C:\TMP\webdeploy.msi"
    .EXAMPLE
     get-childitem *.msi | Get-MsiProductCodeAndName
    .Notes
     NAME: Get-MsiProductCodeAndName 
     AUTHOR: Tore Groneng tore@firstpoint.no @toregroneng tore.groneng@gmail.com
     LASTEDIT: March 2014 
     KEYWORDS: General scripting DSC
     HELP:OK
#>
[cmdletbinding()]
Param(
        [Parameter(
            Mandatory=$true,
            ValueFromPipeline=$true
        )]
        [string[]] $PathToMSI
    )
BEGIN{

Write-Verbose "Resolve relative paths"
$newPaths = @()
foreach ($p in $PathToMSI)
{
    $newPaths += (Resolve-Path -Path $p).path
}

$PathToMSI = $newPaths

$MsiTool = @'
    [DllImport("msi.dll", CharSet = CharSet.Unicode, PreserveSig = true, SetLastError = true, ExactSpelling = true)]
    private static extern UInt32 MsiOpenPackageW(string szPackagePath, out IntPtr hProduct);

    [DllImport("msi.dll", CharSet = CharSet.Unicode, PreserveSig = true, SetLastError = true, ExactSpelling = true)]
    private static extern uint MsiCloseHandle(IntPtr hAny);

    [DllImport("msi.dll", CharSet = CharSet.Unicode, PreserveSig = true, SetLastError = true, ExactSpelling = true)]
    private static extern uint MsiGetPropertyW(IntPtr hAny, string name, StringBuilder buffer, ref int bufferLength);

    private static string GetPackageProperty(string msi, string property)
    {
        IntPtr MsiHandle = IntPtr.Zero;
        try
        {
            var res = MsiOpenPackageW(msi, out MsiHandle);
            if (res != 0)
            {
                return null;
            }
            
            int length = 256;
            var buffer = new StringBuilder(length);
            res = MsiGetPropertyW(MsiHandle, property, buffer, ref length);
            return buffer.ToString();
        }
        finally
        {
            if (MsiHandle != IntPtr.Zero)
            {
                MsiCloseHandle(MsiHandle);
            }
        }
    }
    public static string GetProductCode(string msi)
    {
        return GetPackageProperty(msi, "ProductCode");
    }
    
    public static string GetProductName(string msi)
    {
        return GetPackageProperty(msi, "ProductName");
    }
'@

    $TypeParam = @{ "PassThru" = $true;
                    "Namespace" = "Microsoft.Windows.DesiredStateConfiguration.PackageResource";
                    "Name" = "MsiTools";
                    "UsingNamespace" = "System.Text";
                    "MemberDefinition" = $MsiTool
                    }

   $ErrorActionPreference = "SilentlyContinue"
   $msi = $null
   $msi = [Microsoft.Windows.DesiredStateConfiguration.PackageResource.MsiTools]
   $ErrorActionPreference = "Continue"
   if($msi -ne $null)
   {
        Write-Verbose "Type alreay added"        
   }
   else
   {
        Write-Verbose "Adding type MsiTools"
        Add-Type @TypeParam | Out-Null
        $msi = [Microsoft.Windows.DesiredStateConfiguration.PackageResource.MsiTools]
   }
   
}

Process{

    if($msi -ne $null)
    {
       $MsiInfo = "" | Select ProductName, ProductGUID,path
       $msiinfo.ProductName = $msi::GetProductName($PathToMSI)
       $msiinfo.ProductGUID = $Msi::GetProductCode($PathToMSI)
       if($psitem -ne $null)
       {
            $msiinfo.path = $PSItem
       }
       else
       {
            $msiinfo.path = $PathToMSI[0]
       }
       $msiinfo
    }
    else
    {
        Write-Error "Unable to create Microsoft.Windows.DesiredStateConfiguration.PackageResource.MsiTools object"
    }
}
}