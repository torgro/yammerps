﻿function Get-ComputerSingleAttribute
{ 
[cmdletbinding()]
Param
(
    # [Parameter(Mandatory=$false)]
    [String] 
    $attribute = "Name"
    ,
    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    $Computer= (read-host)
    ,
    $adquery= (Get-ADComputer -identity "$computer" -Properties "$attribute" | Select-Object -ExpandProperty "$attribute")
)

BEGIN
{}

PROCESS
{
$adquery
$adquery | clip.exe;
}

END
{}
}

function Get-Adcomputer
{
[cmdletbinding()]
Param(
    [string]$Identity
)

[pscustomobject]@{
    Name = $Identity
    Class = "computer"
}
}

(Get-Adcomputer -Identity torgro -PipelineVariable pv).Name | foreach-object {out-file -FilePath "c:\temp\$($pv.class).txt" -Append}

    function Send-PingAsync {
<#
Author: Jesse Davis (@secabstraction)
License: BSD 3-Clause
#>
[CmdLetBinding()]
     Param(
        [Parameter(Mandatory = $true, Position = 0, ValueFromPipeline = $true)]
        [ValidateNotNullOrEmpty()]
        [String[]]$ComputerName,
        
        [Parameter(Position = 1)]
        [ValidateNotNullOrEmpty()]
        [Int32]$Timeout = 250
    ) #End Param

    $Pings = New-Object Collections.Arraylist

    foreach ($Computer in $ComputerName) {
        [void]$Pings.Add((New-Object Net.NetworkInformation.Ping).SendPingAsync($Computer, $Timeout))
    }
    Write-Verbose "Waiting for ping tasks to complete..."
    [Threading.Tasks.Task]::WaitAll($Pings)

    foreach ($Ping in $Pings) { Write-Output $Ping.Result }
}

