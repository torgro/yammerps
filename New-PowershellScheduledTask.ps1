﻿function New-PowershellScheduledTask
{
<#
.Synopsis
   Create a Scheduled task to execute a powershell script

.DESCRIPTION
   Create a Scheduled task to execute a powershell script

.EXAMPLE
   $NewTask = @{
      PowershellScriptFile = "C:\Script\Invoke-Pester.ps1"
      TaskName = "Start Pester"
   }
   New-PowershellScheduledTask @NewTask

   This will create a new scheduled task which will execute the invoke-pester.ps1 scriptfile
.EXAMPLE

   $NewTask = @{
      PowershellScriptFile = "C:\Script\Invoke-Pester.ps1"
      TaskName = "Start Pester"
      PassThru = $true
   }
   $Task = New-PowershellScheduledTask @NewTask

   This will create a new task, however it will not register it with the Task Scheduler. You
   may do this by running 
   $task | Register-ScheduledTask -TaskName "MyTask"
#>
[cmdletbinding()]
Param(
    [string]
    $PowershellScriptFile
    ,
    [int]
    $TriggerDelay = "10"
    ,
    $Trigger = (New-ScheduledTaskTrigger -AtLogOn)
    ,
    [Parameter(Mandatory)]
    [string]
    $TaskName 
    ,
    [String]
    $Description = "Powershell task created by New-PowershellScheduledTask"
    ,
    [switch]
    $PassThru

)

    if (-not (Test-Path -Path $PowershellScriptFile))
    {
        Write-Warning -Message "File [$PowershellScriptFile] was not found"
    }

    $NewTaskAction = @{
        Execute = 'C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe' 
        Argument = "-NonInteractive -NoLogo -NoProfile -File $PowershellScriptFile"
    }
    $action = New-ScheduledTaskAction @NewTaskAction

    $Trigger.Delay = "PT$($TriggerDelay)S"

    $settings = New-ScheduledTaskSettingsSet

    $newTask = @{
        Action = $action
        Trigger = $Trigger
        Settings = $settings
        Description = $Description
    }

    $task = New-ScheduledTask @newTask

    if ($PassThru)
    {
        $task
    }
    else
    {
        Register-ScheduledTask -InputObject $task -TaskName $TaskName
    }
}