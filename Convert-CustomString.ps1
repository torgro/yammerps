﻿function Convert-CustomString
{
[cmdletbinding()]
Param(
    [Parameter(ValueFromPipeline)]
    [string]
    $InputObject
    ,
    [string]
    $ObjectSplitChar
    ,
    [string]
    $ItemSplitChar
)
    $array = $InputObject.Split($ObjectSplitChar)
    $itemCountArray = New-Object -TypeName System.Collections.Generic.List[int]

    foreach ($item in $array)
    {
        $itemArray = $item.Split($ItemSplitChar)
        $count = ($itemArray | Measure-Object).Count
        $itemCountArray.Add($count)
    }

    $maxProperties = ($itemCountArray | Measure-Object -Maximum).Maximum

    foreach ($item in $array)
    {
        $output = [ordered]@{}
            
        foreach ($i in (1..$maxProperties))
        {
            $output.Add("Item$i","")
        }

        $itemArray = $item.Split($ItemSplitChar)
        $counter = 1
        foreach ($obj in $itemArray)
        {
            if ($counter -eq $itemArray.Count -and $counter -ne $maxProperties)
            {
                $counter++
                $output["item$counter"] = $obj
                continue
            }
            $output["item$counter"] = $obj
            $counter++
        }
        [PScustomObject]$output
    }        
}
