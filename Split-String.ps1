﻿function Split-String
{
<#
.SYNOPSIS
    Splits a string into an array.

.DESCRIPTION
    Splits any string into chuncks of your choice, pair of 2 or more. 

.PARAMETER NumberOfChars
    Number of characters each element should consist of

.PARAMETER InputString
    The string-object you want to split into chuncks

.EXAMPLE
    Split-String -NumberOfChars 2 -InputString "aabbcc"

    aa
    bb
    cc

    Returns an array of 3 elements. First element (base 0) is aa, second is bb and third is cc
  
.NOTES 
     SMART
     Logic by Knut Magne Huglen 
     AUTHOR: Tore Groneng tore@firstpoint.no @toregroneng tore.groneng@gmail.com
#>
[CmdletBinding()]
[OutputType([String[]])]
Param(
    [int]
    $NumberOfChars = 2
    ,
    [Parameter(Mandatory=$true)]
    [string]
    $InputString
)
    
    [int]$int = $null
    [bool]$IsEven = [int]::TryParse(($InputString.Length / $NumberOfChars),[ref]$int)

    if ($IsEven -eq $false)
    {
        Write-Error -Message "Inputstring length and NumberOfChars parameter does not yield a even number" -Category InvalidArgument -ErrorAction Stop
    }
   
    while ($InputString.Length -gt 0)
    {
        $InputString.Substring(0, $NumberOfChars)
        $InputString = $InputString.Substring($NumberOfChars)
    }
}