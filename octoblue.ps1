﻿function Lab-Shutdown {
  $ErrorActionPreference = 'Continue'
  import-module XenServerPSModule
  connect-xenserver -url http://xs.bretty.local -username USERNAME -password PASSWORD
  $file = "c:\Scripts\vmlist.txt"
  $VMNames = Get-Content $file
  foreach($VMName in $VMNames)
    {
      $VM = Get-XenVM -Name $VMName
      Write-Host "Scheduling Power off of VM '$VMName'"
      Invoke-XenVM -VM $VM -XenAction CleanShutdown -Async
    }
  Disconnect-XenServer
  $From = "xs@bretty.me.uk"
  $To = "dave@bretty.me.uk"
  $SMTPServer = "xsmail.bretty.me.uk"
  $SMTPPort = "25"
  $Username = "xs@bretty.me.uk"
  $Password = "PASSWORD"
  $subject = "bretty.local Lab Shutdown"
  $body = "bretty.local - lab has been shutdown"
  $smtp = New-Object System.Net.Mail.SmtpClient($SMTPServer, $SMTPPort);
  $smtp.EnableSSL = $false
  $smtp.Credentials = New-Object System.Net.NetworkCredential($Username, $Password);
  $smtp.Send($From, $To, $subject, $body);
}


# Added from http://bretty.me.uk/save-money-using-citrix-octoblu-automation-for-power-management/
# Author: Dave Brett 
function Lab-Startup {
  $ErrorActionPreference = 'Continue'
  import-module XenServerPSModule
  connect-xenserver -url http://xs.bretty.local -username USERNAME -password PASSWORD
  $file = "c:\Scripts\vmlist.txt"
  $VMNames = Get-Content $file
  foreach($VMName in $VMNames)
    {
      $VM = Get-XenVM -Name $VMName
      Write-Host "Scheduling Power on of VM '$VMName'"
      Invoke-XenVM -VM $VM -XenAction Start -Async
    }
  Disconnect-XenServer
  $From = "xs@bretty.me.uk"
  $To = "dave@bretty.me.uk"
  $SMTPServer = "xsmail.bretty.me.uk"
  $SMTPPort = "25"
  $Username = "xs@bretty.me.uk"
  $Password = "PASSWORD"
  $subject = "bretty.local Lab Startup"
  $body = "bretty.local - lab has been started"
  $smtp = New-Object System.Net.Mail.SmtpClient($SMTPServer, $SMTPPort);
  $smtp.EnableSSL = $false
  $smtp.Credentials = New-Object System.Net.NetworkCredential($Username, $Password);
  $smtp.Send($From, $To, $subject, $body);
}

function Set-LabPowerstate
{
<# 
.Synopsis 
   Sets powermode for VMs
.DESCRIPTION 
   This can shutdown or restart a VM or a collection of VMs. The function supports WhatIf. The XenServerPSModule
   and vaild credentials on the XenServer is required to run this function. This function supports pipelineinput
   for the VMname parameter.
   
.EXAMPLE 
    PS> $SetLabPowerstate = @{
            Credential = Get-Credential
            XenServerUrl = "http://xenServer.domain.local"
            VMname = "DC01"
        }
    PS> Set-LabPowerstate @SetLabPowerstate

This will startup the VM DC01.

.EXAMPLE 
   PS> $SetLabPowerstate = @{
            Credential = Get-Credential
            XenServerUrl = "http://xenServer.domain.local"
            VMname = "DC01"
            Shutdown = $true
       }
   PS> Set-LabPowerstate @SetLabPowerstate

   This will shutdown the VM DC01.

.EXAMPLE 
   PS> $SetLabPowerstate = @{
            Credential = Get-Credential
            XenServerUrl = "http://xenServer.domain.local"
            Shutdown = $true
       }
   PS> "DC01","DC02" | Set-LabPowerstate @SetLabPowerstate

   This will shutdown the nodes DC01 and DC02 using the pipeline   
#>
[cmdletbinding(
    SupportsShouldProcess=$true,
    ConfirmImpact='Medium')]
Param
(
    [Parameter(Mandatory)]
    [PScredential]
    $Credential
    ,
    [Parameter(Mandatory)]
    [string]
    $XenServerUrl
    ,
    [Parameter(Mandatory, ValueFromPipeline)]
    [string[]]
    $VMname
    ,
    [switch]
    $Shutdown
)

Begin
{
    $f =  $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"

    Import-Module -Name XenServerPSModule -ErrorAction Stop

    $ConnectXenServer = @{
        url = $XenServerUrl
        UserName = $Credential.GetNetworkCredential().UserName
        Password = $Credential.GetNetworkCredential().Password
    }

    Write-Verbose -Message "$f -  Connecting to XENserver $XenServerUrl"
    Connect-XENserver @ConnectXenServer -ErrorAction Stop
}

Process
{  
    foreach($Name in $VMname)
    {
        $VM = Get-XenVM -Name $Name

        if (-not $vm)
        {
            Write-Warning -Message "$f -  VM with name [$Name] was not found"
            continue
        }

        $output = [pscustomobject]@{
            VMName = $Name
            Action = "Shutdown"
        }
    
        if ($PSBoundParameters.ContainsKey("Shutdown"))
        {
            Write-Verbose -Message "$f -  Scheduling Power off of VM '$Name'"
            if ($PSCmdlet.ShouldProcess("$Name", "Shutting down"))
            {
                #Invoke-XenVM -VM $VM -XenAction CleanShutdown -Async
                $output
            }        
        }
        else
        {
            Write-Verbose -Message "Scheduling Power on of VM '$Name'"
            if ($PSCmdlet.ShouldProcess("$Name", "Starting up"))
            {
                #Invoke-XenVM -VM $VM -XenAction Start -Async
                $output.Action = "StartUp"
                $output
            }        
        }       
    }
    
    Write-Verbose -Message "$f -  Disconnect from XENserver"
    #Disconnect-XenServer
}

End
{
    Write-Verbose -Message "$f - END"
}
}

function Get-XENvm
{
Param(
    $Name
)

if($Name -eq "testvm")
{
    [pscustomobject]@{
        Name = "testvm"
    }
}

}

$vmListFilePath = "c:\Scripts\vmlist.txt"

if (Test-Path -Path "$vmListFilePath")
{
    $setLabPower = @{
        Credential = Import-Clixml -Path "c:\scripts\XenCred.xml"
        XenServerUrl = "http://xs.bretty.local"
        Shutdown = $true
    }

    $result = foreach($vm in (Get-Content -Path "$vmListFilePath"))
    {
        Set-LabPowerState @setLabPower -VMname $vm
    }

    $sendMail = @{
        From       = "xs@bretty.me.uk"
        To         = "dave@bretty.me.uk"
        SMTPServer = "xsmail.bretty.me.uk"
        Subject    = "bretty.local Lab action"
        Body       = $result | Out-String
        Credential = Import-Clixml -Path "c:\scripts\mailCred.xml"
    }

    Send-MailMessage @sendMail
}



