﻿function Get-AllAst
{
[cmdletbinding()]
Param(
    [scriptblock]$ScriptBlock
    ,
    [string]$Filepath
    ,
    [string]$Path
    ,
    [switch]$OutputASTType
)
$f = $MyInvocation.InvocationName

if($PSBoundParameters.ContainsKey("ScriptBlock"))
{
    $ast = [System.Management.Automation.Language.Parser]::ParseInput($ScriptBlock,[ref]$null,[ref]$null)    
}

if($PSBoundParameters.ContainsKey("Filepath"))
{
    $file = Resolve-Path -Path $Filepath -ErrorAction SilentlyContinue
    Write-Verbose -Message "$f -  Filepath is [$($file.path)]"
    if($file.Path)
    {
        $ast = [System.Management.Automation.Language.Parser]::ParseFile($file.Path,[ref]$null,[ref]$null)
    }
}

if($ast)
{
    Write-Verbose -Message "$f -  Finding all AST"
    $all = $ast.FindAll({$args[0] -is [System.Management.Automation.Language.Ast]},$true)
}

if($all -and $OutputASTType)
{
    Write-Verbose -Message "$f -  Getting AST type for all"
    foreach($astType in $all)
    {
        $astType.GetType().Name
    }
}

if(-not $OutputASTType)
{
    Write-Verbose -Message "$f -  Outputting results"
    $all
}

Write-Verbose -Message "$f - END"
}