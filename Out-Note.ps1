﻿function Out-Note
{
<#
.Synopsis
   Send a quick note to a new OneNote page
.DESCRIPTION
   This functions uses COM-object to interact with OneNote. It requires OneNote 2013 and .net 4.0 to function correctly.
.EXAMPLE
   Out-Note -Note "Remember to call Frank on 12/12 by lunch"

   This will create a new page in OneNote in the General section with the message above

.EXAMPLE
   Out-Note -section "UnfiledNotes" -Note "Friday is taco's day"

   This will create a new page in OneNote in the UnfiledNotes section with the message above

.NOTES

.FUNCTIONALITY
   Quickly make notes in OneNote from powershell
#>
[cmdletbinding()]
Param(
    [string]$Note
    ,
    [String]$SectionName = "General"
)
    # http://stackoverflow.com/questions/8186819/creating-new-one-note-2010-page-from-c-sharp
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"

    $onenote = New-Object -ComObject OneNote.Application -ErrorAction SilentlyContinue

    if (-not $onenote)
    { 
        throw "Error - Unable to create OneNoe application object (COMobject error)"
    }

    $scope = [Microsoft.Office.Interop.OneNote.HierarchyScope]::hsPages
    [ref]$xml = ""

    $onenote.GetHierarchy($null, $scope, $xml)

    $schema = @{one="http://schemas.microsoft.com/office/onenote/2013/onenote"}
    
    Write-Verbose -Message "$f -  Finding section in OneNote"
    $xpath = "//one:Notebook/one:Section"
    Select-Xml -Xml ([xml]$xml.Value) -Namespace $schema -XPath $xpath | foreach {
        if ($psitem.Node.Name -eq $SectionName)
        { 
            $SectionID = $psitem.Node.ID
        }
    }

    if (-not $SectionID)
    { 
        throw "Unable to find Section with name $SectionName"
    }

    Write-Verbose -Message "$f -  Creating new page"

    [ref]$newpageID =""
    $onenote.CreateNewPage("$SectionID",[ref]$newpageID,[Microsoft.Office.Interop.OneNote.NewPageStyle]::npsBlankPageWithTitle)

    if (-not $newpageID.Value)
    { 
        throw "Unable to create new OneNote page"
    }

    Write-Verbose -Message "$f -  Getting page content"
    [ref]$NewPageXML = ""
    $onenote.GetPageContent($newpageID.Value,[ref]$NewPageXML,[Microsoft.Office.Interop.OneNote.PageInfo]::piAll)

    if (-not $NewPageXML)
    { 
        throw "Unable to get OneNote page content"
    }

    Write-Verbose -Message "$f -  Parsing new page xml content to find the title element"
    
    [Reflection.Assembly]::LoadWithPartialName("System.Xml.Linq") | Out-Null
    $xDoc = [System.Xml.Linq.XDocument]::Parse($NewPageXML.Value)

    $title = $xDoc.Descendants() | where Name -like "*}T"

    if (-not $title)
    { 
        throw "Unable to get title of new onenote page"
    }

    $title.Value = "$Note"

    Write-Verbose -Message "$f -  Setting page title to $Note"
    $onenote.UpdatePageContent($xDoc.ToString())

$NewPageContent = @"
    <one:Page xmlns:one="http://schemas.microsoft.com/office/onenote/2013/onenote" ID="$($newpageID.Value)" >
            <one:Outline>
                <one:Position x="36.0" y="86.4000015258789" z="0" />
                <one:Size width="117.001953125" height="40.28314971923828" />
                <one:OEChildren> 
                    <one:OE>
                        <one:T><![CDATA[$Note]]></one:T>
                    </one:OE>
                </one:OEChildren>                
            </one:Outline>
    </one:Page>
"@
    Write-Verbose -Message "$f -  Inserting note on page"
    $onenote.UpdatePageContent($NewPageContent)
}