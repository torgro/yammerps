﻿function Get-Pipe
{
[cmdletbinding()]
Param(
    [Parameter(ValueFromPipeLine)]
    [string]
    $Name
)

Process
{
    foreach ($path in [System.IO.Directory]::GetFiles("\\.\pipe\"))
    {
        if ($PSBoundParameters.ContainsKey("Name"))
        {
            if ($path -like $Name)
            {
                $path
            }
        }
        else
        {
            $path
        }
    }
}

End
{

}
}