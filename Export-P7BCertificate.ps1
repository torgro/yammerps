﻿function Export-P7BCertificate 
{
[CmdletBinding()]
Param(
    [Parameter(Mandatory = $true)]
    [Security.Cryptography.X509Certificates.X509Certificate2]
    $Certificate
    ,
    [Parameter(Mandatory = $true)]
    [string]
    $OutputFile
    ,
    [System.Security.Cryptography.X509Certificates.X509RevocationMode]
    $RevokationMode = ([System.Security.Cryptography.X509Certificates.X509RevocationMode]::NoCheck)
    
)
    $collection = New-Object -Typename Security.Cryptography.X509Certificates.X509Certificate2Collection
   
    $chain = New-Object -TypeName Security.Cryptography.X509Certificates.X509Chain
    $chain.ChainPolicy.RevocationMode = $RevokationMode
    $null = $chain.Build($Certificate)

    foreach ($cert in $chain.ChainElements)
    {
        $null = $collection.Add($cert.Certificate)
    }

    $chain.Reset()
   
    Set-Content -Path $OutputFile -Value $collection.Export([System.Security.Cryptography.X509Certificates.X509ContentType]::Pkcs7) -Encoding Byte
}


