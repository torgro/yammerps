﻿function Restart-RemoteService
{
<#
.SYNOPSIS
Restart-RemoteService restarts services on remote computers

.PARAMETER Computername
	[string] Name of computer running the service

.PARAMETER Name
	[string] Name of service that should be restarted

.EXAMPLE
	Restart-RemoteService -Computername server1.contoso.local -Name healthservice

	Restarts the 'healthservice' service on computer server1.contoso.local

.NOTES 
     SMART
	 AUTHOR: Tore Groneng tore@firstpoint.no @toregroneng tore.groneng@gmail.com
#> 
[CmdletBinding()]
param 
	(
	[Alias("Host")]
	[string]$ComputerName = ""
	,
	[Alias("ServiceName")]
	[string]$Name = ""
	)
	
	Try
	{
		$f = $MyInvocation.MyCommand.name
		Write-Verbose -Message "Starting $f"
		Write-Verbose -Message "Computername = $ComputerName"
		Write-Verbose -Message "Service = $Name"

		if ($ComputerName -ne "")
		{
			if ($Name -ne "")
			{
				$svc = get-service -name $name -computername $Computername
				if ($svc -ne $null)
				{
					Write-Verbose -Message "Restarting service '$Name' on computer '$ComputerName'"
					restart-service -inputobject $svc
					[string]$exe = (get-wmiobject -query "Select * from win32_service where name='$name'" -ComputerName $ComputerName).PathName
					if ($exe -ne "")
					{
						Write-Verbose -Message "Have Service executeable $exe"
						$exe = $exe.replace('"','')
						[string]$ExeName = [System.IO.Path]::GetFileName($exe)
						[string]$FullPath = [System.IO.Path]::GetDirectoryName($exe) + "\"
						if ($ExeName -ne "")
						{
							Write-Verbose -Message "Have fileName '$ExeName', looking for the process"
							$process = Get-WmiObject -computername $Computername -query "select * from Win32_Process where name='$ExeName'"
							if ($process -ne $null)
							{
								Write-Verbose -Message "Have process, looking for the startTime"
							
								#[wmi]$wmi | out-null
								Write-Verbose -Message "Created wmi-object"
								$startTime = ([wmi]"").ConvertToDateTime($process.CreationDate)
								Write-Verbose -Message "Converted date to readable format $starttime"
								$output = "" | select ProcessName, StartTime, Path, Computer
								Write-Verbose -Message "Created output object"
								$output.ProcessName = $ExeName 
								$output.StartTime = $startTime
								$output.path = $FullPath
								$output.computer = $ComputerName
								Write-Verbose -Message "Outputing results"
								$output
								
							}
							else
							{
								Write-Verbose -Message "Could not find a process for executable '$ExeName'"
							}
						}
						else
						{
							Write-Verbose -Message "Unable to get fileName"
						}
					}
					else
					{
						Write-Verbose -Message "Could not find service PathName"
					}
				}
			}
			else
			{
				Write-Verbose -Message "No ServiceName specified!"
			}
		}
		else
		{
			Write-Verbose -Message "No computername specified!"
		}
	}
	Catch
	{
		$ex = $_.Exception
		$exMsg = Get-InstallerException -ex $ex
		write-host $exMsg
	}
}