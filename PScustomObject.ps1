

Function New-CustomObject
{
[cmdletbinding()]
Param(
    [string[]] $Properties
    ,
    [String[]] $DefaultDisplayProps
    )

    write-verbose "Building a hashlist of properties"
    $hash = @{}

    foreach($prop in $Properties)
    {
        $hash.Add($prop,$null)
    }

    Write-Verbose "Creating a new object of type PSobject and adding properties"
    $obj = New-Object -TypeName psobject -Property $hash

    Write-Verbose "Creating a DefaultDisplayPropertySet"
    $set = New-Object ([System.Management.Automation.PSPropertySet])('DefaultDisplayPropertySet',$DefaultDisplayProps)

    Write-Verbose "Creating a PSMemberInfo array"
    $members = [System.Management.Automation.PSMemberInfo[]]@($set)

    Write-Verbose "Adding MemberSet to the PSobject"
    $obj | Add-Member -MemberType MemberSet -Name PSStandardMembers -Value $members

    $obj
}

[string[]]$Properties = "Name","Full","phone","Address"
[string[]]$DefaultDisplay = "Name","Address"

$custom = Create-CustomObject -Properties $Properties -DefaultDisplayProps $DefaultDisplay


