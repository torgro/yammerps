﻿function Get-ParseResult
{
[cmdletbinding()]
Param(
    [string]
    $InputObject = 'string,@{key="value1","value2"},"another string",@("1","2","3")'
)

$ParseErrors = @()
$FakeCommand = [scriptblock]::Create("fake-baf18518-b7da-4f09-b6a3-734fe2f8ca4d -FakeParameter $InputObject")
$AST = [System.Management.Automation.Language.Parser]::ParseInput($FakeCommand,[ref]$null,[ref]$ParseErrors)

if ($null -ne $ParseErrors)
{
    foreach ($err in $ParseErrors)
    {
        throw $err
    }
}

$findCommand = $AST.Find(
    {
        param($ChildAst) 
        $ChildAst -is [System.Management.Automation.Language.CommandAst]
    },
    $false
)

$ArgumentAst = $findCommand.CommandElements[2]

if ($null -eq $ArgumentAst)
{
    Write-Error -Message "Unable to parse string [$InputObject]" -ErrorAction Stop
}

$arguments = [System.Collections.Generic.List[PSCustomobject]]::new()
$counter = 0

foreach ($arg in $ArgumentAst.Elements)
{
    Write-Verbose -Message "$($arg.gettype())" -Verbose
    Write-Verbose -Message $arg.Extent.Text -Verbose

    if ($arg -is [System.Management.Automation.Language.StringConstantExpressionAst])
    {
        $object = [pscustomobject]@{
            Position = $counter
            Extent = $arg.Extent.Text
            Value = $arg.Extent.Text
            AstType = $arg.GetType().Name
            ObjectType = $arg.Extent.Text.GetType()
        }
        $object.pstypenames.Insert(0,"Arg.Parsed")
        $Arguments.Add($object)
    }
    
    if ($arg -isnot [System.Management.Automation.Language.StringConstantExpressionAst])
    {
        $cmd = [scriptblock]::Create($arg.Extent.Text)
        Write-Verbose -Message "isnot string" -Verbose
        $objectValue = &$cmd
        $object = [pscustomobject]@{
            Position = $counter
            Extent = $arg.Extent.Text
            Value = $objectValue
            AstType = $arg.GetType().Name
            ObjectType = $objectValue.GetType()
        }
        $object.pstypenames.Insert(0,"Arg.Parsed")
        $Arguments.Add($object)
    }
    $counter++
}

if (-not (Get-TypeData -TypeName "Arg.Parsed"))
{
    Update-TypeData -TypeName "Arg.Parsed" -DefaultDisplayPropertySet  Position,Value,AstType,ObjectType
}

$arguments

}

