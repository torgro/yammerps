﻿function Find-CommandCalled
{
 [CmdletBinding(DefaultParameterSetName='none')]
<#
.Synopsis
   Find commands called in scripts, files or entire paths containing ps1 or ps1m files
.DESCRIPTION
   It finds commands recursivly in the current working directory.
.EXAMPLE
   Find-CommandCalled -CommandName Write-Verbose -FilePath c:\temp\script.ps1

   This will find all calls to the Write-Verbose command in the file c:\temp\script.ps1
.EXAMPLE
   Get-Item -Path c:\temp\script.ps1 | Find-CommandCalled -CommandName Write-Verbose

   This will find all calls to the Write-Verbose command in the file c:\temp\script.ps1 using the pipeline
.EXAMPLE
   Get-Item -Path c:\temp\script.ps1 | Find-CommandCalled

   This will find all calls to ANY command in the file c:\temp\script.ps1 using the pipeline
.EXAMPLE
   Get-Item -Path c:\temp | Find-CommandCalled -CommandName Write-Verbose

   This will find all calls to the Write-Verbose command in the files in or below c:\temp using the pipeline
.EXAMPLE
   Find-CommandCalled -CommandName Write-Verbose -Path (Get-Item -Path c:\temp)

   This will find all calls to the Write-Verbose command in the files in or below c:\temp
.EXAMPLE
   $scriptBlock = {
        Write-Verbose -Message "verbosy"
        Invoke-Mycommand -World All
   }
   $scriptBlock | Find-CommandCalled -CommandName Invoke-Mycommand

   This will find all calls to the Invoke-Mycommand command in the scriptblock using the pipeline
.EXAMPLE
   $scriptBlock = {
        Write-Verbose -Message "verbosy"
        Invoke-Mycommand -World All
   }
   Find-CommandCalled -ScriptBlock $scriptBlock

   This will find all commands called by the scriptblock
.EXAMPLE
   Find-CommandCalled -CommandName Write-Verbose

   This will find all script or modulefiles that call Write-Verbose in or below the current directory
.INPUTS
   Scriptblock, fileinfo or directoryinfo
.OUTPUTS
   PSCustomObject
.NOTES
   General notes
.FUNCTIONALITY
   Find commands using AbstractSyntaxTree
#>
Param (    
    [string]
    $CommandName = "*"
    ,
    [Parameter(ParameterSetName='ByScriptBlock',ValueFromPipeline)]
    [scriptblock]
    $ScriptBlock
    ,
    [Parameter(ParameterSetName='ByPath',ValueFromPipeline)]
    [System.IO.DirectoryInfo]
    $Path
    ,
    [Parameter(ParameterSetName='ByFile',ValueFromPipeline,ValueFromPipelineByPropertyName)]
    [Alias("FullName")]
    [System.IO.FileInfo]
    $FilePath
)

BEGIN
{
    $f = $MyInvocation.InvocationName
   
    if (-not (Get-TypeData -TypeName AST.Command))
    {
        Update-TypeData -TypeName AST.Command -DefaultDisplayPropertySet "File","StartLineNumber","EndLineNumber","Text"
    }

    function Get-ASTcommand
    {
    [cmdletbinding()]
    Param (
        [Parameter(ValueFromPipeLine)]
        [System.Management.Automation.Language.CommandAst[]]
        $CommandAST
        ,
        [string]
        $CommandName
    )
    BEGIN
    {
        $f = $MyInvocation.InvocationName
        Write-Verbose -Message "$f - START"
    }
    PROCESS
    {
        Write-Verbose -Message "$f -  Processing"
        foreach ($command in $CommandAST)
        {
            if ($command.GetCommandName() -like "$CommandName")
            {
                $Output = $command.Extent
                $Output.PSObject.TypeNames.Insert(0,"AST.Command")
                $Output
            }
        }
    }
    END
    {
        Write-Verbose "$f - END"
    }
    }
}

PROCESS
{
    if ($PSBoundParameters.ContainsKey("ScriptBlock") -eq $false -and $PSBoundParameters.ContainsKey("Path") -eq $false -and $PSBoundParameters.ContainsKey("FilePath") -eq $false)
    {
        $Path = Get-Item -Path .\
    }

    if ($PSBoundParameters.ContainsKey("ScriptBlock"))
    {        
        $Ast = [System.Management.Automation.Language.Parser]::ParseInput($ScriptBlock,[ref]$null,[ref]$null)
        $AstCommand = $Ast.FindAll({$args[0] -is [System.Management.Automation.Language.CommandAst]},$true)
        Get-ASTcommand -CommandAST $AstCommand -CommandName $CommandName
    }

    if ($PSBoundParameters.ContainsKey("Path") -or $path -ne $null)
    {        
        foreach ($file in (Get-ChildItem -Path $Path -Recurse))
        {
            if ($file.Extension -eq ".ps1" -or $file.Extension -eq ".psm1")
            {                
                $Ast = [System.Management.Automation.Language.Parser]::ParseFile($file.FullName,[ref]$null,[ref]$null)
                $AstCommand = $Ast.FindAll({$args[0] -is [System.Management.Automation.Language.CommandAst]},$true)
                Get-ASTcommand -CommandAST $AstCommand -CommandName $CommandName
            }
        }
    }

    if ($PSBoundParameters.ContainsKey("FilePath"))
    {                        
        [string] $ResolvedFilePath = (Resolve-Path -Path $FilePath).Path
        $Ast = [System.Management.Automation.Language.Parser]::ParseFile($ResolvedFilePath,[ref]$null,[ref]$null)
        $AstCommand = $Ast.FindAll({$args[0] -is [System.Management.Automation.Language.CommandAst]},$true)
        Get-ASTcommand -CommandAST $AstCommand -CommandName $CommandName
    }
}

END
{
    Write-Verbose -Message "$f - END"
}

}