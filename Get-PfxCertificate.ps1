﻿function Get-PfxCertificate
{
[cmdletbinding()]
Param(
    [Parameter(
        ParameterSetName='ByPath',
        ValueFromPipelineByPropertyName
    )]
    [Alias('fullname','Path','PSpath')]
    [string[]]
    $FilePath
    ,
    [Parameter(
        ParameterSetName='ByLitteralPath',
        ValueFromPipelineByPropertyName
    )]
    [string[]]
    $LiteralPath
    ,
    [Parameter(ParameterSetName='ByPath')]
    [Parameter(ParameterSetName='ByLitteralPath')]
    [System.Security.SecureString]
    $Password
    ,
    [Parameter(
        ParameterSetName='ByThumbPrint',
        ValueFromPipelineByPropertyName
    )]
    [string[]]
    $ThumbPrint
    ,
    [Parameter(ParameterSetName='ByThumbPrint')]
    [System.Security.Cryptography.X509Certificates.StoreLocation]
    $StoreLocation = [System.Security.Cryptography.X509Certificates.StoreLocation]::LocalMachine
)

Begin
{
    
    $machineKeyPath = Join-Path -Path $env:ProgramData -ChildPath 'Microsoft' |
                      Join-Path -ChildPath Crypto |
                      Join-Path -ChildPath RSA |
                      Join-Path -ChildPath MachineKeys
}

Process
{
    $GetPfxCert = @{}

    if ($null -ne $FilePath)
    {
        $GetPfxCert.FilePath = $FilePath
        $paths = $FilePath
    }

    if ($null -ne $LiteralPath)
    {
        $paths = $LiteralPath
        $GetPfxCert.LiteralPath = $LiteralPath
    }

    try
    {
        if ($null -ne $Password)
        {
            $bstr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($Password)
            $clear = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($bstr)
            [System.Runtime.InteropServices.Marshal]::ZeroFreeBSTR($bstr)
        }

        if ($null -ne $ThumbPrint)
        {
            Write-Verbose -Message "Opening cert store"
            $store = [System.Security.Cryptography.X509Certificates.X509Store]::new($StoreLocation)
            $store.Open([System.Security.Cryptography.X509Certificates.OpenFlags]::ReadOnly)
            $cert = $store.Certificates | where thumbprint -eq $ThumbPrint
            $store.Close()

            if ($null -eq $cert)
            {
                Write-Error -Message "Unable to find certificate with thumbprint [$ThumbPrint]" -ErrorAction Stop
            }
    
            if ($null -ne $cert.PrivateKey)
            {
                $containerInfo = $cert.PrivateKey.CspKeyContainerInfo
                $cert | Add-Member -MemberType NoteProperty -Name ProviderName -Value $containerInfo.ProviderName
                $cert | Add-Member -MemberType NoteProperty -Name MachineKeyPath -Value (Join-Path -Path $machineKeyPath -ChildPath $containerInfo.UniqueKeyContainerName)
            }
            $cert
        }


        if ($null -eq $Password -and $null -eq $ThumbPrint)
        {
            Microsoft.PowerShell.Security\Get-PfxCertificate @GetPfxCert
        }
        else 
        {
            foreach ($file in $paths)
            {
                Write-Verbose -Message "Processing file [$file]"
                $exists = Test-Path -Path $file
                if (-not $exists)
                {
                    $exists = Test-Path -LiteralPath $file
                }

                if ($exists)
                {
                    $cert = [System.Security.Cryptography.X509Certificates.X509Certificate2]::new($file, $clear)
                    if ($null -ne $cert.PrivateKey)
                    {
                        $containerInfo = $cert.PrivateKey.CspKeyContainerInfo
                        $cert | Add-Member -MemberType NoteProperty -Name ProviderName -Value $containerInfo.ProviderName
                        $cert | Add-Member -MemberType NoteProperty -Name MachineKeyPath -Value (Join-Path -Path $machineKeyPath -ChildPath $containerInfo.UniqueKeyContainerName)
                    }
                    $cert
                }
            }
        }
    }
    catch
    {
        $ex = $_.Exception
        throw $ex
    }
    Finally
    {
        $clear = [string]::Empty
    }
}



}