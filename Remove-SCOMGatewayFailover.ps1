﻿Function Remove-SCOMGatewayFailover
{
<# 
.Synopsis  
    Removes a failover managementserver from a gateways configuration
.Description 
    All parameters are dynamic and requires the OperationsManager module to work.
.Example 
    Remove-SCOMGatewayFailover -Gateway scomgw.contoso.com -FailoverMS scomms2.contoso.com
    Removes scomms2.contoso.com from the list of failoverserver for gateway scomgw.contoso.com
.Parameter Gateway
    The gateway server you want to configure Managementserver failover for (ValidateSet with dynamic parameter)
.Parameter FailoverMS
    The primary managementserver (ValidateSet with dynamic parameter)
.Role 
    General 
.Component 
    SCOM OpsMgr 
.Notes 
    NAME: Remove-SCOMGatewayFailover
    AUTHOR: Tore Groneng tore@firstpoint.no @toregroneng tore.groneng@gmail.com
    LASTEDIT: Jan 2015 
    KEYWORDS: OpsMgr, SCOM, SCOM2012, Gateway, SCOM-Gateway
    HELP:OK
.Link 
    Http://www.firstpoint.no 
#Requires -Version 4.0 
#>
[CmdletBinding()]
Param()
    DynamicParam 
    {
        $GateWayAttribute = New-Object System.Management.Automation.ParameterAttribute
        $GateWayAttribute.Position = 0
        $GateWayAttribute.Mandatory = $true
        
        $FailoverMSattribute = New-Object System.Management.Automation.ParameterAttribute
        $FailoverMSattribute.Position = 2
        $FailoverMSattribute.Mandatory = $true
        
        $attributeCollectionGW = new-object System.Collections.ObjectModel.Collection[System.Attribute]
        $attributeCollectionGW.Add($GateWayAttribute)

        $attributeCollectionFailover = new-object System.Collections.ObjectModel.Collection[System.Attribute]
        $attributeCollectionFailover.Add($FailoverMSattribute)

        $ValidateSetGatewayValues = Get-SCOMManagementServer | Where-Object isGateway -eq $true | Select-Object -ExpandProperty DisplayName
        $ValidateSetAttributeGW = New-Object System.Management.Automation.ValidateSetAttribute($ValidateSetGatewayValues)
        $attributeCollectionGW.Add($ValidateSetAttributeGW)

        $ValidateSetValuesMS = Get-SCOMManagementServer | Where-Object isGateway -eq $false | Select-Object -ExpandProperty DisplayName
        $ValidateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute($ValidateSetValuesMS)
        $attributeCollectionFailover.Add($ValidateSetAttribute)

        $GatewayParam = New-Object System.Management.Automation.RuntimeDefinedParameter('Gateway', [string], $attributeCollectionGW)
        $FailoverMSParam = New-Object System.Management.Automation.RuntimeDefinedParameter('FailoverMS', [string], $attributeCollectionFailover)
        
        $paramDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        $paramDictionary.Add('Gateway',$GatewayParam)
        $paramDictionary.Add('FailoverMS',$FailoverMSParam)
        return $paramDictionary
   }

    BEGIN
    {
        $f = $MyInvocation.InvocationName
        Write-Verbose -Message "$f - START"

        Write-Verbose -Message "$f -  Finding GatewayServer object"
        $Gateway = Get-SCOMManagementServer -Name $PSBoundParameters.Gateway

        Write-Verbose -Message "$f -  Checking for failoverservers"
        $failoverServer = $null
        $failoverServer = $Gateway.GetFailoverManagementServers() | where DisplayName -eq $PSBoundParameters.FailoverMS
        
        if(-not $failoverServers)
        {
            Write-Verbose -Message "$f -  Gateway does have a failover managementserver with name $($PSBoundParameters.FailoverMS)"
            break
        }
        
        Write-Verbose -Message "$f -  Creating a ManagementGroup object"
        $MG = Get-SCOMManagementGroup

        $MSType = [Type] $Gateway.GetType()

        Write-Verbose -Message "$f -  Creating a list collection of generic type Microsoft.EnterpriseManagement.Administration.ManagementServer"
        $GenericList = [System.Collections.Generic.List``1]
        $GenericTypeList = $GenericList.MakeGenericType(@($MSType))

        Write-Verbose -Message "$f -  Creating instance of list and adding the gateway server"
        $Gateways = [Activator]::CreateInstance($GenericTypeList)
        $Gateways.Add($gateway)

        Write-Verbose -Message "$f -  Creating instance of list and adding the failover servers that should be present"
        $FailoverManagementServers = [Activator]::CreateInstance($GenericTypeList)
        foreach($server in ($gateway.GetFailoverManagementServers() | where DisplayName -ne $PSBoundParameters.FailoverMS))
        {            
            $FailoverManagementServers.Add($server)
        }

        Write-Verbose -Message "$f -  Committing the gateway managementservers configuration to the managementgroup"
        $MG.Administration.SetManagementServer($Gateways, $gateway.GetPrimaryManagementServer(), $FailoverManagementServers)

        Write-Verbose -Message "$f -  Outputting current managementServer configuration"
        Get-SCOMParentManagementServer -GatewayServer $gateway

        Write-Verbose -Message "$f - END"
    }
}