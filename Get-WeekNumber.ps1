﻿function Get-WeekNumber
{
[cmdletbinding()]
Param(
    [Parameter(ValueFromPipeline)]
    [datetime]
    $date = (Get-Date)
)

Process
{
    $FirstDayOfWeek = [System.Globalization.DateTimeFormatInfo]::CurrentInfo.FirstDayOfWeek
    $CalendarWeekRule = [System.Globalization.DateTimeFormatInfo]::CurrentInfo.CalendarWeekRule
    $culture = Get-Culture
    $out = [PSCustomObject]@{
        Date = $date
        WeekNumber = $culture.Calendar.GetWeekOfYear($date, $CalendarWeekRule, $FirstDayOfWeek)
    }
    
    $out
}

}