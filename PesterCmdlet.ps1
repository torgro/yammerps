﻿Function Get-CmdletParameter
{
[cmdletBinding()]
Param(
    $CmdletName
)
$common = [System.Management.Automation.Cmdlet]::CommonParameters

$cmdlet = Get-Command -Name $CmdletName

if(-not $cmdlet)
{
    Write-Verbose "$CmdletName not found"
    break
}

$AllParams = $cmdlet.ParameterSets | foreach { $_.Parameters } | Select -ExpandProperty Name -Unique
$params = New-Object System.Collections.ArrayList
foreach($param in $AllParams)
{
    if($common -notcontains $param)
    {
        [void]$params.Add($param)
    }
}
return $params
}

function Get-CmdletParameterSet
{
[CmdletBinding()]
Param(
    $CmdletName
)
    $cmdlet = Get-Command -Name $CmdletName

    if(-not $cmdlet)
    {
        Write-Verbose "$CmdletName not found"
        break
    }

    $Sets = New-Object System.Collections.ArrayList
    foreach($set in $cmdlet.ParameterSets)
    {
        [void]$sets.Add($set.Name)
    }

    return $sets
}

function Test-ParameterExist
{
[cmdletbinding()]
Param(
    [System.Management.Automation.FunctionInfo]$command
    ,
    [string]$ParameterName
    ,
    [string]$ParameterSetName
)
}

function Get-CmdletParameterTests
{
[CmdletBinding()]
Param(
    [string]$cmdletName
)
    $Params = Get-CmdletParameter -CmdletName $cmdletName
    $cmdlet = Get-Command -Name $cmdletName

    $sb = New-Object System.Text.StringBuilder
    $block = Get-ParamterTestScriptBlock -cmdletName $cmdletName
    $nl = [environment]::NewLine
    [void]$sb.Append($block)
    [void]$sb.AppendLine()
    foreach($p in $Params)
    {
        $test = Get-ParameterTest -ParamName $p
        [void]$sb.Append($test)
        [void]$sb.AppendLine()
        if(Test-IsMandatoryParameter -cmdlet $cmdlet -parameterName $p)
        {
            $test = Get-ParameterTest -ParamName $P -Mandatory
            [void]$sb.Append($test)
            [void]$sb.AppendLine()
        }
    }
    foreach($set in $cmdlet.ParameterSets)
    {
        foreach ($parameter in $Params)
        {
            $test = Get-ParameterSetTests -SetName $set.Name -ParameterName $parameter -cmdlet $cmdlet
            [void]$sb.Append($test)
            [void]$sb.AppendLine()
        }
    }



    [void]$sb.Append("   }")
    return $sb.ToString()
}

function Test-IsMandatoryParameter
{
Param(
    $cmdlet
    ,
    $parameterName
)
$cmdlet.ParameterSets | foreach { $_.Parameters | foreach { $_ | where isMandatory -eq $true | where Name -eq "$parameterName" } }
}

function Get-ParameterTest
{
Param(
    $ParamName
    ,
    [switch]$Mandatory
)
if($Mandatory)
{
$test = @"
        It "$ParamName parameter should be Mandatory" {
             `$MandatoryParams -contains "$ParamName" | Should Be `$true
        }

"@
}
else
{
$test = @"
        It "$ParamName parameter should exist" {
            `$AllParams -contains "$ParamName" | Should Be `$true
        }

"@
}

return $test
}

function Get-ParamterSetsCountTest
{
Param(
    [int]$Count
)

$test = @"
        It "Parameters sets count should be equal to $Count" {
            `$ParamSetsCount | Should Be $Count
        }

"@

return $test
}

Function Get-ParameterSetTests
{
Param(
    [string]$SetName
    ,
    [String]$ParameterName
    ,
    $cmdlet
)
$test = $null
if(($cmdlet.ParameterSets | where name -eq "$SetName" | select -ExpandProperty Parameters | select -ExpandProperty Name) -contains $ParameterName)
{
$test = @"
        It "$ParameterName parameter should exist in ParameterSet '$Setname'" {
            { `$cmdlet.ResolveParameter("$ParameterName").Parametersets["$SetName"] } | Should not Be `$null
        }
"@
}

return $test
}

function Get-ParamterTestScriptBlock
{
Param(
    [string]$cmdletName
)
$Block = @'
        $ParamSetsCount = $cmdlet.ParameterSets.Count

        $MandatoryParams = $cmdlet.ParameterSets | foreach { 
                           $_.Parameters | foreach {
                           $_ | where isMandatory -eq $true } } | 
                           Select-Object -ExpandProperty Name -Unique

        $AllParams = $cmdlet.ParameterSets | foreach { 
                     $_.Parameters } |                        
                     Select-Object -ExpandProperty Name -Unique | 
                     foreach { 
                        if([System.Management.Automation.Cmdlet]::CommonParameters -notcontains $_)
                        { 
                            $_ 
                        }
        }

'@

$cmd = "`$cmdlet = Get-Command -Name $cmdletName"

$Block = @"
    Context "Parameter validation" {
        $cmd
        $Block
"@

return $Block
}