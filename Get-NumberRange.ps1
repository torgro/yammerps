﻿function Get-NumberRange
{
[cmdletbinding()]
Param()
DynamicParam {
    $Dictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary

    $NewDynParam = @{
        Name = "Firstnumber"
        Alias = "int"
        Mandatory = $true        
        DPDictionary = $Dictionary
    }    
    
    $NumberRange = @(1,2,3,4,5)
    $null = $NewDynParam.Add("ValidateSet",$NumberRange)

    if(-not (Get-Variable -Scope Global | Where-Object Name -eq "HiddenNumberVariable"))
    {
        New-Variable -Name HiddenNumberVariable -Scope Global
    }
    # Create the Firstnumber parameter
    New-DynamicParam @NewDynParam -Type int -ValidateScript {$null = Set-Variable -Name HiddenNumberVariable -Value $_ -Scope Global;$true}

    $SecondNumberRange = @(1,2,3,4,5)

    if(Get-Variable -Scope Global | Where-Object Name -eq "HiddenNumberVariable")
    {
        $SecondNumberRange = $SecondNumberRange | Where-Object {$_ -gt (Get-Variable -Name HiddenNumberVariable -Scope Global).Value} 
    }

    # Create the SecondNumber parameter
    New-DynamicParam -Name SecondNumber -Type int -ValidateSet $SecondNumberRange -DPDictionary $Dictionary
    #out-file -FilePath C:\temp\dynpara.txt -Encoding utf8 -InputObject (Get-Variable -Scope Global | where name -eq "HiddenNumberVariable" | out-string)
    # Return the parameters
    $Dictionary
    Get-Variable -Name HiddenNumberVariable -Scope Global -ErrorAction SilentlyContinue | Remove-Variable -Scope Global
}

BEGIN{
    # Remove variable from the global scope, we don't need it anymore
    Get-Variable -Name HiddenNumberVariable -Scope Global -ErrorAction SilentlyContinue | Remove-Variable -Scope Global 
}

PROCESS{
    [pscustomobject]$PSBoundParameters    
}

END{
}
}