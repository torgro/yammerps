function Get-CMFolderID
{
[cmdletbinding()]
Param(
    [parameter(Mandatory=$true)]
    [string] $FolderName
)
    $f = $MyInvocation.MyCommand.Name

    if(-not (Get-Module ConfigurationManager))
    {
        Write-Verbose "$f - Importing ConfigurationManager module"
        Import-Module "$(Split-Path $env:SMS_ADMIN_UI_PATH -Parent)\ConfigurationManager.psd1" -Verbose:$false
    }

    $sitecode = (Get-PSDrive -PSProvider CMSite).Name

    Write-Verbose "$f - Sitecode=$sitecode"

    Write-Verbose "$f - Setting current location to $($sitecode):"
    Set-Location "$($sitecode):"

    Get-WmiObject -Class SMS_ObjectContainerNode -Namespace Root\SMS\Site_$sitecode -filter "Name='$FolderName'"
}

function Get-CMContainerObjects
{
[cmdletbinding()]
Param(
        [parameter(Mandatory=$true)]
        $folderObject
        ,
        [Validateset("devicecollection")]
        [string] $ObjectType = "devicecollection"
)
    $f = $MyInvocation.MyCommand.Name

    switch($ObjectType)
    {            
        "devicecollection"
        {           
            Write-Verbose "$f - Getting devicecollection"
            $folderObject | foreach { Get-CMDeviceCollection -CollectionId $_.InstanceKey}  
        }
    }
}
 

function Get-CMFolderObject
{
[cmdletbinding()]
Param(
    [parameter(Mandatory=$true, ParameterSetName="Name")]
    [string]$FolderName

    ,
    [Validateset("devicecollection")]
    [string] $ObjectType = "devicecollection"
    ,
    [parameter(Mandatory=$true,ParameterSetName="ID")]
    [int]$ObjectID
    ,
    [int] $ParentFolderID
)
    $f = $MyInvocation.MyCommand.Name

    if(-not (Get-Module ConfigurationManager))
    {
        Write-Verbose "$f - Importing ConfigurationManager module"
        Import-Module "$(Split-Path $env:SMS_ADMIN_UI_PATH -Parent)\ConfigurationManager.psd1" -Verbose:$false
    }

    $sitecode = (Get-PSDrive -PSProvider CMSite).Name

    Write-Verbose "$f - Sitecode=$sitecode"

    Write-Verbose "$f - Setting current location to $($sitecode):"
    Set-Location "$($sitecode):"

    if($FolderName)
    {
        Write-Verbose "$f - In Name-parameterset, getting folder name '$FolderName'"
 
        Write-Verbose "$f - Getting folderobjects"

        $FolderObjects = $null
        $FolderObjects = Get-CMFolderID -FolderName $FolderName
        
        $FolderObjectsCount = ($FolderObjects | Measure-Object).Count

        Write-Verbose "$f - Found $FolderObjectsCount objects"

        if($FolderObjectsCount -eq 0)
        {
            throw "$f - Object not found"
        }

        foreach($folder in $FolderObjects)
        {
            $folderObject = $null

            if($ParentFolderID)
            {
                Write-Verbose "$f - ParentFolderID specified, checking"
                if($folder.ParentContainerNodeID -eq $ParentFolderID)
                {
                    $folderObject = Get-WmiObject -Class SMS_ObjectContainerItem -Namespace Root\SMS\Site_$SiteCode -filter "ContainerNodeID='$($folder.ContainerNodeID)'"
                }
            }
            else
            {
                Write-Verbose "$f - ParentFolderID NOT specified"
                $folderObject = Get-WmiObject -Class SMS_ObjectContainerItem -Namespace Root\SMS\Site_$SiteCode -filter "ContainerNodeID='$($folder.ContainerNodeID)'"
            }         
            
            if($folderObject)
            {
                Get-CMContainerObjects -folderObject $folderObject -ObjectType $ObjectType
            }
            else
            {
                Write-Verbose "$f - NO objects found for ContainerNodeID '$($folder.ContainerNodeID)'"
            }
        }
    }
    else
    {
        Write-Verbose "$f - In ID-parameterset, getting objects for folder with id $ObjectID"
        $folderObject = Get-WmiObject -Class SMS_ObjectContainerItem -Namespace Root\SMS\Site_$SiteCode -filter "ContainerNodeID=$ObjectID"
        
        if($folderObject)
        {            
            Get-CMContainerObjects -folderObject $folderObject -ObjectType $ObjectType
        }
        else
        {
            Write-Verbose "$f - NO objects found for ContainerNodeID '$($folder.ContainerNodeID)'"
        }      
     }
}