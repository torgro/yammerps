﻿function Set-SCOMGatewayFailover
{
<# 
.Synopsis  
    Sets SCOM gateway Primary and failover management server
.Description 
    All parameters are dynamic and requires the OperationsManager module to work.
.Example 
    Set-SCOMGatewayFailover -Gateway scomgw.contoso.com -PrimaryMS scomms1.contoso.com -FailoverMS scomms2.contoso.com
    Sets the primary managementserver to scommms1.contoso.com and failover to scomms2.contoso.com for gateway scomgw.contoso.com
.Parameter Gateway
    The gateway server you want to configure Managementserver failover for (ValidateSet with dynamic parameter)
.Parameter PrimaryMS
    The primary managementserver (ValidateSet with dynamic parameter)
.Parameter FailoverMS
    The primary managementserver (ValidateSet with dynamic parameter)
.Role 
    General 
.Component 
    SCOM OpsMgr 
.Notes 
    NAME: Set-SCOMGatewayFailover
    AUTHOR: Tore Groneng tore@firstpoint.no @toregroneng tore.groneng@gmail.com
    LASTEDIT: Jan 2015 
    KEYWORDS: OpsMgr, SCOM, SCOM2012, Gateway, SCOM-Gateway
    HELP:OK
.Link 
    Http://www.firstpoint.no 
#Requires -Version 4.0 
#>
[cmdletbinding()]
Param()
   DynamicParam 
   {
        $GateWayAttribute = New-Object System.Management.Automation.ParameterAttribute
        $GateWayAttribute.Position = 0
        $GateWayAttribute.Mandatory = $true

        $PrimaryMSattribute = New-Object System.Management.Automation.ParameterAttribute
        $PrimaryMSattribute.Position = 1
        $PrimaryMSattribute.Mandatory = $true
        
        $FailoverMSattribute = New-Object System.Management.Automation.ParameterAttribute
        $FailoverMSattribute.Position = 2
        $FailoverMSattribute.Mandatory = $true
        
        $attributeCollectionGW = new-object System.Collections.ObjectModel.Collection[System.Attribute]
        $attributeCollectionGW.Add($GateWayAttribute)

        $attributeCollectionPrimary= new-object System.Collections.ObjectModel.Collection[System.Attribute]
        $attributeCollectionPrimary.Add($PrimaryMSattribute)

        $attributeCollectionFailover = new-object System.Collections.ObjectModel.Collection[System.Attribute]
        $attributeCollectionFailover.Add($FailoverMSattribute)

        $ValidateSetGatewayValues = Get-SCOMManagementServer | Where-Object isGateway -eq $true | Select-Object -ExpandProperty DisplayName
        $ValidateSetAttributeGW = New-Object System.Management.Automation.ValidateSetAttribute($ValidateSetGatewayValues)
        $attributeCollectionGW.Add($ValidateSetAttributeGW)

        $ValidateSetValuesMS = Get-SCOMManagementServer | Where-Object isGateway -eq $false | Select-Object -ExpandProperty DisplayName
        $ValidateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute($ValidateSetValuesMS)
        $attributeCollectionPrimary.Add($ValidateSetAttribute)
        $attributeCollectionFailover.Add($ValidateSetAttribute)

        $GatewayParam = New-Object System.Management.Automation.RuntimeDefinedParameter('Gateway', [string], $attributeCollectionGW)
        $PrimaryMSParam = New-Object System.Management.Automation.RuntimeDefinedParameter('PrimaryMS', [string], $attributeCollectionPrimary)
        $FailoverMSParam = New-Object System.Management.Automation.RuntimeDefinedParameter('FailoverMS', [string], $attributeCollectionFailover)
        
        $paramDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        $paramDictionary.Add('Gateway',$GatewayParam)
        $paramDictionary.Add('PrimaryMS', $PrimaryMSParam)
        $paramDictionary.Add('FailoverMS',$FailoverMSParam)
        return $paramDictionary
   } 

   BEGIN
   {
        $f = $MyInvocation.InvocationName
        Write-Verbose -Message "$f - START"

        Write-Verbose -Message "$f -  Finding gatewayserver with name $($PSBoundParameters.Gateway)"
        $Gateway = Get-SCOMManagementServer -Name $PSBoundParameters.Gateway

        Write-Verbose -Message "$f -  Finding Primary ManagementServer with name $($PSBoundParameters.PrimaryMS)"
        $primary = Get-SCOMManagementServer -Name $PSBoundParameters.PrimaryMS

        Write-Verbose -Message "$f -  Finding failover ManagementServer with name $($PSBoundParameters.FailoverMS)"
        $Failover = Get-SCOMManagementServer -Name $PSBoundParameters.FailoverMS

        if($Primary.IsRootManagementServer -eq $false)
        {
            Write-Warning -Message "Selected ManagementServer with name $($PSBoundParameters.PrimaryMS) is NOT a RootManagementServer"
        }

        Write-Verbose -Message "$f -  Setting PrimaryServer for Gateway $($PSBoundParameters.Gateway)"
        Set-SCOMParentManagementServer -GatewayServer $Gateway -PrimaryServer $primary

        Write-Verbose -Message "$f -  Setting FailoverServer for Gateway $($PSBoundParameters.Gateway)"
        Set-SCOMParentManagementServer -GatewayServer $Gateway -FailoverServer $Failover
    
        Write-Verbose -Message "$f - END"
    }
}