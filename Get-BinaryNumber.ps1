﻿function Get-BinaryNumber
{
[CmdletBinding()]
[OutputType([string])]
Param(
    [Parameter(ValueFromPipeLine)]
    [int[]]$Number
    ,
    [int]$NumberOfDigits
)

    BEGIN
    {
        if(-not $PSBoundParameters.ContainsKey("NumberOfDigits"))
        {
            $NumberOfDigits = 4
        }
    }

    PROCESS
    {
        foreach($num in $Number)
        {
            if($num -ge 0)
            {
                ([int][System.Convert]::ToString("$num",2)).ToString("D$NumberOfDigits")
            }        
        }
    }

}

function Get-HexNumber
{
[CmdletBinding()]
[OutputType([string])]
Param(
    [Parameter(ValueFromPipeLine)]
    [int64[]]$Number
    ,
    [int]$NumberOfDigits
)

    BEGIN
    {
        if(-not $PSBoundParameters.ContainsKey("NumberOfDigits"))
        {
            $NumberOfDigits = 4
        }
    }

    PROCESS
    {
        foreach($num in $Number)
        {
            "0x$($num.ToString("X$NumberOfDigits"))"
        }
    }
}

function Get-IntFromBinary
{
[CmdletBinding()]
[OutputType([int64])]
Param(
    [Parameter(ValueFromPipeLine)]
    [string[]]$BinaryNumber
)
    BEGIN
    {}

    PROCESS
    {
        foreach($string in $BinaryNumber)
        {
            [System.Convert]::ToInt64($string,2)
        }
    }
}

function Get-IntFromHex
{
[CmdletBinding()]
[OutputType([int64])]
Param(
    [Parameter(ValueFromPipeLine)]
    [string[]]$HexValue
)
    BEGIN
    {}

    PROCESS
    {
        foreach($string in $HexValue)
        {
            [System.Convert]::ToInt64($string,16)
        }
    }
}

function Convert-Number
{
[CmdletBinding()]
[OutputType([String])]
Param(
    [Parameter(ValueFromPipeLine)]
    [int64[]]$Number
)
    BEGIN{}

    PROCESS
    {
        foreach($int in $Number)
        {
            $out = "" | Select-Object -Property Int, Hex, Binary, Bytes, OddNumber
            $out.Int = $int
            $out.Hex = Get-HexNumber -Number $int
            $out.Binary = Get-BinaryNumber -Number $int
            $out.Bytes = [System.BitConverter]::GetBytes($int)
            $out.OddNumber = Get-OddNumber -Number $int
            $out
        }
    }
}

function Get-IntFromByteArray
{
[cmdletbinding()]
[OutputType([int64])]
Param(    
    [Parameter(ValueFromPipeline)]
    [byte[]]$Bytes    
)
    BEGIN
    {
        $arrayList = New-Object -TypeName System.Collections.ArrayList
    }

    PROCESS
    {    
        if($Bytes -is [array])
        {
            Write-Verbose "is array" -Verbose
            $null = $arrayList.AddRange($Bytes)
        }
        else
        {
            Write-Verbose "is NOT array" -Verbose
            $null = $arrayList.Add($Bytes)
        }
    }
    
    END
    {
        Write-Verbose -Message "Count=$($arrayList.Count)" -Verbose
        [System.BitConverter]::ToInt64($arrayList.ToArray(),0)
        #$arrayList
    }
}

function Get-OddNumber
{
[cmdletbinding()]
[OutputType([bool])]
Param(
    [Parameter(ValueFromPipeline)]
    [int64[]]$Number
)

    BEGIN
    {}

    PROCESS
    {
        foreach($int in $Number)
        {
            if($int % 2 -eq 0)
            {
                $false
            }
            else
            {
                $true
            }
        }
    }
}