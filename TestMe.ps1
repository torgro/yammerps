Function Get-Person
{
[cmdletbinding()]
Param(
    
    [string]$Name
    ,
    [string]$DisplayName
    ,
    [string]$ObjectID
    ,
    [pscredential]$Credential
    ,
    [string]$Uri
)
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"

    [string]$ObjectType = "Person"

    $Get_Object = @{
        ObjectType  = $ObjectType
        Credential  = $Credential
        Uri         = $Uri
        ObjectName  = $null
        ObjectValue = $null
    }

    if($Name)
    {
        $Get_Object.ObjectName  = "Name"
        $Get_Object.ObjectValue = "$Name"
    }

    if($DisplayName)
    {
        $Get_Object.ObjectName  = "DisplayName"
        $Get_Object.ObjectValue = "$DisplayName"
    }

    if($ObjectID)
    {
        $Get_Object.ObjectName  = "ObjectID"
        $Get_Object.ObjectValue = "$ObjectID"
    }
    
    Write-Verbose -Message "$f -  Removing null values from hashtable"
    $Get_Object = $Get_Object | Remove-HashtableNullValue

    Write-Verbose -Message "$f -  Getting objects"
    Get-Object @Get_Object

    Write-Verbose -Message "$f - END"
}

Function Update-Person
{
[cmdletbinding()]
Param(
    [string]$Name
    ,
    [string]$DisplayName
    ,
    [Parameter(Mandatory)]
    [string]$ObjectID
    ,
    [pscredential]$Credential
    ,
    [string]$Uri
)
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"

    [string]$ObjectType = "Person"
    [string]$Operation = "Update"
    
    Write-Verbose -Message "$f -  Checking if object exists"
    $ObjectExists = Get-Person @PSBoundParameters

    if(-not $ObjectExists)
    {
        throw "Object does not exist"
    }

    $KeyValue  = @{
        ObjectType  = $ObjectType
        ObjectID    = $ObjectExists.Node.ObjectID
        Operation   = $Operation
        ObjectName  = $null
        ObjectValue = $null
    }

    if($Name)
    {
        $KeyValue.ObjectName = "Name"
        $KeyValue.ObjectValue = "$Name"
    }

    if($DisplayName)
    {
        $KeyValue.ObjectName = "DisplayName"
        $KeyValue.ObjectValue = "$DisplayName"
    }

    $Import_Object = @{
        KeyValue   = $KeyValue
        Credential = $Credential
        Uri        = $Uri
    }

    Import-Object @Import_Object
}

Function Get-Group
{
[cmdletbinding()]
Param(
    
    [string]$Name
    ,
    [string]$Description
    ,
    [string]$ObjectID
    ,
    [pscredential]$Credential
    ,
    [string]$Uri
)
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"

    [string]$ObjectType = "Group"

    $Get_Object = @{
        ObjectType  = $ObjectType
        Credential  = $Credential
        Uri         = $Uri
        ObjectName  = $null
        ObjectValue = $null
    }

    if($Name)
    {
        $Get_Object.ObjectName  = "Name"
        $Get_Object.ObjectValue = "$Name"
    }

    if($Description)
    {
        $Get_Object.ObjectName  = "Description"
        $Get_Object.ObjectValue = "$Description"
    }

    if($ObjectID)
    {
        $Get_Object.ObjectName  = "ObjectID"
        $Get_Object.ObjectValue = "$ObjectID"
    }
    
    Write-Verbose -Message "$f -  Removing null values from hashtable"
    $Get_Object = $Get_Object | Remove-HashtableNullValue

    Write-Verbose -Message "$f -  Getting objects"
    Get-Object @Get_Object

    Write-Verbose -Message "$f - END"
}

Function Update-Group
{
[cmdletbinding()]
Param(
    [string]$Name
    ,
    [string]$Description
    ,
    [Parameter(Mandatory)]
    [string]$ObjectID
    ,
    [pscredential]$Credential
    ,
    [string]$Uri
)
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"

    [string]$ObjectType = "Group"
    [string]$Operation = "Update"
    
    Write-Verbose -Message "$f -  Checking if object exists"
    $ObjectExists = Get-Group @PSBoundParameters

    if(-not $ObjectExists)
    {
        throw "Object does not exist"
    }

    $KeyValue  = @{
        ObjectType  = $ObjectType
        ObjectID    = $ObjectExists.Node.ObjectID
        Operation   = $Operation
        ObjectName  = $null
        ObjectValue = $null
    }

    if($Name)
    {
        $KeyValue.ObjectName  = "Name"
        $KeyValue.ObjectValue = "$Name"
    }

    if($Description)
    {
        $KeyValue.ObjectName  = "Description"
        $KeyValue.ObjectValue = "$Description"
    }

    $Import_Object = @{
        KeyValue   = $KeyValue
        Credential = $Credential
        Uri        = $Uri
    }

    Import-Object @Import_Object
}

Function Get-Object
{
[cmdletbinding()]
Param(
    [Parameter(Mandatory)]
    [ValidateSet("Person","Group")]
    [string]$ObjectType
    ,
    [string]$ObjectName
    ,
    [string]$ObjectValue
    ,
    [pscredential]$Credential
    ,
    [string]$Uri = "http://localhost:34544/Sillysystem"
)
    $F = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"

    [string]$xPath = "//$ObjectType"

    If($ObjectName -and $ObjectValue)
    {
        $xPath += "[@$ObjectName='$ObjectValue']"
    }

    $Export_Object = @{
        xPath      = $xPath
        Credential = $Credential
        Uri        = $Uri
    }

    Write-Verbose -Message "$f -  Remove null values from hashtable"
    $Export_Object = $Export_Object | Remove-HashtableNullValue

    Export-Object @Export_Object
    
}

Function Remove-HashtableNullValue
{
[cmdletbinding()]
Param(
    [Parameter(ValueFromPipeline)]
    [Alias("InputObject")]
    [hashtable]$Hash
)
    BEGIN
    {
        $F = $MyInvocation.InvocationName
        Write-Verbose -Message "$f - START"
        [int]$CountNonNull = 0
    }

    PROCESS
    {    
        foreach($key in $Hash.Keys)
        {
            if($Hash.$key -ne $null)
            {
                $CountNonNull ++
            }
        }
        if($CountNonNull -gt 0)
        {
            foreach($KeyValue in $Hash.Clone().GetEnumerator())
            {
                if($KeyValue.Value -eq $null)
                {
                    Write-Verbose -Message "$f -  Key '$($KeyValue.key)' is null, removing it"
                    $Hash.Remove($KeyValue.Key)
                }
            }
            return $hash
        }
        else
        {
            Write-Verbose -Message "$f -  All keys were null"
            return @{}
        }
    }

    END
    {
        Write-Verbose -Message "$f - END"
    }
}

function Export-Object 
{
[cmdletbinding()]
Param(
    [Parameter(Mandatory)]
    [string]$xPath
    ,
    [pscredential]$Credential
    ,
    [string]$Uri
)
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"
    
    Write-Verbose -Message "$f -  Getting data"
    $data = Get-XMLdocument

    Write-Verbose -Message "$f - xpath = $xPath"

    Write-Verbose -Message "$f -  Returning objects"
    Select-Xml -Xml $data -XPath $xPath

    Write-Verbose -Message "$f - END"
}

function Import-Object
{
[cmdletbinding()]
Param(
    [Parameter(Mandatory)]
    [Hashtable]$KeyValue
    ,
    [pscredential]$Credential
    ,
    [string]$Uri
)
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"

    Write-Verbose -Message "$f -  Getting data"
    $db = Get-XMLdocument

    if(-not ($KeyValue.ObjectType -ne "Person" -or $KeyValue.ObjectType -ne "Group"))
    {
        throw "Object type $($KeyValue.ObjectType) not supported"
    }

    if($KeyValue.Operation -ne "Update")
    {
        throw "Operation on objectID $($KeyValue.ObjectID) cannot complete, operation $($KeyValue.Operation) is not supported or null"
    }

    if(-not $KeyValue.ObjectName)
    {
        throw "ObjectName $($KeyValue.ObjectName) is not supported or null"
    }

    [string]$ObjectID    = $KeyValue.ObjectID
    [string]$ObjectType  = $KeyValue.ObjectType
    [string]$ObjectName  = $KeyValue.ObjectName
    [string]$ObjectValue = $KeyValue.ObjectValue

    [string]$xPath = "//$ObjectType[@ObjectID = '$ObjectID']"
    Write-Verbose -Message "$f -  xpath = $xPath"

    $obj = $db | Select-Xml -XPath $xPath
    Write-Verbose -Message "$F -  ObjectName = $ObjectName = $ObjectValue"

    $obj.Node.$ObjectName = $ObjectValue

    Set-Variable -Name xml -Value $db.InnerXml -Scope Global    
}

Function Get-XMLdocument
{
[CmdletBinding()]
Param()
    $f = $MyInvocation.InvocationName
    Write-Verbose -Message "$f - START"

$data = @"
<xml version="1.0" encoding="UTF-8">
    <SillySystem>
        <Person Name="Tore" DisplayName = "Tore Groneng" ObjectID="db8740f2-d7a8-429e-8b3b-0e637c13de61"/>
        <Person Name="John" DisplayName = "Doe" ObjectID="f912a290-0772-4a29-af05-198a383ed72c"/>
        <Group Name="Users" Description = "Contains all users" ObjectID="99c026f4-6a13-489a-84ea-5e070e26cf5f"/>
        <Group Name="Admins" Description = "Contains all admins" ObjectID="41e67c6a-f3a0-4e22-9897-3da5d9898d54"/>
    </SillySystem>
</xml>
"@
    Write-Verbose -Message "$f -  Checking for 'global' data variable"
    if(-not(Get-Variable -Name xml -Scope Global -ErrorAction SilentlyContinue))
    {
        Write-Verbose -Message "$f -  Not found, creating new variable named xml in global scope"
        Set-Variable -Name xml -Value $data -Scope Global
    }
    else
    {
        Write-Verbose -Message "$f -  Variable found, creating XML document"
        $data = (Get-Variable -Name XML -Scope Global).Value
    }
    
    Write-Verbose "$f - END"
    return [xml]($data)
}
